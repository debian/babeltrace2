/* A Bison parser, made by GNU Bison 3.8.2.  */

/* Bison implementation for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2015, 2018-2021 Free Software Foundation,
   Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* C LALR(1) parser skeleton written by Richard Stallman, by
   simplifying the original so-called "semantic" parser.  */

/* DO NOT RELY ON FEATURES THAT ARE NOT DOCUMENTED in the manual,
   especially those whose name start with YY_ or yy_.  They are
   private implementation details that can be changed or removed.  */

/* All symbols defined below should begin with yy or YY, to avoid
   infringing on user name space.  This should be done even for local
   variables, as they might otherwise be expanded by user macros.
   There are some unavoidable exceptions within include files to
   define necessary library symbols; they are noted "INFRINGES ON
   USER NAME SPACE" below.  */

/* Identify Bison output, and Bison version.  */
#define YYBISON 30802

/* Bison version string.  */
#define YYBISON_VERSION "3.8.2"

/* Skeleton name.  */
#define YYSKELETON_NAME "yacc.c"

/* Pure parsers.  */
#define YYPURE 1

/* Push parsers.  */
#define YYPUSH 0

/* Pull parsers.  */
#define YYPULL 1




/* First part of user prologue.  */
#line 1 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"

/*
 * SPDX-License-Identifier: MIT
 *
 * Copyright 2010 - Mathieu Desnoyers <mathieu.desnoyers@efficios.com>
 *
 * Common Trace Format Metadata Grammar.
 */

#include "plugins/ctf/common/src/metadata/tsdl/logging.hpp"

#include "common/list.h"
#include "common/assert.h"

#include "plugins/ctf/common/src/metadata/tsdl/scanner.hpp"
#include "plugins/ctf/common/src/metadata/tsdl/ast.hpp"
#include "plugins/ctf/common/src/metadata/tsdl/objstack.hpp"
#include "plugins/ctf/common/src/metadata/tsdl/parser-wrap.hpp"

/*
 * Avoid warning about "yynerrs" being unused, seen with bison 3.5.1 + clang 15
 * on Ubuntu 20.04.
 */
BT_DIAG_IGNORE_UNUSED_BUT_SET_VARIABLE

thread_local const ctf_scanner *currentCtfScanner;

#define YYFPRINTF(_stream, _fmt, args...)                                                          \
    do {                                                                                           \
        int size = snprintf(NULL, 0, (_fmt), ##args);                                              \
        std::string str(size, '\0');                                                               \
        int written = snprintf(&str[0], size + 1, (_fmt), ##args);                                 \
        BT_ASSERT(size == written);                                                                \
        BT_CPPLOGT_SPEC(currentCtfScanner->logger, "{}", str.c_str());                             \
    } while (0)

/* Join two lists, put "add" at the end of "head".  */
static inline void
_bt_list_splice_tail (struct bt_list_head *add, struct bt_list_head *head)
{
	/* Do nothing if the list which gets added is empty.  */
	if (add != add->next) {
		add->next->prev = head->prev;
		add->prev->next = head;
		head->prev->next = add->next;
		head->prev = add->prev;
	}
}

int yylex(union YYSTYPE *yyval, yyscan_t yyscanner);
int yylex_init_extra(struct ctf_scanner *scanner, yyscan_t * ptr_yy_globals);
int yylex_destroy(yyscan_t yyscanner);
void yyrestart(FILE * in_str, yyscan_t yyscanner);
int yyget_lineno(yyscan_t yyscanner);
char *yyget_text(yyscan_t yyscanner);

/*
 * Static node for out of memory errors. Only "type" is used. lineno is
 * always left at 0. The rest of the node content can be overwritten,
 * but is never used.
 */
static struct ctf_node error_node = {
	.parent = nullptr,
	.siblings = {},
	.tmp_head = {},
	.lineno = 0,
	.visited = 0,
	.type = NODE_ERROR,
};

const char *node_type(struct ctf_node *node)
{
	switch (node->type) {
#define ENTRY(S) case S: return #S;
	FOREACH_CTF_NODES(ENTRY)
#undef ENTRY
	};

	bt_common_abort();
}

void setstring(struct ctf_scanner *scanner, YYSTYPE *lvalp, const char *src)
{
	lvalp->s = (char *) objstack_alloc(scanner->objstack, strlen(src) + 1);
	strcpy(lvalp->s, src);
}

static
int str_check(size_t str_len, size_t offset, size_t len)
{
	/* check overflow */
	if (offset + len < offset)
		return -1;
	if (offset + len > str_len)
		return -1;
	return 0;
}

static
int bt_isodigit(int c)
{
	switch (c) {
	case '0':
	case '1':
	case '2':
	case '3':
	case '4':
	case '5':
	case '6':
	case '7':
		return 1;
	default:
		return 0;
	}
}

static
int parse_base_sequence(const char *src, size_t len, size_t pos,
		char *buffer, size_t *buf_len, int base)
{
	const size_t max_char = 3;
	int nr_char = 0;

	while (!str_check(len, pos, 1) && nr_char < max_char) {
		char c = src[pos++];

		if (base == 8) {
			if (bt_isodigit(c))
				buffer[nr_char++] = c;
			else
				break;
		} else if (base == 16) {
			if (isxdigit(c))
				buffer[nr_char++] = c;
			else
				break;

		} else {
			/* Unsupported base */
			return -1;
		}
	}
	BT_ASSERT_DBG(nr_char > 0);
	buffer[nr_char] = '\0';
	*buf_len = nr_char;
	return 0;
}

static
int import_basic_string(struct ctf_scanner *scanner, YYSTYPE *lvalp,
		size_t len, const char *src, char delim)
{
	size_t pos = 0, dpos = 0;

	if (str_check(len, pos, 1))
		return -1;
	if (src[pos++] != delim)
		return -1;

	while (src[pos] != delim) {
		char c;

		if (str_check(len, pos, 1))
			return -1;
		c = src[pos++];
		if (c == '\\') {
			if (str_check(len, pos, 1))
				return -1;
			c = src[pos++];

			switch (c) {
			case 'a':
				c = '\a';
				break;
			case 'b':
				c = '\b';
				break;
			case 'f':
				c = '\f';
				break;
			case 'n':
				c = '\n';
				break;
			case 'r':
				c = '\r';
				break;
			case 't':
				c = '\t';
				break;
			case 'v':
				c = '\v';
				break;
			case '\\':
				c = '\\';
				break;
			case '\'':
				c = '\'';
				break;
			case '\"':
				c = '\"';
				break;
			case '?':
				c = '?';
				break;
			case '0':
			case '1':
			case '2':
			case '3':
			case '4':
			case '5':
			case '6':
			case '7':
			{
				char oct_buffer[4];
				size_t oct_len;

				if (parse_base_sequence(src, len, pos - 1,
						oct_buffer, &oct_len, 8))
					return -1;
				c = strtoul(&oct_buffer[0], NULL, 8);
				pos += oct_len - 1;
				break;
			}
			case 'x':
			{
				char hex_buffer[4];
				size_t hex_len;

				if (parse_base_sequence(src, len, pos,
						hex_buffer, &hex_len, 16))
					return -1;
				c = strtoul(&hex_buffer[0], NULL, 16);
				pos += hex_len;
				break;
			}
			default:
				return -1;
			}
		}
		if (str_check(len, dpos, 1))
			return -1;
		lvalp->s[dpos++] = c;
	}

	if (str_check(len, dpos, 1))
		return -1;
	lvalp->s[dpos++] = '\0';

	if (str_check(len, pos, 1))
		return -1;
	if (src[pos++] != delim)
		return -1;

	if (str_check(len, pos, 1))
		return -1;
	if (src[pos] != '\0')
		return -1;
	return 0;
}

int import_string(struct ctf_scanner *scanner, YYSTYPE *lvalp,
		const char *src, char delim)
{
	size_t len;

	len = strlen(src) + 1;
	lvalp->s = (char *) objstack_alloc(scanner->objstack, len);
	if (src[0] == 'L') {
		// TODO: import wide string
		_BT_CPPLOGE_APPEND_CAUSE_LINENO(currentCtfScanner->logger,
			yyget_lineno(scanner),
			"wide characters are not supported as of this version: "
			"scanner-addr={}", fmt::ptr(scanner));
		return -1;
	} else {
		return import_basic_string(scanner, lvalp, len, src, delim);
	}
}

static void init_scope(struct ctf_scanner_scope *scope,
		       struct ctf_scanner_scope *parent)
{
	scope->parent = parent;
	scope->classes = g_hash_table_new_full(g_str_hash, g_str_equal,
					     NULL, NULL);
}

static void finalize_scope(struct ctf_scanner_scope *scope)
{
	g_hash_table_destroy(scope->classes);
}

static void push_scope(struct ctf_scanner *scanner)
{
	struct ctf_scanner_scope *ns;

	BT_CPPLOGT_SPEC(currentCtfScanner->logger,
		"Pushing scope: scanner-addr={}", fmt::ptr(scanner));
	ns = (ctf_scanner_scope *) malloc(sizeof(struct ctf_scanner_scope));
	init_scope(ns, scanner->cs);
	scanner->cs = ns;
}

static void pop_scope(struct ctf_scanner *scanner)
{
	struct ctf_scanner_scope *os;

	BT_CPPLOGT_SPEC(currentCtfScanner->logger,
		"Popping scope: scanner-addr={}", fmt::ptr(scanner));
	os = scanner->cs;
	scanner->cs = os->parent;
	finalize_scope(os);
	free(os);
}

static int lookup_type(struct ctf_scanner_scope *s, const char *id)
{
	int ret;

	ret = GPOINTER_TO_INT(g_hash_table_lookup(s->classes, id));
	BT_CPPLOGT_SPEC(currentCtfScanner->logger,
		"Looked up type: scanner-addr={}, id=\"{}\", ret={}",
		fmt::ptr(s), id, ret);
	return ret;
}

int is_type(struct ctf_scanner *scanner, const char *id)
{
	struct ctf_scanner_scope *it;
	int ret = 0;

	for (it = scanner->cs; it; it = it->parent) {
		if (lookup_type(it, id)) {
			ret = 1;
			break;
		}
	}
	BT_CPPLOGT_SPEC(currentCtfScanner->logger,
		"Found if ID is type: scanner-addr={}, id=\"{}\", ret={}",
		fmt::ptr(scanner), id, ret);
	return ret;
}

static void add_type(struct ctf_scanner *scanner, char *id)
{
	BT_CPPLOGT_SPEC(currentCtfScanner->logger,
		"Adding type: scanner-addr={}, id=\"{}\"", fmt::ptr(scanner),
		id);
	if (lookup_type(scanner->cs, id))
		return;
	g_hash_table_insert(scanner->cs->classes, id, id);
}

static struct ctf_node *make_node(struct ctf_scanner *scanner,
				  enum node_type type)
{
	struct ctf_node *node;

	node = (ctf_node *) objstack_alloc(scanner->objstack, sizeof(*node));
	if (!node) {
		_BT_CPPLOGE_APPEND_CAUSE_LINENO(currentCtfScanner->logger,
			yyget_lineno(scanner->scanner),
			"failed to allocate one stack entry: "
			"scanner-addr={}", fmt::ptr(scanner));
		return &error_node;
	}
	node->type = type;
	node->lineno = yyget_lineno(scanner->scanner);
	BT_INIT_LIST_HEAD(&node->tmp_head);
	bt_list_add(&node->siblings, &node->tmp_head);

	switch (type) {
	case NODE_ROOT:
		node->type = NODE_ERROR;
		BT_CPPLOGE_SPEC(currentCtfScanner->logger, "Trying to create root node: scanner-addr={}",
			fmt::ptr(scanner));
		break;
	case NODE_EVENT:
		BT_INIT_LIST_HEAD(&node->u.event.declaration_list);
		break;
	case NODE_STREAM:
		BT_INIT_LIST_HEAD(&node->u.stream.declaration_list);
		break;
	case NODE_ENV:
		BT_INIT_LIST_HEAD(&node->u.env.declaration_list);
		break;
	case NODE_TRACE:
		BT_INIT_LIST_HEAD(&node->u.trace.declaration_list);
		break;
	case NODE_CLOCK:
		BT_INIT_LIST_HEAD(&node->u.clock.declaration_list);
		break;
	case NODE_CALLSITE:
		BT_INIT_LIST_HEAD(&node->u.callsite.declaration_list);
		break;
	case NODE_CTF_EXPRESSION:
		BT_INIT_LIST_HEAD(&node->u.ctf_expression.left);
		BT_INIT_LIST_HEAD(&node->u.ctf_expression.right);
		break;
	case NODE_UNARY_EXPRESSION:
		break;
	case NODE_TYPEDEF:
		BT_INIT_LIST_HEAD(&node->u.field_class_def.field_class_declarators);
		break;
	case NODE_TYPEALIAS_TARGET:
		BT_INIT_LIST_HEAD(&node->u.field_class_alias_target.field_class_declarators);
		break;
	case NODE_TYPEALIAS_ALIAS:
		BT_INIT_LIST_HEAD(&node->u.field_class_alias_name.field_class_declarators);
		break;
	case NODE_TYPEALIAS:
		break;
	case NODE_TYPE_SPECIFIER:
		break;
	case NODE_TYPE_SPECIFIER_LIST:
		BT_INIT_LIST_HEAD(&node->u.field_class_specifier_list.head);
		break;
	case NODE_POINTER:
		break;
	case NODE_TYPE_DECLARATOR:
		BT_INIT_LIST_HEAD(&node->u.field_class_declarator.pointers);
		break;
	case NODE_FLOATING_POINT:
		BT_INIT_LIST_HEAD(&node->u.floating_point.expressions);
		break;
	case NODE_INTEGER:
		BT_INIT_LIST_HEAD(&node->u.integer.expressions);
		break;
	case NODE_STRING:
		BT_INIT_LIST_HEAD(&node->u.string.expressions);
		break;
	case NODE_ENUMERATOR:
		BT_INIT_LIST_HEAD(&node->u.enumerator.values);
		break;
	case NODE_ENUM:
		BT_INIT_LIST_HEAD(&node->u._enum.enumerator_list);
		break;
	case NODE_STRUCT_OR_VARIANT_DECLARATION:
		BT_INIT_LIST_HEAD(&node->u.struct_or_variant_declaration.field_class_declarators);
		break;
	case NODE_VARIANT:
		BT_INIT_LIST_HEAD(&node->u.variant.declaration_list);
		break;
	case NODE_STRUCT:
		BT_INIT_LIST_HEAD(&node->u._struct.declaration_list);
		BT_INIT_LIST_HEAD(&node->u._struct.min_align);
		break;
	case NODE_UNKNOWN:
	default:
		node->type = NODE_ERROR;
		BT_CPPLOGE_SPEC(currentCtfScanner->logger, "Unknown node type: scanner-addr={}, node-type={}",
			fmt::ptr(scanner), type);
		break;
	}

	return node;
}

static int reparent_ctf_expression(struct ctf_node *node,
				   struct ctf_node *parent)
{
	switch (parent->type) {
	case NODE_EVENT:
		_bt_list_splice_tail(&node->tmp_head, &parent->u.event.declaration_list);
		break;
	case NODE_STREAM:
		_bt_list_splice_tail(&node->tmp_head, &parent->u.stream.declaration_list);
		break;
	case NODE_ENV:
		_bt_list_splice_tail(&node->tmp_head, &parent->u.env.declaration_list);
		break;
	case NODE_TRACE:
		_bt_list_splice_tail(&node->tmp_head, &parent->u.trace.declaration_list);
		break;
	case NODE_CLOCK:
		_bt_list_splice_tail(&node->tmp_head, &parent->u.clock.declaration_list);
		break;
	case NODE_CALLSITE:
		_bt_list_splice_tail(&node->tmp_head, &parent->u.callsite.declaration_list);
		break;
	case NODE_FLOATING_POINT:
		_bt_list_splice_tail(&node->tmp_head, &parent->u.floating_point.expressions);
		break;
	case NODE_INTEGER:
		_bt_list_splice_tail(&node->tmp_head, &parent->u.integer.expressions);
		break;
	case NODE_STRING:
		_bt_list_splice_tail(&node->tmp_head, &parent->u.string.expressions);
		break;

	case NODE_ROOT:
	case NODE_CTF_EXPRESSION:
	case NODE_TYPEDEF:
	case NODE_TYPEALIAS_TARGET:
	case NODE_TYPEALIAS_ALIAS:
	case NODE_TYPEALIAS:
	case NODE_TYPE_SPECIFIER:
	case NODE_TYPE_SPECIFIER_LIST:
	case NODE_POINTER:
	case NODE_TYPE_DECLARATOR:
	case NODE_ENUMERATOR:
	case NODE_ENUM:
	case NODE_STRUCT_OR_VARIANT_DECLARATION:
	case NODE_VARIANT:
	case NODE_STRUCT:
	case NODE_UNARY_EXPRESSION:
		return -EPERM;

	case NODE_UNKNOWN:
	default:
		BT_CPPLOGE_SPEC(currentCtfScanner->logger, "Unknown node type: node-type={}", parent->type);
		return -EINVAL;
	}
	return 0;
}

static int reparent_typedef(struct ctf_node *node, struct ctf_node *parent)
{
	switch (parent->type) {
	case NODE_ROOT:
		_bt_list_splice_tail(&node->tmp_head, &parent->u.root.declaration_list);
		break;
	case NODE_EVENT:
		_bt_list_splice_tail(&node->tmp_head, &parent->u.event.declaration_list);
		break;
	case NODE_STREAM:
		_bt_list_splice_tail(&node->tmp_head, &parent->u.stream.declaration_list);
		break;
	case NODE_ENV:
		_bt_list_splice_tail(&node->tmp_head, &parent->u.env.declaration_list);
		break;
	case NODE_TRACE:
		_bt_list_splice_tail(&node->tmp_head, &parent->u.trace.declaration_list);
		break;
	case NODE_CLOCK:
		_bt_list_splice_tail(&node->tmp_head, &parent->u.clock.declaration_list);
		break;
	case NODE_CALLSITE:
		_bt_list_splice_tail(&node->tmp_head, &parent->u.callsite.declaration_list);
		break;
	case NODE_VARIANT:
		_bt_list_splice_tail(&node->tmp_head, &parent->u.variant.declaration_list);
		break;
	case NODE_STRUCT:
		_bt_list_splice_tail(&node->tmp_head, &parent->u._struct.declaration_list);
		break;

	case NODE_FLOATING_POINT:
	case NODE_INTEGER:
	case NODE_STRING:
	case NODE_CTF_EXPRESSION:
	case NODE_TYPEDEF:
	case NODE_TYPEALIAS_TARGET:
	case NODE_TYPEALIAS_ALIAS:
	case NODE_TYPEALIAS:
	case NODE_TYPE_SPECIFIER:
	case NODE_TYPE_SPECIFIER_LIST:
	case NODE_POINTER:
	case NODE_TYPE_DECLARATOR:
	case NODE_ENUMERATOR:
	case NODE_ENUM:
	case NODE_STRUCT_OR_VARIANT_DECLARATION:
	case NODE_UNARY_EXPRESSION:
		return -EPERM;

	case NODE_UNKNOWN:
	default:
		BT_CPPLOGE_SPEC(currentCtfScanner->logger, "Unknown node type: node-type={}", parent->type);
		return -EINVAL;
	}
	return 0;
}

static int reparent_field_class_alias(struct ctf_node *node, struct ctf_node *parent)
{
	switch (parent->type) {
	case NODE_ROOT:
		_bt_list_splice_tail(&node->tmp_head, &parent->u.root.declaration_list);
		break;
	case NODE_EVENT:
		_bt_list_splice_tail(&node->tmp_head, &parent->u.event.declaration_list);
		break;
	case NODE_STREAM:
		_bt_list_splice_tail(&node->tmp_head, &parent->u.stream.declaration_list);
		break;
	case NODE_ENV:
		_bt_list_splice_tail(&node->tmp_head, &parent->u.env.declaration_list);
		break;
	case NODE_TRACE:
		_bt_list_splice_tail(&node->tmp_head, &parent->u.trace.declaration_list);
		break;
	case NODE_CLOCK:
		_bt_list_splice_tail(&node->tmp_head, &parent->u.clock.declaration_list);
		break;
	case NODE_CALLSITE:
		_bt_list_splice_tail(&node->tmp_head, &parent->u.callsite.declaration_list);
		break;
	case NODE_VARIANT:
		_bt_list_splice_tail(&node->tmp_head, &parent->u.variant.declaration_list);
		break;
	case NODE_STRUCT:
		_bt_list_splice_tail(&node->tmp_head, &parent->u._struct.declaration_list);
		break;

	case NODE_FLOATING_POINT:
	case NODE_INTEGER:
	case NODE_STRING:
	case NODE_CTF_EXPRESSION:
	case NODE_TYPEDEF:
	case NODE_TYPEALIAS_TARGET:
	case NODE_TYPEALIAS_ALIAS:
	case NODE_TYPEALIAS:
	case NODE_TYPE_SPECIFIER:
	case NODE_TYPE_SPECIFIER_LIST:
	case NODE_POINTER:
	case NODE_TYPE_DECLARATOR:
	case NODE_ENUMERATOR:
	case NODE_ENUM:
	case NODE_STRUCT_OR_VARIANT_DECLARATION:
	case NODE_UNARY_EXPRESSION:
		return -EPERM;

	case NODE_UNKNOWN:
	default:
		BT_CPPLOGE_SPEC(currentCtfScanner->logger, "Unknown node type: node-type={}", parent->type);
		return -EINVAL;
	}
	return 0;
}

static int reparent_field_class_specifier(struct ctf_node *node,
				   struct ctf_node *parent)
{
	switch (parent->type) {
	case NODE_TYPE_SPECIFIER_LIST:
		_bt_list_splice_tail(&node->tmp_head, &parent->u.field_class_specifier_list.head);
		break;

	case NODE_TYPE_SPECIFIER:
	case NODE_EVENT:
	case NODE_STREAM:
	case NODE_ENV:
	case NODE_TRACE:
	case NODE_CLOCK:
	case NODE_CALLSITE:
	case NODE_VARIANT:
	case NODE_STRUCT:
	case NODE_TYPEDEF:
	case NODE_TYPEALIAS_TARGET:
	case NODE_TYPEALIAS_ALIAS:
	case NODE_TYPE_DECLARATOR:
	case NODE_ENUM:
	case NODE_STRUCT_OR_VARIANT_DECLARATION:
	case NODE_TYPEALIAS:
	case NODE_FLOATING_POINT:
	case NODE_INTEGER:
	case NODE_STRING:
	case NODE_CTF_EXPRESSION:
	case NODE_POINTER:
	case NODE_ENUMERATOR:
	case NODE_UNARY_EXPRESSION:
		return -EPERM;

	case NODE_UNKNOWN:
	default:
		BT_CPPLOGE_SPEC(currentCtfScanner->logger, "Unknown node type: node-type={}", parent->type);
		return -EINVAL;
	}
	return 0;
}

static int reparent_field_class_specifier_list(struct ctf_node *node,
					struct ctf_node *parent)
{
	switch (parent->type) {
	case NODE_ROOT:
		bt_list_add_tail(&node->siblings, &parent->u.root.declaration_list);
		break;
	case NODE_EVENT:
		bt_list_add_tail(&node->siblings, &parent->u.event.declaration_list);
		break;
	case NODE_STREAM:
		bt_list_add_tail(&node->siblings, &parent->u.stream.declaration_list);
		break;
	case NODE_ENV:
		bt_list_add_tail(&node->siblings, &parent->u.env.declaration_list);
		break;
	case NODE_TRACE:
		bt_list_add_tail(&node->siblings, &parent->u.trace.declaration_list);
		break;
	case NODE_CLOCK:
		bt_list_add_tail(&node->siblings, &parent->u.clock.declaration_list);
		break;
	case NODE_CALLSITE:
		bt_list_add_tail(&node->siblings, &parent->u.callsite.declaration_list);
		break;
	case NODE_VARIANT:
		bt_list_add_tail(&node->siblings, &parent->u.variant.declaration_list);
		break;
	case NODE_STRUCT:
		bt_list_add_tail(&node->siblings, &parent->u._struct.declaration_list);
		break;
	case NODE_TYPEDEF:
		parent->u.field_class_def.field_class_specifier_list = node;
		break;
	case NODE_TYPEALIAS_TARGET:
		parent->u.field_class_alias_target.field_class_specifier_list = node;
		break;
	case NODE_TYPEALIAS_ALIAS:
		parent->u.field_class_alias_name.field_class_specifier_list = node;
		break;
	case NODE_ENUM:
		parent->u._enum.container_field_class = node;
		break;
	case NODE_STRUCT_OR_VARIANT_DECLARATION:
		parent->u.struct_or_variant_declaration.field_class_specifier_list = node;
		break;
	case NODE_TYPE_DECLARATOR:
	case NODE_TYPE_SPECIFIER:
	case NODE_TYPEALIAS:
	case NODE_FLOATING_POINT:
	case NODE_INTEGER:
	case NODE_STRING:
	case NODE_CTF_EXPRESSION:
	case NODE_POINTER:
	case NODE_ENUMERATOR:
	case NODE_UNARY_EXPRESSION:
		return -EPERM;

	case NODE_UNKNOWN:
	default:
		BT_CPPLOGE_SPEC(currentCtfScanner->logger, "Unknown node type: node-type={}", parent->type);
		return -EINVAL;
	}
	return 0;
}

static int reparent_field_class_declarator(struct ctf_node *node,
				    struct ctf_node *parent)
{
	switch (parent->type) {
	case NODE_TYPE_DECLARATOR:
		parent->u.field_class_declarator.type = TYPEDEC_NESTED;
		parent->u.field_class_declarator.u.nested.field_class_declarator = node;
		break;
	case NODE_STRUCT_OR_VARIANT_DECLARATION:
		_bt_list_splice_tail(&node->tmp_head, &parent->u.struct_or_variant_declaration.field_class_declarators);
		break;
	case NODE_TYPEDEF:
		_bt_list_splice_tail(&node->tmp_head, &parent->u.field_class_def.field_class_declarators);
		break;
	case NODE_TYPEALIAS_TARGET:
		_bt_list_splice_tail(&node->tmp_head, &parent->u.field_class_alias_target.field_class_declarators);
		break;
	case NODE_TYPEALIAS_ALIAS:
		_bt_list_splice_tail(&node->tmp_head, &parent->u.field_class_alias_name.field_class_declarators);
		break;

	case NODE_ROOT:
	case NODE_EVENT:
	case NODE_STREAM:
	case NODE_ENV:
	case NODE_TRACE:
	case NODE_CLOCK:
	case NODE_CALLSITE:
	case NODE_VARIANT:
	case NODE_STRUCT:
	case NODE_TYPEALIAS:
	case NODE_ENUM:
	case NODE_FLOATING_POINT:
	case NODE_INTEGER:
	case NODE_STRING:
	case NODE_CTF_EXPRESSION:
	case NODE_TYPE_SPECIFIER:
	case NODE_TYPE_SPECIFIER_LIST:
	case NODE_POINTER:
	case NODE_ENUMERATOR:
	case NODE_UNARY_EXPRESSION:
		return -EPERM;

	case NODE_UNKNOWN:
	default:
		BT_CPPLOGE_SPEC(currentCtfScanner->logger, "Unknown node type: node-type={}", parent->type);
		return -EINVAL;
	}
	return 0;
}

/*
 * set_parent_node
 *
 * Link node to parent. Returns 0 on success, -EPERM if it is not permitted to
 * create the link declared by the input, -ENOENT if node or parent is NULL,
 * -EINVAL if there is an internal structure problem.
 */
static int set_parent_node(struct ctf_node *node,
			 struct ctf_node *parent)
{
	if (!node || !parent)
		return -ENOENT;

	/* Note: Linking to parent will be done only by an external visitor */

	switch (node->type) {
	case NODE_ROOT:
		BT_CPPLOGE_SPEC(currentCtfScanner->logger, "Trying to reparent root node.");
		return -EINVAL;

	case NODE_EVENT:
		if (parent->type == NODE_ROOT) {
			_bt_list_splice_tail(&node->tmp_head, &parent->u.root.event);
		} else {
			return -EPERM;
		}
		break;
	case NODE_STREAM:
		if (parent->type == NODE_ROOT) {
			_bt_list_splice_tail(&node->tmp_head, &parent->u.root.stream);
		} else {
			return -EPERM;
		}
		break;
	case NODE_ENV:
		if (parent->type == NODE_ROOT) {
			_bt_list_splice_tail(&node->tmp_head, &parent->u.root.env);
		} else {
			return -EPERM;
		}
		break;
	case NODE_TRACE:
		if (parent->type == NODE_ROOT) {
			_bt_list_splice_tail(&node->tmp_head, &parent->u.root.trace);
		} else {
			return -EPERM;
		}
		break;
	case NODE_CLOCK:
		if (parent->type == NODE_ROOT) {
			_bt_list_splice_tail(&node->tmp_head, &parent->u.root.clock);
		} else {
			return -EPERM;
		}
		break;
	case NODE_CALLSITE:
		if (parent->type == NODE_ROOT) {
			_bt_list_splice_tail(&node->tmp_head, &parent->u.root.callsite);
		} else {
			return -EPERM;
		}
		break;

	case NODE_CTF_EXPRESSION:
		return reparent_ctf_expression(node, parent);
	case NODE_UNARY_EXPRESSION:
		if (parent->type == NODE_TYPE_DECLARATOR)
			parent->u.field_class_declarator.bitfield_len = node;
		else
			return -EPERM;
		break;

	case NODE_TYPEDEF:
		return reparent_typedef(node, parent);
	case NODE_TYPEALIAS_TARGET:
		if (parent->type == NODE_TYPEALIAS)
			parent->u.field_class_alias.target = node;
		else
			return -EINVAL;
		/* fall-through */
	case NODE_TYPEALIAS_ALIAS:
		if (parent->type == NODE_TYPEALIAS)
			parent->u.field_class_alias.alias = node;
		else
			return -EINVAL;
		/* fall-through */
	case NODE_TYPEALIAS:
		return reparent_field_class_alias(node, parent);

	case NODE_POINTER:
		if (parent->type == NODE_TYPE_DECLARATOR) {
			_bt_list_splice_tail(&node->tmp_head, &parent->u.field_class_declarator.pointers);
		} else
			return -EPERM;
		break;
	case NODE_TYPE_DECLARATOR:
		return reparent_field_class_declarator(node, parent);

	case NODE_TYPE_SPECIFIER_LIST:
		return reparent_field_class_specifier_list(node, parent);

	case NODE_TYPE_SPECIFIER:
		return reparent_field_class_specifier(node, parent);

	case NODE_FLOATING_POINT:
	case NODE_INTEGER:
	case NODE_STRING:
	case NODE_ENUM:
	case NODE_VARIANT:
	case NODE_STRUCT:
		return -EINVAL;	/* Dealt with internally within grammar */

	case NODE_ENUMERATOR:
		if (parent->type == NODE_ENUM) {
			_bt_list_splice_tail(&node->tmp_head, &parent->u._enum.enumerator_list);
		} else {
			return -EPERM;
		}
		break;
	case NODE_STRUCT_OR_VARIANT_DECLARATION:
		switch (parent->type) {
		case NODE_STRUCT:
			_bt_list_splice_tail(&node->tmp_head, &parent->u._struct.declaration_list);
			break;
		case NODE_VARIANT:
			_bt_list_splice_tail(&node->tmp_head, &parent->u.variant.declaration_list);
			break;
		default:
			return -EINVAL;
		}
		break;

	case NODE_UNKNOWN:
	default:
		BT_CPPLOGE_SPEC(currentCtfScanner->logger, "Unknown node type: node-type={}", parent->type);
		return -EINVAL;
	}
	return 0;
}

static
void yyerror(struct ctf_scanner *scanner, yyscan_t yyscanner, const char *str)
{
	_BT_CPPLOGE_APPEND_CAUSE_LINENO(currentCtfScanner->logger,
		yyget_lineno(scanner->scanner),
		"{}: token=\"{}\"", str, yyget_text(scanner->scanner));
}

#define reparent_error(scanner, str)				\
do {								\
	yyerror(scanner, scanner->scanner, YY_("reparent_error: " str)); \
	YYERROR;						\
} while (0)

static struct ctf_ast *ctf_ast_alloc(struct ctf_scanner *scanner)
{
	struct ctf_ast *ast;

	ast = (ctf_ast *) objstack_alloc(scanner->objstack, sizeof(*ast));
	if (!ast)
		return NULL;
	ast->root.type = NODE_ROOT;
	BT_INIT_LIST_HEAD(&ast->root.tmp_head);
	BT_INIT_LIST_HEAD(&ast->root.u.root.declaration_list);
	BT_INIT_LIST_HEAD(&ast->root.u.root.trace);
	BT_INIT_LIST_HEAD(&ast->root.u.root.env);
	BT_INIT_LIST_HEAD(&ast->root.u.root.stream);
	BT_INIT_LIST_HEAD(&ast->root.u.root.event);
	BT_INIT_LIST_HEAD(&ast->root.u.root.clock);
	BT_INIT_LIST_HEAD(&ast->root.u.root.callsite);
	return ast;
}

int ctf_scanner_append_ast(struct ctf_scanner *scanner, FILE *input)
{
	/* Start processing new stream */
	struct ClearCurrentCtfScanner {
		~ClearCurrentCtfScanner() {
			currentCtfScanner = nullptr;
		}
	} clearMoiLa;

	currentCtfScanner = scanner;
	yyrestart(input, scanner->scanner);
	return yyparse(scanner, scanner->scanner);
}

struct ctf_scanner *ctf_scanner_alloc(const bt2c::Logger &parentLogger)
{
	ctf_scanner *scanner = new ctf_scanner {parentLogger};
	int ret = yylex_init_extra(scanner, &scanner->scanner);
	if (ret) {
		BT_CPPLOGE_SPEC(scanner->logger, "yylex_init_extra() failed: ret={}", ret);
		goto cleanup_scanner;
	}
	scanner->objstack = objstack_create(scanner->logger);
	if (!scanner->objstack)
		goto cleanup_lexer;
	scanner->ast = ctf_ast_alloc(scanner);
	if (!scanner->ast)
		goto cleanup_objstack;
	init_scope(&scanner->root_scope, NULL);
	scanner->cs = &scanner->root_scope;

	return scanner;

cleanup_objstack:
	objstack_destroy(scanner->objstack);
cleanup_lexer:
	ret = yylex_destroy(scanner->scanner);
	if (!ret)
		BT_CPPLOGE_SPEC(scanner->logger, "yylex_destroy() failed: scanner-addr={}, ret={}",
			fmt::ptr(scanner), ret);
cleanup_scanner:
	delete scanner;
	return NULL;
}

void ctf_scanner_free(struct ctf_scanner *scanner)
{
	int ret;

	if (!scanner)
		return;

	struct ctf_scanner_scope *scope = scanner->cs;

	do {
		struct ctf_scanner_scope *parent = scope->parent;
		finalize_scope(scope);

		/*
		 * The root scope is allocated within the ctf_scanner structure,
		 * do doesn't need freeing.  All others are allocated on their
		 * own.
		 */
		if (scope != &scanner->root_scope)
			free(scope);

		scope = parent;
	} while (scope);

	objstack_destroy(scanner->objstack);
	ret = yylex_destroy(scanner->scanner);
	if (ret)
		BT_CPPLOGE_SPEC(currentCtfScanner->logger, "yylex_destroy() failed: scanner-addr={}, ret={}",
			fmt::ptr(scanner), ret);
	delete scanner;
}

/*
 * The bison-provided version of strlen (yystrlen) generates a benign
 * -Wnull-dereference warning.  That version is used when building on cygwin,
 * for example, but you can also enable it by hand (to test) by removing the
 * preprocessor conditional around it.
 *
 * Define yystrlen such that it will always use strlen.  As far as we know,
 * strlen provided by all the platforms we use is reliable.
 */
#define yystrlen strlen


#line 1122 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"

# ifndef YY_CAST
#  ifdef __cplusplus
#   define YY_CAST(Type, Val) static_cast<Type> (Val)
#   define YY_REINTERPRET_CAST(Type, Val) reinterpret_cast<Type> (Val)
#  else
#   define YY_CAST(Type, Val) ((Type) (Val))
#   define YY_REINTERPRET_CAST(Type, Val) ((Type) (Val))
#  endif
# endif
# ifndef YY_NULLPTR
#  if defined __cplusplus
#   if 201103L <= __cplusplus
#    define YY_NULLPTR nullptr
#   else
#    define YY_NULLPTR 0
#   endif
#  else
#   define YY_NULLPTR ((void*)0)
#  endif
# endif

/* Use api.header.include to #include this header
   instead of duplicating it here.  */
#ifndef YY_YY_PLUGINS_CTF_COMMON_SRC_METADATA_TSDL_PARSER_HPP_INCLUDED
# define YY_YY_PLUGINS_CTF_COMMON_SRC_METADATA_TSDL_PARSER_HPP_INCLUDED
/* Debug traces.  */
#ifndef YYDEBUG
# define YYDEBUG 1
#endif
#if YYDEBUG
extern int yydebug;
#endif
/* "%code requires" blocks.  */
#line 1056 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"

#ifndef ALLOW_INCLUDE_PARSER_H
# error "Don't include parser.h directly, include parser-wrap.h instead."
#endif

#include "plugins/ctf/common/src/metadata/tsdl/scanner.hpp"

#line 1165 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"

/* Token kinds.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
  enum yytokentype
  {
    YYEMPTY = -2,
    YYEOF = 0,                     /* "end of file"  */
    YYerror = 256,                 /* error  */
    YYUNDEF = 257,                 /* "invalid token"  */
    CTF_INTEGER_LITERAL = 258,     /* CTF_INTEGER_LITERAL  */
    CTF_STRING_LITERAL = 259,      /* CTF_STRING_LITERAL  */
    CTF_CHARACTER_LITERAL = 260,   /* CTF_CHARACTER_LITERAL  */
    CTF_LSBRAC = 261,              /* CTF_LSBRAC  */
    CTF_RSBRAC = 262,              /* CTF_RSBRAC  */
    CTF_LPAREN = 263,              /* CTF_LPAREN  */
    CTF_RPAREN = 264,              /* CTF_RPAREN  */
    CTF_LBRAC = 265,               /* CTF_LBRAC  */
    CTF_RBRAC = 266,               /* CTF_RBRAC  */
    CTF_RARROW = 267,              /* CTF_RARROW  */
    CTF_STAR = 268,                /* CTF_STAR  */
    CTF_PLUS = 269,                /* CTF_PLUS  */
    CTF_MINUS = 270,               /* CTF_MINUS  */
    CTF_LT = 271,                  /* CTF_LT  */
    CTF_GT = 272,                  /* CTF_GT  */
    CTF_TYPEASSIGN = 273,          /* CTF_TYPEASSIGN  */
    CTF_COLON = 274,               /* CTF_COLON  */
    CTF_SEMICOLON = 275,           /* CTF_SEMICOLON  */
    CTF_DOTDOTDOT = 276,           /* CTF_DOTDOTDOT  */
    CTF_DOT = 277,                 /* CTF_DOT  */
    CTF_EQUAL = 278,               /* CTF_EQUAL  */
    CTF_COMMA = 279,               /* CTF_COMMA  */
    CTF_CONST = 280,               /* CTF_CONST  */
    CTF_CHAR = 281,                /* CTF_CHAR  */
    CTF_DOUBLE = 282,              /* CTF_DOUBLE  */
    CTF_ENUM = 283,                /* CTF_ENUM  */
    CTF_ENV = 284,                 /* CTF_ENV  */
    CTF_EVENT = 285,               /* CTF_EVENT  */
    CTF_FLOATING_POINT = 286,      /* CTF_FLOATING_POINT  */
    CTF_FLOAT = 287,               /* CTF_FLOAT  */
    CTF_INTEGER = 288,             /* CTF_INTEGER  */
    CTF_INT = 289,                 /* CTF_INT  */
    CTF_LONG = 290,                /* CTF_LONG  */
    CTF_SHORT = 291,               /* CTF_SHORT  */
    CTF_SIGNED = 292,              /* CTF_SIGNED  */
    CTF_STREAM = 293,              /* CTF_STREAM  */
    CTF_STRING = 294,              /* CTF_STRING  */
    CTF_STRUCT = 295,              /* CTF_STRUCT  */
    CTF_TRACE = 296,               /* CTF_TRACE  */
    CTF_CALLSITE = 297,            /* CTF_CALLSITE  */
    CTF_CLOCK = 298,               /* CTF_CLOCK  */
    CTF_TYPEALIAS = 299,           /* CTF_TYPEALIAS  */
    CTF_TYPEDEF = 300,             /* CTF_TYPEDEF  */
    CTF_UNSIGNED = 301,            /* CTF_UNSIGNED  */
    CTF_VARIANT = 302,             /* CTF_VARIANT  */
    CTF_VOID = 303,                /* CTF_VOID  */
    CTF_BOOL = 304,                /* CTF_BOOL  */
    CTF_COMPLEX = 305,             /* CTF_COMPLEX  */
    CTF_IMAGINARY = 306,           /* CTF_IMAGINARY  */
    CTF_TOK_ALIGN = 307,           /* CTF_TOK_ALIGN  */
    IDENTIFIER = 308,              /* IDENTIFIER  */
    ID_TYPE = 309,                 /* ID_TYPE  */
    CTF_ERROR = 310                /* CTF_ERROR  */
  };
  typedef enum yytokentype yytoken_kind_t;
#endif
/* Token kinds.  */
#define YYEMPTY -2
#define YYEOF 0
#define YYerror 256
#define YYUNDEF 257
#define CTF_INTEGER_LITERAL 258
#define CTF_STRING_LITERAL 259
#define CTF_CHARACTER_LITERAL 260
#define CTF_LSBRAC 261
#define CTF_RSBRAC 262
#define CTF_LPAREN 263
#define CTF_RPAREN 264
#define CTF_LBRAC 265
#define CTF_RBRAC 266
#define CTF_RARROW 267
#define CTF_STAR 268
#define CTF_PLUS 269
#define CTF_MINUS 270
#define CTF_LT 271
#define CTF_GT 272
#define CTF_TYPEASSIGN 273
#define CTF_COLON 274
#define CTF_SEMICOLON 275
#define CTF_DOTDOTDOT 276
#define CTF_DOT 277
#define CTF_EQUAL 278
#define CTF_COMMA 279
#define CTF_CONST 280
#define CTF_CHAR 281
#define CTF_DOUBLE 282
#define CTF_ENUM 283
#define CTF_ENV 284
#define CTF_EVENT 285
#define CTF_FLOATING_POINT 286
#define CTF_FLOAT 287
#define CTF_INTEGER 288
#define CTF_INT 289
#define CTF_LONG 290
#define CTF_SHORT 291
#define CTF_SIGNED 292
#define CTF_STREAM 293
#define CTF_STRING 294
#define CTF_STRUCT 295
#define CTF_TRACE 296
#define CTF_CALLSITE 297
#define CTF_CLOCK 298
#define CTF_TYPEALIAS 299
#define CTF_TYPEDEF 300
#define CTF_UNSIGNED 301
#define CTF_VARIANT 302
#define CTF_VOID 303
#define CTF_BOOL 304
#define CTF_COMPLEX 305
#define CTF_IMAGINARY 306
#define CTF_TOK_ALIGN 307
#define IDENTIFIER 308
#define ID_TYPE 309
#define CTF_ERROR 310

/* Value type.  */
#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED
union YYSTYPE
{
#line 1090 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"

	long long ll;
	unsigned long long ull;
	char c;
	char *s;
	struct ctf_node *n;

#line 1303 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"

};
typedef union YYSTYPE YYSTYPE;
# define YYSTYPE_IS_TRIVIAL 1
# define YYSTYPE_IS_DECLARED 1
#endif




int yyparse (struct ctf_scanner *scanner, yyscan_t yyscanner);

/* "%code provides" blocks.  */
#line 1064 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"

	void setstring(struct ctf_scanner *scanner, YYSTYPE *lvalp, const char *src);

	int import_string(struct ctf_scanner *scanner, YYSTYPE *lvalp, const char *src, char delim);

#line 1323 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"

#endif /* !YY_YY_PLUGINS_CTF_COMMON_SRC_METADATA_TSDL_PARSER_HPP_INCLUDED  */
/* Symbol kind.  */
enum yysymbol_kind_t
{
  YYSYMBOL_YYEMPTY = -2,
  YYSYMBOL_YYEOF = 0,                      /* "end of file"  */
  YYSYMBOL_YYerror = 1,                    /* error  */
  YYSYMBOL_YYUNDEF = 2,                    /* "invalid token"  */
  YYSYMBOL_CTF_INTEGER_LITERAL = 3,        /* CTF_INTEGER_LITERAL  */
  YYSYMBOL_CTF_STRING_LITERAL = 4,         /* CTF_STRING_LITERAL  */
  YYSYMBOL_CTF_CHARACTER_LITERAL = 5,      /* CTF_CHARACTER_LITERAL  */
  YYSYMBOL_CTF_LSBRAC = 6,                 /* CTF_LSBRAC  */
  YYSYMBOL_CTF_RSBRAC = 7,                 /* CTF_RSBRAC  */
  YYSYMBOL_CTF_LPAREN = 8,                 /* CTF_LPAREN  */
  YYSYMBOL_CTF_RPAREN = 9,                 /* CTF_RPAREN  */
  YYSYMBOL_CTF_LBRAC = 10,                 /* CTF_LBRAC  */
  YYSYMBOL_CTF_RBRAC = 11,                 /* CTF_RBRAC  */
  YYSYMBOL_CTF_RARROW = 12,                /* CTF_RARROW  */
  YYSYMBOL_CTF_STAR = 13,                  /* CTF_STAR  */
  YYSYMBOL_CTF_PLUS = 14,                  /* CTF_PLUS  */
  YYSYMBOL_CTF_MINUS = 15,                 /* CTF_MINUS  */
  YYSYMBOL_CTF_LT = 16,                    /* CTF_LT  */
  YYSYMBOL_CTF_GT = 17,                    /* CTF_GT  */
  YYSYMBOL_CTF_TYPEASSIGN = 18,            /* CTF_TYPEASSIGN  */
  YYSYMBOL_CTF_COLON = 19,                 /* CTF_COLON  */
  YYSYMBOL_CTF_SEMICOLON = 20,             /* CTF_SEMICOLON  */
  YYSYMBOL_CTF_DOTDOTDOT = 21,             /* CTF_DOTDOTDOT  */
  YYSYMBOL_CTF_DOT = 22,                   /* CTF_DOT  */
  YYSYMBOL_CTF_EQUAL = 23,                 /* CTF_EQUAL  */
  YYSYMBOL_CTF_COMMA = 24,                 /* CTF_COMMA  */
  YYSYMBOL_CTF_CONST = 25,                 /* CTF_CONST  */
  YYSYMBOL_CTF_CHAR = 26,                  /* CTF_CHAR  */
  YYSYMBOL_CTF_DOUBLE = 27,                /* CTF_DOUBLE  */
  YYSYMBOL_CTF_ENUM = 28,                  /* CTF_ENUM  */
  YYSYMBOL_CTF_ENV = 29,                   /* CTF_ENV  */
  YYSYMBOL_CTF_EVENT = 30,                 /* CTF_EVENT  */
  YYSYMBOL_CTF_FLOATING_POINT = 31,        /* CTF_FLOATING_POINT  */
  YYSYMBOL_CTF_FLOAT = 32,                 /* CTF_FLOAT  */
  YYSYMBOL_CTF_INTEGER = 33,               /* CTF_INTEGER  */
  YYSYMBOL_CTF_INT = 34,                   /* CTF_INT  */
  YYSYMBOL_CTF_LONG = 35,                  /* CTF_LONG  */
  YYSYMBOL_CTF_SHORT = 36,                 /* CTF_SHORT  */
  YYSYMBOL_CTF_SIGNED = 37,                /* CTF_SIGNED  */
  YYSYMBOL_CTF_STREAM = 38,                /* CTF_STREAM  */
  YYSYMBOL_CTF_STRING = 39,                /* CTF_STRING  */
  YYSYMBOL_CTF_STRUCT = 40,                /* CTF_STRUCT  */
  YYSYMBOL_CTF_TRACE = 41,                 /* CTF_TRACE  */
  YYSYMBOL_CTF_CALLSITE = 42,              /* CTF_CALLSITE  */
  YYSYMBOL_CTF_CLOCK = 43,                 /* CTF_CLOCK  */
  YYSYMBOL_CTF_TYPEALIAS = 44,             /* CTF_TYPEALIAS  */
  YYSYMBOL_CTF_TYPEDEF = 45,               /* CTF_TYPEDEF  */
  YYSYMBOL_CTF_UNSIGNED = 46,              /* CTF_UNSIGNED  */
  YYSYMBOL_CTF_VARIANT = 47,               /* CTF_VARIANT  */
  YYSYMBOL_CTF_VOID = 48,                  /* CTF_VOID  */
  YYSYMBOL_CTF_BOOL = 49,                  /* CTF_BOOL  */
  YYSYMBOL_CTF_COMPLEX = 50,               /* CTF_COMPLEX  */
  YYSYMBOL_CTF_IMAGINARY = 51,             /* CTF_IMAGINARY  */
  YYSYMBOL_CTF_TOK_ALIGN = 52,             /* CTF_TOK_ALIGN  */
  YYSYMBOL_IDENTIFIER = 53,                /* IDENTIFIER  */
  YYSYMBOL_ID_TYPE = 54,                   /* ID_TYPE  */
  YYSYMBOL_CTF_ERROR = 55,                 /* CTF_ERROR  */
  YYSYMBOL_YYACCEPT = 56,                  /* $accept  */
  YYSYMBOL_file = 57,                      /* file  */
  YYSYMBOL_keywords = 58,                  /* keywords  */
  YYSYMBOL_postfix_expression = 59,        /* postfix_expression  */
  YYSYMBOL_unary_expression = 60,          /* unary_expression  */
  YYSYMBOL_unary_expression_or_range = 61, /* unary_expression_or_range  */
  YYSYMBOL_declaration = 62,               /* declaration  */
  YYSYMBOL_event_declaration = 63,         /* event_declaration  */
  YYSYMBOL_event_declaration_begin = 64,   /* event_declaration_begin  */
  YYSYMBOL_event_declaration_end = 65,     /* event_declaration_end  */
  YYSYMBOL_stream_declaration = 66,        /* stream_declaration  */
  YYSYMBOL_stream_declaration_begin = 67,  /* stream_declaration_begin  */
  YYSYMBOL_stream_declaration_end = 68,    /* stream_declaration_end  */
  YYSYMBOL_env_declaration = 69,           /* env_declaration  */
  YYSYMBOL_env_declaration_begin = 70,     /* env_declaration_begin  */
  YYSYMBOL_env_declaration_end = 71,       /* env_declaration_end  */
  YYSYMBOL_trace_declaration = 72,         /* trace_declaration  */
  YYSYMBOL_trace_declaration_begin = 73,   /* trace_declaration_begin  */
  YYSYMBOL_trace_declaration_end = 74,     /* trace_declaration_end  */
  YYSYMBOL_clock_declaration = 75,         /* clock_declaration  */
  YYSYMBOL_clock_declaration_begin = 76,   /* clock_declaration_begin  */
  YYSYMBOL_clock_declaration_end = 77,     /* clock_declaration_end  */
  YYSYMBOL_callsite_declaration = 78,      /* callsite_declaration  */
  YYSYMBOL_callsite_declaration_begin = 79, /* callsite_declaration_begin  */
  YYSYMBOL_callsite_declaration_end = 80,  /* callsite_declaration_end  */
  YYSYMBOL_integer_declaration_specifiers = 81, /* integer_declaration_specifiers  */
  YYSYMBOL_declaration_specifiers = 82,    /* declaration_specifiers  */
  YYSYMBOL_field_class_declarator_list = 83, /* field_class_declarator_list  */
  YYSYMBOL_integer_field_class_specifier = 84, /* integer_field_class_specifier  */
  YYSYMBOL_field_class_specifier = 85,     /* field_class_specifier  */
  YYSYMBOL_struct_class_specifier = 86,    /* struct_class_specifier  */
  YYSYMBOL_struct_declaration_begin = 87,  /* struct_declaration_begin  */
  YYSYMBOL_struct_declaration_end = 88,    /* struct_declaration_end  */
  YYSYMBOL_variant_field_class_specifier = 89, /* variant_field_class_specifier  */
  YYSYMBOL_variant_declaration_begin = 90, /* variant_declaration_begin  */
  YYSYMBOL_variant_declaration_end = 91,   /* variant_declaration_end  */
  YYSYMBOL_enum_field_class_specifier = 92, /* enum_field_class_specifier  */
  YYSYMBOL_struct_or_variant_declaration_list = 93, /* struct_or_variant_declaration_list  */
  YYSYMBOL_struct_or_variant_declaration = 94, /* struct_or_variant_declaration  */
  YYSYMBOL_alias_declaration_specifiers = 95, /* alias_declaration_specifiers  */
  YYSYMBOL_struct_or_variant_declarator_list = 96, /* struct_or_variant_declarator_list  */
  YYSYMBOL_struct_or_variant_declarator = 97, /* struct_or_variant_declarator  */
  YYSYMBOL_enumerator_list = 98,           /* enumerator_list  */
  YYSYMBOL_enumerator = 99,                /* enumerator  */
  YYSYMBOL_abstract_declarator_list = 100, /* abstract_declarator_list  */
  YYSYMBOL_abstract_declarator = 101,      /* abstract_declarator  */
  YYSYMBOL_direct_abstract_declarator = 102, /* direct_abstract_declarator  */
  YYSYMBOL_alias_abstract_declarator_list = 103, /* alias_abstract_declarator_list  */
  YYSYMBOL_alias_abstract_declarator = 104, /* alias_abstract_declarator  */
  YYSYMBOL_direct_alias_abstract_declarator = 105, /* direct_alias_abstract_declarator  */
  YYSYMBOL_declarator = 106,               /* declarator  */
  YYSYMBOL_direct_declarator = 107,        /* direct_declarator  */
  YYSYMBOL_field_class_declarator = 108,   /* field_class_declarator  */
  YYSYMBOL_direct_field_class_declarator = 109, /* direct_field_class_declarator  */
  YYSYMBOL_pointer = 110,                  /* pointer  */
  YYSYMBOL_type_qualifier_list = 111,      /* type_qualifier_list  */
  YYSYMBOL_ctf_assignment_expression_list = 112, /* ctf_assignment_expression_list  */
  YYSYMBOL_ctf_assignment_expression = 113 /* ctf_assignment_expression  */
};
typedef enum yysymbol_kind_t yysymbol_kind_t;




#ifdef short
# undef short
#endif

/* On compilers that do not define __PTRDIFF_MAX__ etc., make sure
   <limits.h> and (if available) <stdint.h> are included
   so that the code can choose integer types of a good width.  */

#ifndef __PTRDIFF_MAX__
# include <limits.h> /* INFRINGES ON USER NAME SPACE */
# if defined __STDC_VERSION__ && 199901 <= __STDC_VERSION__
#  include <stdint.h> /* INFRINGES ON USER NAME SPACE */
#  define YY_STDINT_H
# endif
#endif

/* Narrow types that promote to a signed type and that can represent a
   signed or unsigned integer of at least N bits.  In tables they can
   save space and decrease cache pressure.  Promoting to a signed type
   helps avoid bugs in integer arithmetic.  */

#ifdef __INT_LEAST8_MAX__
typedef __INT_LEAST8_TYPE__ yytype_int8;
#elif defined YY_STDINT_H
typedef int_least8_t yytype_int8;
#else
typedef signed char yytype_int8;
#endif

#ifdef __INT_LEAST16_MAX__
typedef __INT_LEAST16_TYPE__ yytype_int16;
#elif defined YY_STDINT_H
typedef int_least16_t yytype_int16;
#else
typedef short yytype_int16;
#endif

/* Work around bug in HP-UX 11.23, which defines these macros
   incorrectly for preprocessor constants.  This workaround can likely
   be removed in 2023, as HPE has promised support for HP-UX 11.23
   (aka HP-UX 11i v2) only through the end of 2022; see Table 2 of
   <https://h20195.www2.hpe.com/V2/getpdf.aspx/4AA4-7673ENW.pdf>.  */
#ifdef __hpux
# undef UINT_LEAST8_MAX
# undef UINT_LEAST16_MAX
# define UINT_LEAST8_MAX 255
# define UINT_LEAST16_MAX 65535
#endif

#if defined __UINT_LEAST8_MAX__ && __UINT_LEAST8_MAX__ <= __INT_MAX__
typedef __UINT_LEAST8_TYPE__ yytype_uint8;
#elif (!defined __UINT_LEAST8_MAX__ && defined YY_STDINT_H \
       && UINT_LEAST8_MAX <= INT_MAX)
typedef uint_least8_t yytype_uint8;
#elif !defined __UINT_LEAST8_MAX__ && UCHAR_MAX <= INT_MAX
typedef unsigned char yytype_uint8;
#else
typedef short yytype_uint8;
#endif

#if defined __UINT_LEAST16_MAX__ && __UINT_LEAST16_MAX__ <= __INT_MAX__
typedef __UINT_LEAST16_TYPE__ yytype_uint16;
#elif (!defined __UINT_LEAST16_MAX__ && defined YY_STDINT_H \
       && UINT_LEAST16_MAX <= INT_MAX)
typedef uint_least16_t yytype_uint16;
#elif !defined __UINT_LEAST16_MAX__ && USHRT_MAX <= INT_MAX
typedef unsigned short yytype_uint16;
#else
typedef int yytype_uint16;
#endif

#ifndef YYPTRDIFF_T
# if defined __PTRDIFF_TYPE__ && defined __PTRDIFF_MAX__
#  define YYPTRDIFF_T __PTRDIFF_TYPE__
#  define YYPTRDIFF_MAXIMUM __PTRDIFF_MAX__
# elif defined PTRDIFF_MAX
#  ifndef ptrdiff_t
#   include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  endif
#  define YYPTRDIFF_T ptrdiff_t
#  define YYPTRDIFF_MAXIMUM PTRDIFF_MAX
# else
#  define YYPTRDIFF_T long
#  define YYPTRDIFF_MAXIMUM LONG_MAX
# endif
#endif

#ifndef YYSIZE_T
# ifdef __SIZE_TYPE__
#  define YYSIZE_T __SIZE_TYPE__
# elif defined size_t
#  define YYSIZE_T size_t
# elif defined __STDC_VERSION__ && 199901 <= __STDC_VERSION__
#  include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  define YYSIZE_T size_t
# else
#  define YYSIZE_T unsigned
# endif
#endif

#define YYSIZE_MAXIMUM                                  \
  YY_CAST (YYPTRDIFF_T,                                 \
           (YYPTRDIFF_MAXIMUM < YY_CAST (YYSIZE_T, -1)  \
            ? YYPTRDIFF_MAXIMUM                         \
            : YY_CAST (YYSIZE_T, -1)))

#define YYSIZEOF(X) YY_CAST (YYPTRDIFF_T, sizeof (X))


/* Stored state numbers (used for stacks). */
typedef yytype_int16 yy_state_t;

/* State numbers in computations.  */
typedef int yy_state_fast_t;

#ifndef YY_
# if defined YYENABLE_NLS && YYENABLE_NLS
#  if ENABLE_NLS
#   include <libintl.h> /* INFRINGES ON USER NAME SPACE */
#   define YY_(Msgid) dgettext ("bison-runtime", Msgid)
#  endif
# endif
# ifndef YY_
#  define YY_(Msgid) Msgid
# endif
#endif


#ifndef YY_ATTRIBUTE_PURE
# if defined __GNUC__ && 2 < __GNUC__ + (96 <= __GNUC_MINOR__)
#  define YY_ATTRIBUTE_PURE __attribute__ ((__pure__))
# else
#  define YY_ATTRIBUTE_PURE
# endif
#endif

#ifndef YY_ATTRIBUTE_UNUSED
# if defined __GNUC__ && 2 < __GNUC__ + (7 <= __GNUC_MINOR__)
#  define YY_ATTRIBUTE_UNUSED __attribute__ ((__unused__))
# else
#  define YY_ATTRIBUTE_UNUSED
# endif
#endif

/* Suppress unused-variable warnings by "using" E.  */
#if ! defined lint || defined __GNUC__
# define YY_USE(E) ((void) (E))
#else
# define YY_USE(E) /* empty */
#endif

/* Suppress an incorrect diagnostic about yylval being uninitialized.  */
#if defined __GNUC__ && ! defined __ICC && 406 <= __GNUC__ * 100 + __GNUC_MINOR__
# if __GNUC__ * 100 + __GNUC_MINOR__ < 407
#  define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN                           \
    _Pragma ("GCC diagnostic push")                                     \
    _Pragma ("GCC diagnostic ignored \"-Wuninitialized\"")
# else
#  define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN                           \
    _Pragma ("GCC diagnostic push")                                     \
    _Pragma ("GCC diagnostic ignored \"-Wuninitialized\"")              \
    _Pragma ("GCC diagnostic ignored \"-Wmaybe-uninitialized\"")
# endif
# define YY_IGNORE_MAYBE_UNINITIALIZED_END      \
    _Pragma ("GCC diagnostic pop")
#else
# define YY_INITIAL_VALUE(Value) Value
#endif
#ifndef YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_END
#endif
#ifndef YY_INITIAL_VALUE
# define YY_INITIAL_VALUE(Value) /* Nothing. */
#endif

#if defined __cplusplus && defined __GNUC__ && ! defined __ICC && 6 <= __GNUC__
# define YY_IGNORE_USELESS_CAST_BEGIN                          \
    _Pragma ("GCC diagnostic push")                            \
    _Pragma ("GCC diagnostic ignored \"-Wuseless-cast\"")
# define YY_IGNORE_USELESS_CAST_END            \
    _Pragma ("GCC diagnostic pop")
#endif
#ifndef YY_IGNORE_USELESS_CAST_BEGIN
# define YY_IGNORE_USELESS_CAST_BEGIN
# define YY_IGNORE_USELESS_CAST_END
#endif


#define YY_ASSERT(E) ((void) (0 && (E)))

#if 1

/* The parser invokes alloca or malloc; define the necessary symbols.  */

# ifdef YYSTACK_USE_ALLOCA
#  if YYSTACK_USE_ALLOCA
#   ifdef __GNUC__
#    define YYSTACK_ALLOC __builtin_alloca
#   elif defined __BUILTIN_VA_ARG_INCR
#    include <alloca.h> /* INFRINGES ON USER NAME SPACE */
#   elif defined _AIX
#    define YYSTACK_ALLOC __alloca
#   elif defined _MSC_VER
#    include <malloc.h> /* INFRINGES ON USER NAME SPACE */
#    define alloca _alloca
#   else
#    define YYSTACK_ALLOC alloca
#    if ! defined _ALLOCA_H && ! defined EXIT_SUCCESS
#     include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
      /* Use EXIT_SUCCESS as a witness for stdlib.h.  */
#     ifndef EXIT_SUCCESS
#      define EXIT_SUCCESS 0
#     endif
#    endif
#   endif
#  endif
# endif

# ifdef YYSTACK_ALLOC
   /* Pacify GCC's 'empty if-body' warning.  */
#  define YYSTACK_FREE(Ptr) do { /* empty */; } while (0)
#  ifndef YYSTACK_ALLOC_MAXIMUM
    /* The OS might guarantee only one guard page at the bottom of the stack,
       and a page size can be as small as 4096 bytes.  So we cannot safely
       invoke alloca (N) if N exceeds 4096.  Use a slightly smaller number
       to allow for a few compiler-allocated temporary stack slots.  */
#   define YYSTACK_ALLOC_MAXIMUM 4032 /* reasonable circa 2006 */
#  endif
# else
#  define YYSTACK_ALLOC YYMALLOC
#  define YYSTACK_FREE YYFREE
#  ifndef YYSTACK_ALLOC_MAXIMUM
#   define YYSTACK_ALLOC_MAXIMUM YYSIZE_MAXIMUM
#  endif
#  if (defined __cplusplus && ! defined EXIT_SUCCESS \
       && ! ((defined YYMALLOC || defined malloc) \
             && (defined YYFREE || defined free)))
#   include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#   ifndef EXIT_SUCCESS
#    define EXIT_SUCCESS 0
#   endif
#  endif
#  ifndef YYMALLOC
#   define YYMALLOC malloc
#   if ! defined malloc && ! defined EXIT_SUCCESS
void *malloc (YYSIZE_T); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
#  ifndef YYFREE
#   define YYFREE free
#   if ! defined free && ! defined EXIT_SUCCESS
void free (void *); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
# endif
#endif /* 1 */

#if (! defined yyoverflow \
     && (! defined __cplusplus \
         || (defined YYSTYPE_IS_TRIVIAL && YYSTYPE_IS_TRIVIAL)))

/* A type that is properly aligned for any stack member.  */
union yyalloc
{
  yy_state_t yyss_alloc;
  YYSTYPE yyvs_alloc;
};

/* The size of the maximum gap between one aligned stack and the next.  */
# define YYSTACK_GAP_MAXIMUM (YYSIZEOF (union yyalloc) - 1)

/* The size of an array large to enough to hold all stacks, each with
   N elements.  */
# define YYSTACK_BYTES(N) \
     ((N) * (YYSIZEOF (yy_state_t) + YYSIZEOF (YYSTYPE)) \
      + YYSTACK_GAP_MAXIMUM)

# define YYCOPY_NEEDED 1

/* Relocate STACK from its old location to the new one.  The
   local variables YYSIZE and YYSTACKSIZE give the old and new number of
   elements in the stack, and YYPTR gives the new location of the
   stack.  Advance YYPTR to a properly aligned location for the next
   stack.  */
# define YYSTACK_RELOCATE(Stack_alloc, Stack)                           \
    do                                                                  \
      {                                                                 \
        YYPTRDIFF_T yynewbytes;                                         \
        YYCOPY (&yyptr->Stack_alloc, Stack, yysize);                    \
        Stack = &yyptr->Stack_alloc;                                    \
        yynewbytes = yystacksize * YYSIZEOF (*Stack) + YYSTACK_GAP_MAXIMUM; \
        yyptr += yynewbytes / YYSIZEOF (*yyptr);                        \
      }                                                                 \
    while (0)

#endif

#if defined YYCOPY_NEEDED && YYCOPY_NEEDED
/* Copy COUNT objects from SRC to DST.  The source and destination do
   not overlap.  */
# ifndef YYCOPY
#  if defined __GNUC__ && 1 < __GNUC__
#   define YYCOPY(Dst, Src, Count) \
      __builtin_memcpy (Dst, Src, YY_CAST (YYSIZE_T, (Count)) * sizeof (*(Src)))
#  else
#   define YYCOPY(Dst, Src, Count)              \
      do                                        \
        {                                       \
          YYPTRDIFF_T yyi;                      \
          for (yyi = 0; yyi < (Count); yyi++)   \
            (Dst)[yyi] = (Src)[yyi];            \
        }                                       \
      while (0)
#  endif
# endif
#endif /* !YYCOPY_NEEDED */

/* YYFINAL -- State number of the termination state.  */
#define YYFINAL  72
/* YYLAST -- Last index in YYTABLE.  */
#define YYLAST   2199

/* YYNTOKENS -- Number of terminals.  */
#define YYNTOKENS  56
/* YYNNTS -- Number of nonterminals.  */
#define YYNNTS  58
/* YYNRULES -- Number of rules.  */
#define YYNRULES  233
/* YYNSTATES -- Number of states.  */
#define YYNSTATES  444

/* YYMAXUTOK -- Last valid token kind.  */
#define YYMAXUTOK   310


/* YYTRANSLATE(TOKEN-NUM) -- Symbol number corresponding to TOKEN-NUM
   as returned by yylex, with out-of-bounds checking.  */
#define YYTRANSLATE(YYX)                                \
  (0 <= (YYX) && (YYX) <= YYMAXUTOK                     \
   ? YY_CAST (yysymbol_kind_t, yytranslate[YYX])        \
   : YYSYMBOL_YYUNDEF)

/* YYTRANSLATE[TOKEN-NUM] -- Symbol number corresponding to TOKEN-NUM
   as returned by yylex.  */
static const yytype_int8 yytranslate[] =
{
       0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     2,     3,     4,
       5,     6,     7,     8,     9,    10,    11,    12,    13,    14,
      15,    16,    17,    18,    19,    20,    21,    22,    23,    24,
      25,    26,    27,    28,    29,    30,    31,    32,    33,    34,
      35,    36,    37,    38,    39,    40,    41,    42,    43,    44,
      45,    46,    47,    48,    49,    50,    51,    52,    53,    54,
      55
};

#if YYDEBUG
/* YYRLINE[YYN] -- Source line where rule number YYN was defined.  */
static const yytype_int16 yyrline[] =
{
       0,  1145,  1145,  1150,  1158,  1160,  1162,  1164,  1166,  1168,
    1170,  1172,  1174,  1176,  1178,  1180,  1182,  1184,  1186,  1188,
    1190,  1192,  1194,  1196,  1198,  1200,  1202,  1204,  1206,  1208,
    1210,  1218,  1224,  1230,  1236,  1242,  1248,  1254,  1258,  1266,
    1275,  1284,  1293,  1302,  1314,  1316,  1324,  1341,  1347,  1354,
    1356,  1358,  1360,  1362,  1364,  1366,  1368,  1379,  1389,  1399,
    1420,  1424,  1433,  1438,  1444,  1448,  1457,  1462,  1467,  1471,
    1480,  1485,  1490,  1494,  1503,  1508,  1513,  1517,  1526,  1531,
    1536,  1540,  1549,  1554,  1559,  1568,  1576,  1585,  1593,  1602,
    1610,  1619,  1627,  1629,  1637,  1642,  1647,  1652,  1657,  1662,
    1667,  1672,  1678,  1684,  1695,  1700,  1705,  1710,  1715,  1720,
    1725,  1730,  1735,  1740,  1745,  1750,  1755,  1761,  1767,  1775,
    1781,  1789,  1795,  1801,  1809,  1815,  1821,  1830,  1837,  1845,
    1853,  1859,  1865,  1873,  1882,  1894,  1899,  1904,  1911,  1919,
    1927,  1935,  1944,  1951,  1960,  1967,  1975,  1984,  1991,  2000,
    2010,  2015,  2020,  2026,  2033,  2040,  2048,  2055,  2063,  2069,
    2076,  2083,  2091,  2097,  2104,  2112,  2122,  2123,  2136,  2146,
    2157,  2167,  2177,  2198,  2207,  2215,  2226,  2235,  2240,  2254,
    2256,  2264,  2266,  2268,  2277,  2279,  2287,  2292,  2297,  2302,
    2307,  2313,  2319,  2325,  2334,  2336,  2344,  2346,  2355,  2360,
    2366,  2372,  2380,  2390,  2392,  2400,  2402,  2411,  2416,  2422,
    2430,  2440,  2442,  2450,  2456,  2462,  2473,  2475,  2483,  2490,
    2496,  2507,  2511,  2516,  2526,  2527,  2533,  2535,  2543,  2555,
    2567,  2578,  2588,  2598
};
#endif

/** Accessing symbol of state STATE.  */
#define YY_ACCESSING_SYMBOL(State) YY_CAST (yysymbol_kind_t, yystos[State])

#if 1
/* The user-facing name of the symbol whose (internal) number is
   YYSYMBOL.  No bounds checking.  */
static const char *yysymbol_name (yysymbol_kind_t yysymbol) YY_ATTRIBUTE_UNUSED;

/* YYTNAME[SYMBOL-NUM] -- String name of the symbol SYMBOL-NUM.
   First, the terminals, then, starting at YYNTOKENS, nonterminals.  */
static const char *const yytname[] =
{
  "\"end of file\"", "error", "\"invalid token\"", "CTF_INTEGER_LITERAL",
  "CTF_STRING_LITERAL", "CTF_CHARACTER_LITERAL", "CTF_LSBRAC",
  "CTF_RSBRAC", "CTF_LPAREN", "CTF_RPAREN", "CTF_LBRAC", "CTF_RBRAC",
  "CTF_RARROW", "CTF_STAR", "CTF_PLUS", "CTF_MINUS", "CTF_LT", "CTF_GT",
  "CTF_TYPEASSIGN", "CTF_COLON", "CTF_SEMICOLON", "CTF_DOTDOTDOT",
  "CTF_DOT", "CTF_EQUAL", "CTF_COMMA", "CTF_CONST", "CTF_CHAR",
  "CTF_DOUBLE", "CTF_ENUM", "CTF_ENV", "CTF_EVENT", "CTF_FLOATING_POINT",
  "CTF_FLOAT", "CTF_INTEGER", "CTF_INT", "CTF_LONG", "CTF_SHORT",
  "CTF_SIGNED", "CTF_STREAM", "CTF_STRING", "CTF_STRUCT", "CTF_TRACE",
  "CTF_CALLSITE", "CTF_CLOCK", "CTF_TYPEALIAS", "CTF_TYPEDEF",
  "CTF_UNSIGNED", "CTF_VARIANT", "CTF_VOID", "CTF_BOOL", "CTF_COMPLEX",
  "CTF_IMAGINARY", "CTF_TOK_ALIGN", "IDENTIFIER", "ID_TYPE", "CTF_ERROR",
  "$accept", "file", "keywords", "postfix_expression", "unary_expression",
  "unary_expression_or_range", "declaration", "event_declaration",
  "event_declaration_begin", "event_declaration_end", "stream_declaration",
  "stream_declaration_begin", "stream_declaration_end", "env_declaration",
  "env_declaration_begin", "env_declaration_end", "trace_declaration",
  "trace_declaration_begin", "trace_declaration_end", "clock_declaration",
  "clock_declaration_begin", "clock_declaration_end",
  "callsite_declaration", "callsite_declaration_begin",
  "callsite_declaration_end", "integer_declaration_specifiers",
  "declaration_specifiers", "field_class_declarator_list",
  "integer_field_class_specifier", "field_class_specifier",
  "struct_class_specifier", "struct_declaration_begin",
  "struct_declaration_end", "variant_field_class_specifier",
  "variant_declaration_begin", "variant_declaration_end",
  "enum_field_class_specifier", "struct_or_variant_declaration_list",
  "struct_or_variant_declaration", "alias_declaration_specifiers",
  "struct_or_variant_declarator_list", "struct_or_variant_declarator",
  "enumerator_list", "enumerator", "abstract_declarator_list",
  "abstract_declarator", "direct_abstract_declarator",
  "alias_abstract_declarator_list", "alias_abstract_declarator",
  "direct_alias_abstract_declarator", "declarator", "direct_declarator",
  "field_class_declarator", "direct_field_class_declarator", "pointer",
  "type_qualifier_list", "ctf_assignment_expression_list",
  "ctf_assignment_expression", YY_NULLPTR
};

static const char *
yysymbol_name (yysymbol_kind_t yysymbol)
{
  return yytname[yysymbol];
}
#endif

#define YYPACT_NINF (-363)

#define yypact_value_is_default(Yyn) \
  ((Yyn) == YYPACT_NINF)

#define YYTABLE_NINF (-33)

#define yytable_value_is_error(Yyn) \
  0

/* YYPACT[STATE-NUM] -- Index in YYTABLE of the portion describing
   STATE-NUM.  */
static const yytype_int16 yypact[] =
{
    1998,  -363,  -363,  -363,    45,    22,    92,    94,  -363,   146,
    -363,  -363,  -363,  -363,   165,   194,    59,   204,   207,   228,
    2088,  2088,  -363,    57,  -363,  -363,  -363,  -363,  -363,   397,
    -363,  -363,   449,  -363,   501,  -363,   553,  -363,   605,  -363,
    -363,  1938,  -363,  1679,  2145,    75,   150,  -363,  -363,  -363,
     657,   709,  -363,   761,  -363,   241,   241,  -363,  -363,  -363,
    -363,   813,  -363,   865,  1756,  1786,  -363,    66,    47,   145,
    -363,  -363,  -363,  -363,  -363,  -363,  -363,  1281,    19,  1333,
    1333,   116,   218,   253,    45,  -363,  -363,    94,   305,   146,
     354,   387,   394,   443,  -363,   209,    59,  -363,  -363,  -363,
    2088,  2088,   450,    57,   502,   554,   606,   658,  -363,  -363,
     710,  -363,   131,    60,  -363,  2028,   449,   205,   234,  -363,
     501,   236,  -363,   553,   238,  -363,   605,  -363,  -363,  1816,
    -363,    30,  -363,  -363,  -363,  -363,  -363,  -363,  -363,  -363,
    -363,  -363,  -363,  -363,  -363,  -363,  -363,  -363,  -363,  -363,
    -363,  -363,   240,   243,   247,    48,  -363,  -363,  -363,   251,
    -363,  -363,  -363,  -363,  -363,  -363,  -363,    81,  -363,  1679,
    2145,  1679,  2145,  -363,   917,  -363,   969,  -363,  1021,  -363,
    -363,  1876,   252,  -363,   813,   254,  -363,   865,    35,    29,
    -363,   152,  -363,    54,     9,    36,  -363,   121,  -363,   271,
      39,   267,   274,   113,  -363,   132,  -363,  1906,  -363,   283,
    -363,   131,   131,  1756,  1786,  1281,   143,  1968,  2088,  1281,
    1816,  -363,   273,  -363,  -363,  -363,  -363,  -363,  -363,  -363,
    1786,   178,  1281,  1281,  1281,  1281,  -363,  1384,  1073,  1679,
    -363,  -363,    50,   300,    97,   349,  -363,  -363,  -363,  1876,
    1876,  -363,  2088,  2088,  1726,   246,  -363,  -363,  -363,  -363,
    -363,   286,  -363,  -363,   139,  2058,    35,  1177,    54,   292,
    -363,    36,  1281,   271,   294,   294,   289,   290,  1906,   291,
     299,  1906,  -363,  -363,  -363,   166,   295,   311,  -363,  -363,
    -363,  -363,  -363,  2118,  -363,  1786,   295,  -363,   185,  -363,
     303,  -363,  -363,  -363,  -363,  -363,  -363,  -363,  1125,   112,
    -363,  1435,  1679,  -363,  1486,  1679,   269,   279,  1756,  1786,
      37,  1281,  1816,  -363,   192,  -363,   313,   335,    43,   339,
    -363,  -363,  -363,  -363,  -363,  -363,  1846,  -363,  -363,   341,
    -363,  -363,   344,  -363,  -363,   294,   294,  -363,   294,   294,
    -363,  2058,  -363,   295,  -363,  1281,  -363,  -363,  1537,  -363,
     120,  -363,   138,   347,   348,   169,   202,   343,  -363,  1786,
     213,  -363,    33,  1281,  1281,   335,  1281,   181,  -363,  -363,
    -363,   215,  -363,   351,   350,  -363,  -363,  1906,  1906,  -363,
    -363,  -363,  -363,  1846,  -363,  -363,  -363,  1588,  -363,  1639,
    1281,  1281,  2058,  -363,  -363,   223,  -363,  -363,  -363,   356,
     352,   358,  -363,   181,  1229,   351,  -363,  -363,  1906,  1906,
    1906,  1906,   345,  -363,  -363,   364,   369,  1846,  -363,  -363,
    -363,  -363,  -363,  -363,   372,  -363,  -363,  -363,  -363,  -363,
    -363,   224,  -363,  -363
};

/* YYDEFACT[STATE-NUM] -- Default reduction number in state STATE-NUM.
   Performed when YYTABLE does not specify something else to do.  Zero
   means the default is an error.  */
static const yytype_uint8 yydefact[] =
{
       0,    88,   105,   110,     0,     0,     0,     0,   109,     0,
     107,   108,   106,   111,     0,   121,     0,     0,     0,     0,
       0,     0,   112,     0,   104,   113,   114,   115,   116,     0,
       2,    50,     0,    51,     0,    52,     0,    53,     0,    54,
      55,     0,    89,     0,     0,   162,   165,   124,    70,    62,
       0,     0,    66,     0,   135,   130,   131,   126,   166,    74,
      82,     0,    78,     0,   198,     0,   150,     0,     0,     0,
     125,   166,     1,     3,    34,    35,    36,     0,     0,     0,
       0,    88,   105,   110,    19,    26,    24,    16,   109,    17,
     107,   108,   106,   111,    25,   121,    21,    27,    29,    28,
       0,    23,   112,    20,   104,   113,   114,   115,    30,    31,
     116,    33,    44,     0,    60,     0,     0,     0,     0,    64,
       0,     0,    68,     0,     0,    72,     0,    49,    90,     0,
      91,   189,    22,     5,    10,    19,    16,     9,    17,     7,
       8,     6,    11,    18,    21,    23,    12,    20,     4,    13,
      14,    15,   186,   187,   188,     0,   184,    84,    94,     0,
      96,    97,    95,    98,    99,   100,   101,     0,    85,     0,
       0,     0,     0,   117,     0,   119,     0,   122,     0,   166,
     166,     0,     0,    80,     0,     0,    76,     0,   198,   221,
     199,     0,   194,   196,   198,     0,   218,     0,    92,   216,
       0,     0,     0,     0,   166,     0,   166,     0,    32,     0,
      63,    45,    46,   198,     0,     0,     0,     0,     0,     0,
       0,    61,     0,   226,    67,    65,    71,    69,    75,    73,
       0,     0,     0,     0,     0,     0,   152,     0,     0,     0,
      86,    87,     0,     0,     0,     0,   118,   120,   123,     0,
       0,   136,     0,     0,     0,   127,   167,    83,    81,    79,
      77,     0,   224,   222,     0,     0,   198,     0,   197,     0,
      57,     0,     0,   217,     0,     0,     0,     0,     0,     0,
       0,     0,   151,   137,    37,     0,   231,     0,    42,    43,
      39,    40,    41,   229,   228,     0,   232,   227,     0,    58,
      48,   193,   190,   191,   192,   158,   185,   102,     0,     0,
     154,     0,     0,   156,     0,     0,   128,   129,   198,     0,
       0,     0,     0,   213,     0,   179,   181,   211,     0,     0,
     200,   225,   223,   173,   175,   174,   207,   195,   202,     0,
     219,    93,     0,   166,   166,   142,   144,   140,   147,   149,
     145,     0,    38,   230,    56,     0,   103,   153,     0,   160,
       0,   163,     0,     0,     0,     0,     0,     0,   182,     0,
       0,   168,     0,     0,     0,   212,     0,   207,   176,   178,
     177,     0,   203,   205,   207,   201,   220,     0,     0,   166,
     166,   166,   166,   207,    47,   159,   155,     0,   157,     0,
       0,     0,     0,   170,   214,     0,   171,   180,   183,     0,
       0,     0,    59,   207,     0,   206,   138,   139,     0,     0,
       0,     0,   233,   161,   164,     0,     0,   207,   169,   215,
     132,   208,   204,   210,     0,   141,   143,   146,   148,   133,
     134,     0,   209,   172
};

/* YYPGOTO[NTERM-NUM].  */
static const yytype_int16 yypgoto[] =
{
    -363,  -363,   -29,   200,   -61,    34,   336,  -363,  -363,   264,
    -363,  -363,   261,  -363,  -363,   266,  -363,  -363,   265,  -363,
    -363,   203,  -363,  -363,   208,  -114,     0,  -127,  -148,   -31,
    -363,   190,    40,  -363,   -46,  -241,  -363,   -47,  -363,  -321,
    -363,    32,  -164,  -234,  -209,  -161,   214,  -362,  -342,    10,
      82,    73,  -189,   211,   -63,  -363,   -25,  -108
};

/* YYDEFGOTO[NTERM-NUM].  */
static const yytype_int16 yydefgoto[] =
{
       0,    29,   111,   112,   113,   301,    30,    31,    32,   114,
      33,    34,   119,    35,    36,   122,    37,    38,   125,    39,
      63,   186,    40,    61,   183,   167,   115,   197,   168,    42,
      57,    58,   255,    70,    71,   283,    47,   181,   256,   336,
     324,   325,   155,   156,   191,   192,   193,   381,   382,   383,
     326,   327,   198,   199,   200,   264,   116,   117
};

/* YYTABLE[YYPACT[STATE-NUM]] -- What to do in state STATE-NUM.  If
   positive, shift that token.  If negative, reduce the rule whose
   number is the opposite.  If YYTABLE_NINF, syntax error.  */
static const yytype_int16 yytable[] =
{
      41,   194,   231,   306,   285,   242,   269,   244,   222,   120,
     130,   123,   222,   126,   154,   222,   209,   188,   222,   241,
      64,    65,   204,   206,   207,   174,   176,   261,   178,    41,
     393,   422,    48,   130,   130,   411,   184,   347,   187,   210,
     350,   320,   189,   188,   195,   320,   189,   195,   189,   189,
     189,   320,   321,   232,   262,    43,   243,    66,   245,   236,
     267,   310,   190,   203,    44,   441,   222,    66,   222,    54,
     222,   432,   237,    67,   311,   309,   222,   306,   218,   222,
     306,   427,   341,   219,   130,   169,   323,   286,   190,   196,
     323,   239,   196,   296,   170,   241,   323,   241,    45,    46,
     213,   214,    49,   298,    50,   337,   240,   158,   313,   365,
      68,    69,    55,    56,   159,   160,   161,   162,   163,   201,
     202,   314,   -22,   357,   306,   194,   263,   164,   -22,   230,
     165,   396,   249,   250,   -22,   166,   358,   215,   -22,   -22,
     154,   270,   154,   216,   397,   271,   416,   417,   360,   398,
     194,   362,   189,   217,   287,    66,    51,   278,   294,   281,
     171,   205,   399,   306,   331,   306,   276,   277,   353,   172,
     265,   300,   300,   300,   300,    52,   266,   435,   436,   437,
     438,   254,   130,   130,   351,   279,   280,   402,   292,   377,
     266,   328,   366,   266,   189,   370,   288,   289,   299,   130,
     222,   332,   271,   194,    53,   354,   339,   254,   154,   271,
     154,   342,   371,   308,    59,   -18,   372,    60,   293,    53,
     295,   -18,   403,   130,    -5,   223,   271,   -18,   343,   344,
      -5,   -18,   -18,   406,   335,   412,    -5,   271,    62,   413,
      -5,    -5,   405,   428,   443,   179,   180,   271,   413,   254,
     254,    54,   318,   319,   224,   194,   226,   328,   228,   -10,
     368,   238,   130,   233,   130,   -10,   234,   302,   303,   304,
     235,   -10,   257,   384,   259,   -10,   -10,   272,   254,   211,
     212,   254,   154,   154,   274,   154,   154,   130,   130,   316,
     317,   275,   284,   297,   394,   330,   387,   388,   329,   389,
     390,   340,   391,   392,    66,   380,   345,   346,   348,   328,
     312,    -9,   408,   409,   384,   410,   349,    -9,   352,   271,
     335,   363,   369,    -9,   355,   240,   158,    -9,    -9,   154,
     384,   364,   373,   159,   160,   161,   162,   163,   130,   425,
     426,   374,   418,   419,   420,   421,   164,   376,   385,   165,
     384,   386,   404,   434,   166,   400,   401,   414,   377,   315,
      -7,   430,   380,   429,   384,    73,    -7,   431,   154,   413,
     154,   335,    -7,   439,   240,   158,    -7,    -7,   440,   442,
     221,   225,   159,   160,   161,   162,   163,   254,   254,   227,
     260,   229,   258,    -8,   415,   164,   380,    72,   165,    -8,
      -6,   375,   367,   166,   407,    -8,    -6,     0,   268,    -8,
      -8,   273,    -6,     0,     0,     0,    -6,    -6,   254,   254,
     254,   254,     1,     2,     3,     4,     5,     6,     7,     8,
       9,    10,    11,    12,    13,    14,    15,    16,    17,    18,
      19,    20,    21,    22,    23,    24,    25,    26,    27,   -11,
       0,    28,    74,    75,    76,   -11,   -12,    77,     0,     0,
      78,   -11,   -12,    79,    80,   -11,   -11,     0,   -12,     0,
       0,     0,   -12,   -12,    81,    82,    83,    84,    85,    86,
      87,    88,    89,    90,    91,    92,    93,    94,    95,    96,
      97,    98,    99,   100,   101,   102,   103,   104,   105,   106,
     107,   108,   109,   110,    74,    75,    76,     0,    -4,    77,
       0,     0,   118,     0,    -4,    79,    80,     0,     0,     0,
      -4,     0,     0,     0,    -4,    -4,    81,    82,    83,    84,
      85,    86,    87,    88,    89,    90,    91,    92,    93,    94,
      95,    96,    97,    98,    99,   100,   101,   102,   103,   104,
     105,   106,   107,   108,   109,   110,    74,    75,    76,     0,
     -13,    77,     0,     0,   121,     0,   -13,    79,    80,     0,
       0,     0,   -13,     0,     0,     0,   -13,   -13,    81,    82,
      83,    84,    85,    86,    87,    88,    89,    90,    91,    92,
      93,    94,    95,    96,    97,    98,    99,   100,   101,   102,
     103,   104,   105,   106,   107,   108,   109,   110,    74,    75,
      76,     0,   -14,    77,     0,     0,   124,     0,   -14,    79,
      80,     0,     0,     0,   -14,     0,     0,     0,   -14,   -14,
      81,    82,    83,    84,    85,    86,    87,    88,    89,    90,
      91,    92,    93,    94,    95,    96,    97,    98,    99,   100,
     101,   102,   103,   104,   105,   106,   107,   108,   109,   110,
      74,    75,    76,     0,   -15,    77,     0,     0,   173,     0,
     -15,    79,    80,     0,     0,     0,   -15,     0,     0,     0,
     -15,   -15,    81,    82,    83,    84,    85,    86,    87,    88,
      89,    90,    91,    92,    93,    94,    95,    96,    97,    98,
      99,   100,   101,   102,   103,   104,   105,   106,   107,   108,
     109,   110,    74,    75,    76,     0,   -32,    77,     0,     0,
     175,     0,   -32,    79,    80,     0,     0,     0,   -32,     0,
       0,     0,   -32,   -32,    81,    82,    83,    84,    85,    86,
      87,    88,    89,    90,    91,    92,    93,    94,    95,    96,
      97,    98,    99,   100,   101,   102,   103,   104,   105,   106,
     107,   108,   109,   110,    74,    75,    76,     0,     0,    77,
       0,     0,   177,     0,     0,    79,    80,     0,     0,     0,
       0,     0,     0,     0,     0,     0,    81,    82,    83,    84,
      85,    86,    87,    88,    89,    90,    91,    92,    93,    94,
      95,    96,    97,    98,    99,   100,   101,   102,   103,   104,
     105,   106,   107,   108,   109,   110,    74,    75,    76,     0,
       0,    77,     0,     0,   182,     0,     0,    79,    80,     0,
       0,     0,     0,     0,     0,     0,     0,     0,    81,    82,
      83,    84,    85,    86,    87,    88,    89,    90,    91,    92,
      93,    94,    95,    96,    97,    98,    99,   100,   101,   102,
     103,   104,   105,   106,   107,   108,   109,   110,    74,    75,
      76,     0,     0,    77,     0,     0,   185,     0,     0,    79,
      80,     0,     0,     0,     0,     0,     0,     0,     0,     0,
      81,    82,    83,    84,    85,    86,    87,    88,    89,    90,
      91,    92,    93,    94,    95,    96,    97,    98,    99,   100,
     101,   102,   103,   104,   105,   106,   107,   108,   109,   110,
      74,    75,    76,     0,     0,    77,     0,     0,   246,     0,
       0,    79,    80,     0,     0,     0,     0,     0,     0,     0,
       0,     0,    81,    82,    83,    84,    85,    86,    87,    88,
      89,    90,    91,    92,    93,    94,    95,    96,    97,    98,
      99,   100,   101,   102,   103,   104,   105,   106,   107,   108,
     109,   110,    74,    75,    76,     0,     0,    77,     0,     0,
     247,     0,     0,    79,    80,     0,     0,     0,     0,     0,
       0,     0,     0,     0,    81,    82,    83,    84,    85,    86,
      87,    88,    89,    90,    91,    92,    93,    94,    95,    96,
      97,    98,    99,   100,   101,   102,   103,   104,   105,   106,
     107,   108,   109,   110,    74,    75,    76,     0,     0,    77,
       0,     0,   248,     0,     0,    79,    80,     0,     0,     0,
       0,     0,     0,     0,     0,     0,    81,    82,    83,    84,
      85,    86,    87,    88,    89,    90,    91,    92,    93,    94,
      95,    96,    97,    98,    99,   100,   101,   102,   103,   104,
     105,   106,   107,   108,   109,   110,    74,    75,    76,     0,
       0,    77,     0,     0,   307,     0,     0,    79,    80,     0,
       0,     0,     0,     0,     0,     0,     0,     0,    81,    82,
      83,    84,    85,    86,    87,    88,    89,    90,    91,    92,
      93,    94,    95,    96,    97,    98,    99,   100,   101,   102,
     103,   104,   105,   106,   107,   108,   109,   110,    74,    75,
      76,     0,     0,    77,     0,     0,   356,     0,     0,    79,
      80,     0,     0,     0,     0,     0,     0,     0,     0,     0,
      81,    82,    83,    84,    85,    86,    87,    88,    89,    90,
      91,    92,    93,    94,    95,    96,    97,    98,    99,   100,
     101,   102,   103,   104,   105,   106,   107,   108,   109,   110,
      74,    75,    76,     0,   338,    77,     0,     0,     0,     0,
       0,    79,    80,     0,     0,     0,     0,     0,     0,     0,
       0,     0,   132,   133,   134,   135,    85,    86,   136,   137,
     138,   139,   140,   141,   142,    94,   143,   144,    97,    98,
      99,     0,   145,   146,   147,   148,   149,   150,   151,   108,
     109,   208,    74,    75,    76,     0,   433,    77,     0,     0,
       0,     0,     0,    79,    80,     0,     0,     0,     0,     0,
       0,     0,     0,     0,   132,   133,   134,   135,    85,    86,
     136,   137,   138,   139,   140,   141,   142,    94,   143,   144,
      97,    98,    99,     0,   145,   146,   147,   148,   149,   150,
     151,   108,   109,   208,    74,    75,    76,     0,     0,    77,
       0,     0,     0,     0,     0,    79,    80,     0,     0,     0,
       0,     0,     0,     0,     0,     0,   132,   133,   134,   135,
      85,    86,   136,   137,   138,   139,   140,   141,   142,    94,
     143,   144,    97,    98,    99,     0,   145,   146,   147,   148,
     149,   150,   151,   108,   109,   208,    74,    75,    76,     0,
       0,    77,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,   132,   133,
     134,   135,    85,    86,   136,   137,   138,   139,   140,   141,
     142,    94,   143,   144,    97,    98,    99,     0,   145,   146,
     147,   148,   149,   150,   151,   108,   109,   208,   131,     0,
       0,     0,     0,     0,     0,   305,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,   132,
     133,   134,   135,    85,    86,   136,   137,   138,   139,   140,
     141,   142,    94,   143,   144,    97,    98,    99,     0,   145,
     146,   147,   148,   149,   150,   151,   108,   152,   153,   131,
       0,     0,     0,     0,     0,     0,   359,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     132,   133,   134,   135,    85,    86,   136,   137,   138,   139,
     140,   141,   142,    94,   143,   144,    97,    98,    99,     0,
     145,   146,   147,   148,   149,   150,   151,   108,   152,   153,
     131,     0,     0,     0,     0,     0,     0,   361,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,   132,   133,   134,   135,    85,    86,   136,   137,   138,
     139,   140,   141,   142,    94,   143,   144,    97,    98,    99,
       0,   145,   146,   147,   148,   149,   150,   151,   108,   152,
     153,   131,     0,     0,     0,     0,     0,     0,   395,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,   132,   133,   134,   135,    85,    86,   136,   137,
     138,   139,   140,   141,   142,    94,   143,   144,    97,    98,
      99,     0,   145,   146,   147,   148,   149,   150,   151,   108,
     152,   153,   131,     0,     0,     0,     0,     0,     0,   423,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,   132,   133,   134,   135,    85,    86,   136,
     137,   138,   139,   140,   141,   142,    94,   143,   144,    97,
      98,    99,     0,   145,   146,   147,   148,   149,   150,   151,
     108,   152,   153,   131,     0,     0,     0,     0,     0,     0,
     424,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,   132,   133,   134,   135,    85,    86,
     136,   137,   138,   139,   140,   141,   142,    94,   143,   144,
      97,    98,    99,   131,   145,   146,   147,   148,   149,   150,
     151,   108,   152,   153,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,   132,   133,   134,   135,    85,    86,
     136,   137,   138,   139,   140,   141,   142,    94,   143,   144,
      97,    98,    99,     0,   145,   146,   147,   148,   149,   150,
     151,   108,   152,   153,   320,     0,     0,     0,     0,   189,
       0,     0,     0,     0,     0,   321,     0,     0,     0,     0,
       0,   128,     2,     3,     4,     0,     0,     7,     8,     9,
      10,    11,    12,    13,   188,    15,    16,     0,     0,   189,
       0,   322,    22,    23,    24,    25,    26,    27,     0,   323,
      28,   128,     2,     3,     4,     0,     0,     7,     8,     9,
      10,    11,    12,    13,   195,    15,    16,     0,     0,   189,
       0,     0,    22,    23,    24,    25,    26,    27,     0,   190,
      28,   128,     2,     3,     4,     0,     0,     7,     8,     9,
      10,    11,    12,    13,   195,    15,    16,     0,     0,   189,
       0,     0,    22,    23,    24,    25,    26,    27,     0,   196,
      28,     1,     2,     3,     4,     0,     0,     7,     8,     9,
      10,    11,    12,    13,   377,    15,    16,     0,     0,   189,
       0,     0,    22,    23,    24,    25,    26,    27,     0,   196,
      28,   378,     2,     3,     4,     0,     0,     7,     8,     9,
      10,    11,    12,    13,     0,    15,    16,   251,     0,     0,
       0,     0,    22,    23,    24,    25,    26,    27,     0,   379,
      28,     1,     2,     3,     4,     0,     0,     7,     8,     9,
      10,    11,    12,    13,     0,    15,    16,   282,     0,     0,
     252,   253,    22,    23,    24,    25,    26,    27,     0,     0,
      28,     1,     2,     3,     4,     0,     0,     7,     8,     9,
      10,    11,    12,    13,     0,    15,    16,     0,     0,     0,
     252,   253,    22,    23,    24,    25,    26,    27,   127,     0,
      28,     0,     0,   128,     2,     3,     4,     0,     0,     7,
       8,     9,    10,    11,    12,    13,     0,    15,    16,     0,
       0,     0,     0,   129,    22,    23,    24,    25,    26,    27,
       0,     0,    28,   132,   133,   134,   135,    85,    86,   136,
     137,   138,   139,   140,   141,   142,    94,   143,   144,    97,
      98,    99,     0,   145,   146,   147,   148,   149,   150,   151,
     108,   290,   291,     1,     2,     3,     4,     5,     6,     7,
       8,     9,    10,    11,    12,    13,    14,    15,    16,    17,
      18,    19,    20,    21,    22,    23,    24,    25,    26,    27,
       0,     0,    28,   128,     2,     3,     4,     0,     0,     7,
       8,     9,    10,    11,    12,    13,     0,    15,    16,     0,
       0,     0,     0,   220,    22,    23,    24,    25,    26,    27,
       0,     0,    28,   333,     2,     3,     4,     0,     0,     7,
       8,     9,    10,    11,    12,    13,     0,    15,    16,     0,
       0,     0,     0,     0,    22,    23,    24,    25,    26,    27,
       0,   334,    28,     1,     2,     3,     4,     0,     0,     7,
       8,     9,    10,    11,    12,    13,     0,    15,    16,     0,
       0,     0,     0,     0,    22,    23,    24,    25,    26,    27,
       0,     0,    28,   128,     2,     3,     4,     0,     0,     7,
       8,     9,    10,    11,    12,    13,     0,    15,    16,     0,
       0,     0,     0,     0,    22,    23,    24,    25,    26,    27,
     157,   158,    28,     0,     0,     0,     0,     0,   159,   160,
     161,   162,   163,     0,     0,     0,     0,     0,     0,     0,
       0,   164,     0,     0,   165,     0,     0,     0,     0,   166
};

static const yytype_int16 yycheck[] =
{
       0,    64,   129,   237,   213,   169,   195,   171,   116,    34,
      41,    36,   120,    38,    43,   123,    77,     8,   126,   167,
      20,    21,    68,    69,    71,    50,    51,   188,    53,    29,
     351,   393,    10,    64,    65,   377,    61,   278,    63,    20,
     281,     8,    13,     8,     8,     8,    13,     8,    13,    13,
      13,     8,    19,    23,    25,    10,   170,    10,   172,    11,
       6,    11,    53,    16,    19,   427,   174,    10,   176,    10,
     178,   413,    24,    16,    24,   239,   184,   311,    18,   187,
     314,   402,   271,    23,   115,    10,    53,   214,    53,    53,
      53,    10,    53,   220,    19,   243,    53,   245,    53,    54,
     100,   101,    10,   230,    10,   266,    25,    26,    11,   318,
      53,    54,    53,    54,    33,    34,    35,    36,    37,    53,
      54,    24,     6,    11,   358,   188,   189,    46,    12,   129,
      49,    11,   179,   180,    18,    54,    24,     6,    22,    23,
     169,    20,   171,    12,    24,    24,   387,   388,   312,    11,
     213,   315,    13,    22,   215,    10,    10,   204,   219,   206,
      10,    16,    24,   397,    25,   399,    53,    54,   295,    19,
      18,   232,   233,   234,   235,    10,    24,   418,   419,   420,
     421,   181,   213,   214,    18,    53,    54,    18,   217,     8,
      24,   254,   319,    24,    13,   322,    53,    54,    20,   230,
     308,   264,    24,   266,    10,    20,   267,   207,   237,    24,
     239,   272,    20,   238,    10,     6,    24,    10,   218,    10,
     220,    12,    20,   254,     6,    20,    24,    18,   274,   275,
      12,    22,    23,    20,   265,    20,    18,    24,    10,    24,
      22,    23,   369,    20,    20,    55,    56,    24,    24,   249,
     250,    10,   252,   253,    20,   318,    20,   320,    20,     6,
     321,    10,   293,    23,   295,    12,    23,   233,   234,   235,
      23,    18,    20,   336,    20,    22,    23,     6,   278,    79,
      80,   281,   311,   312,    17,   314,   315,   318,   319,   249,
     250,    17,     9,    20,   355,     9,   343,   344,    52,   345,
     346,     9,   348,   349,    10,   336,    17,    17,    17,   372,
      10,     6,   373,   374,   377,   376,    17,    12,     7,    24,
     351,    52,   322,    18,    21,    25,    26,    22,    23,   358,
     393,    52,    19,    33,    34,    35,    36,    37,   369,   400,
     401,     6,   389,   390,   391,   392,    46,     8,     7,    49,
     413,     7,     9,   414,    54,     8,     8,     6,     8,    10,
       6,     9,   393,     7,   427,    29,    12,     9,   397,    24,
     399,   402,    18,     9,    25,    26,    22,    23,     9,     7,
     116,   120,    33,    34,    35,    36,    37,   387,   388,   123,
     187,   126,   184,     6,   384,    46,   427,     0,    49,    12,
       6,   328,   320,    54,   372,    18,    12,    -1,   194,    22,
      23,   200,    18,    -1,    -1,    -1,    22,    23,   418,   419,
     420,   421,    25,    26,    27,    28,    29,    30,    31,    32,
      33,    34,    35,    36,    37,    38,    39,    40,    41,    42,
      43,    44,    45,    46,    47,    48,    49,    50,    51,     6,
      -1,    54,     3,     4,     5,    12,     6,     8,    -1,    -1,
      11,    18,    12,    14,    15,    22,    23,    -1,    18,    -1,
      -1,    -1,    22,    23,    25,    26,    27,    28,    29,    30,
      31,    32,    33,    34,    35,    36,    37,    38,    39,    40,
      41,    42,    43,    44,    45,    46,    47,    48,    49,    50,
      51,    52,    53,    54,     3,     4,     5,    -1,     6,     8,
      -1,    -1,    11,    -1,    12,    14,    15,    -1,    -1,    -1,
      18,    -1,    -1,    -1,    22,    23,    25,    26,    27,    28,
      29,    30,    31,    32,    33,    34,    35,    36,    37,    38,
      39,    40,    41,    42,    43,    44,    45,    46,    47,    48,
      49,    50,    51,    52,    53,    54,     3,     4,     5,    -1,
       6,     8,    -1,    -1,    11,    -1,    12,    14,    15,    -1,
      -1,    -1,    18,    -1,    -1,    -1,    22,    23,    25,    26,
      27,    28,    29,    30,    31,    32,    33,    34,    35,    36,
      37,    38,    39,    40,    41,    42,    43,    44,    45,    46,
      47,    48,    49,    50,    51,    52,    53,    54,     3,     4,
       5,    -1,     6,     8,    -1,    -1,    11,    -1,    12,    14,
      15,    -1,    -1,    -1,    18,    -1,    -1,    -1,    22,    23,
      25,    26,    27,    28,    29,    30,    31,    32,    33,    34,
      35,    36,    37,    38,    39,    40,    41,    42,    43,    44,
      45,    46,    47,    48,    49,    50,    51,    52,    53,    54,
       3,     4,     5,    -1,     6,     8,    -1,    -1,    11,    -1,
      12,    14,    15,    -1,    -1,    -1,    18,    -1,    -1,    -1,
      22,    23,    25,    26,    27,    28,    29,    30,    31,    32,
      33,    34,    35,    36,    37,    38,    39,    40,    41,    42,
      43,    44,    45,    46,    47,    48,    49,    50,    51,    52,
      53,    54,     3,     4,     5,    -1,     6,     8,    -1,    -1,
      11,    -1,    12,    14,    15,    -1,    -1,    -1,    18,    -1,
      -1,    -1,    22,    23,    25,    26,    27,    28,    29,    30,
      31,    32,    33,    34,    35,    36,    37,    38,    39,    40,
      41,    42,    43,    44,    45,    46,    47,    48,    49,    50,
      51,    52,    53,    54,     3,     4,     5,    -1,    -1,     8,
      -1,    -1,    11,    -1,    -1,    14,    15,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    25,    26,    27,    28,
      29,    30,    31,    32,    33,    34,    35,    36,    37,    38,
      39,    40,    41,    42,    43,    44,    45,    46,    47,    48,
      49,    50,    51,    52,    53,    54,     3,     4,     5,    -1,
      -1,     8,    -1,    -1,    11,    -1,    -1,    14,    15,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    25,    26,
      27,    28,    29,    30,    31,    32,    33,    34,    35,    36,
      37,    38,    39,    40,    41,    42,    43,    44,    45,    46,
      47,    48,    49,    50,    51,    52,    53,    54,     3,     4,
       5,    -1,    -1,     8,    -1,    -1,    11,    -1,    -1,    14,
      15,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      25,    26,    27,    28,    29,    30,    31,    32,    33,    34,
      35,    36,    37,    38,    39,    40,    41,    42,    43,    44,
      45,    46,    47,    48,    49,    50,    51,    52,    53,    54,
       3,     4,     5,    -1,    -1,     8,    -1,    -1,    11,    -1,
      -1,    14,    15,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    25,    26,    27,    28,    29,    30,    31,    32,
      33,    34,    35,    36,    37,    38,    39,    40,    41,    42,
      43,    44,    45,    46,    47,    48,    49,    50,    51,    52,
      53,    54,     3,     4,     5,    -1,    -1,     8,    -1,    -1,
      11,    -1,    -1,    14,    15,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    25,    26,    27,    28,    29,    30,
      31,    32,    33,    34,    35,    36,    37,    38,    39,    40,
      41,    42,    43,    44,    45,    46,    47,    48,    49,    50,
      51,    52,    53,    54,     3,     4,     5,    -1,    -1,     8,
      -1,    -1,    11,    -1,    -1,    14,    15,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    25,    26,    27,    28,
      29,    30,    31,    32,    33,    34,    35,    36,    37,    38,
      39,    40,    41,    42,    43,    44,    45,    46,    47,    48,
      49,    50,    51,    52,    53,    54,     3,     4,     5,    -1,
      -1,     8,    -1,    -1,    11,    -1,    -1,    14,    15,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    25,    26,
      27,    28,    29,    30,    31,    32,    33,    34,    35,    36,
      37,    38,    39,    40,    41,    42,    43,    44,    45,    46,
      47,    48,    49,    50,    51,    52,    53,    54,     3,     4,
       5,    -1,    -1,     8,    -1,    -1,    11,    -1,    -1,    14,
      15,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      25,    26,    27,    28,    29,    30,    31,    32,    33,    34,
      35,    36,    37,    38,    39,    40,    41,    42,    43,    44,
      45,    46,    47,    48,    49,    50,    51,    52,    53,    54,
       3,     4,     5,    -1,     7,     8,    -1,    -1,    -1,    -1,
      -1,    14,    15,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    25,    26,    27,    28,    29,    30,    31,    32,
      33,    34,    35,    36,    37,    38,    39,    40,    41,    42,
      43,    -1,    45,    46,    47,    48,    49,    50,    51,    52,
      53,    54,     3,     4,     5,    -1,     7,     8,    -1,    -1,
      -1,    -1,    -1,    14,    15,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    25,    26,    27,    28,    29,    30,
      31,    32,    33,    34,    35,    36,    37,    38,    39,    40,
      41,    42,    43,    -1,    45,    46,    47,    48,    49,    50,
      51,    52,    53,    54,     3,     4,     5,    -1,    -1,     8,
      -1,    -1,    -1,    -1,    -1,    14,    15,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    25,    26,    27,    28,
      29,    30,    31,    32,    33,    34,    35,    36,    37,    38,
      39,    40,    41,    42,    43,    -1,    45,    46,    47,    48,
      49,    50,    51,    52,    53,    54,     3,     4,     5,    -1,
      -1,     8,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    25,    26,
      27,    28,    29,    30,    31,    32,    33,    34,    35,    36,
      37,    38,    39,    40,    41,    42,    43,    -1,    45,    46,
      47,    48,    49,    50,    51,    52,    53,    54,     4,    -1,
      -1,    -1,    -1,    -1,    -1,    11,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    25,
      26,    27,    28,    29,    30,    31,    32,    33,    34,    35,
      36,    37,    38,    39,    40,    41,    42,    43,    -1,    45,
      46,    47,    48,    49,    50,    51,    52,    53,    54,     4,
      -1,    -1,    -1,    -1,    -1,    -1,    11,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      25,    26,    27,    28,    29,    30,    31,    32,    33,    34,
      35,    36,    37,    38,    39,    40,    41,    42,    43,    -1,
      45,    46,    47,    48,    49,    50,    51,    52,    53,    54,
       4,    -1,    -1,    -1,    -1,    -1,    -1,    11,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    25,    26,    27,    28,    29,    30,    31,    32,    33,
      34,    35,    36,    37,    38,    39,    40,    41,    42,    43,
      -1,    45,    46,    47,    48,    49,    50,    51,    52,    53,
      54,     4,    -1,    -1,    -1,    -1,    -1,    -1,    11,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    25,    26,    27,    28,    29,    30,    31,    32,
      33,    34,    35,    36,    37,    38,    39,    40,    41,    42,
      43,    -1,    45,    46,    47,    48,    49,    50,    51,    52,
      53,    54,     4,    -1,    -1,    -1,    -1,    -1,    -1,    11,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    25,    26,    27,    28,    29,    30,    31,
      32,    33,    34,    35,    36,    37,    38,    39,    40,    41,
      42,    43,    -1,    45,    46,    47,    48,    49,    50,    51,
      52,    53,    54,     4,    -1,    -1,    -1,    -1,    -1,    -1,
      11,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    25,    26,    27,    28,    29,    30,
      31,    32,    33,    34,    35,    36,    37,    38,    39,    40,
      41,    42,    43,     4,    45,    46,    47,    48,    49,    50,
      51,    52,    53,    54,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    25,    26,    27,    28,    29,    30,
      31,    32,    33,    34,    35,    36,    37,    38,    39,    40,
      41,    42,    43,    -1,    45,    46,    47,    48,    49,    50,
      51,    52,    53,    54,     8,    -1,    -1,    -1,    -1,    13,
      -1,    -1,    -1,    -1,    -1,    19,    -1,    -1,    -1,    -1,
      -1,    25,    26,    27,    28,    -1,    -1,    31,    32,    33,
      34,    35,    36,    37,     8,    39,    40,    -1,    -1,    13,
      -1,    45,    46,    47,    48,    49,    50,    51,    -1,    53,
      54,    25,    26,    27,    28,    -1,    -1,    31,    32,    33,
      34,    35,    36,    37,     8,    39,    40,    -1,    -1,    13,
      -1,    -1,    46,    47,    48,    49,    50,    51,    -1,    53,
      54,    25,    26,    27,    28,    -1,    -1,    31,    32,    33,
      34,    35,    36,    37,     8,    39,    40,    -1,    -1,    13,
      -1,    -1,    46,    47,    48,    49,    50,    51,    -1,    53,
      54,    25,    26,    27,    28,    -1,    -1,    31,    32,    33,
      34,    35,    36,    37,     8,    39,    40,    -1,    -1,    13,
      -1,    -1,    46,    47,    48,    49,    50,    51,    -1,    53,
      54,    25,    26,    27,    28,    -1,    -1,    31,    32,    33,
      34,    35,    36,    37,    -1,    39,    40,    11,    -1,    -1,
      -1,    -1,    46,    47,    48,    49,    50,    51,    -1,    53,
      54,    25,    26,    27,    28,    -1,    -1,    31,    32,    33,
      34,    35,    36,    37,    -1,    39,    40,    11,    -1,    -1,
      44,    45,    46,    47,    48,    49,    50,    51,    -1,    -1,
      54,    25,    26,    27,    28,    -1,    -1,    31,    32,    33,
      34,    35,    36,    37,    -1,    39,    40,    -1,    -1,    -1,
      44,    45,    46,    47,    48,    49,    50,    51,    20,    -1,
      54,    -1,    -1,    25,    26,    27,    28,    -1,    -1,    31,
      32,    33,    34,    35,    36,    37,    -1,    39,    40,    -1,
      -1,    -1,    -1,    45,    46,    47,    48,    49,    50,    51,
      -1,    -1,    54,    25,    26,    27,    28,    29,    30,    31,
      32,    33,    34,    35,    36,    37,    38,    39,    40,    41,
      42,    43,    -1,    45,    46,    47,    48,    49,    50,    51,
      52,    53,    54,    25,    26,    27,    28,    29,    30,    31,
      32,    33,    34,    35,    36,    37,    38,    39,    40,    41,
      42,    43,    44,    45,    46,    47,    48,    49,    50,    51,
      -1,    -1,    54,    25,    26,    27,    28,    -1,    -1,    31,
      32,    33,    34,    35,    36,    37,    -1,    39,    40,    -1,
      -1,    -1,    -1,    45,    46,    47,    48,    49,    50,    51,
      -1,    -1,    54,    25,    26,    27,    28,    -1,    -1,    31,
      32,    33,    34,    35,    36,    37,    -1,    39,    40,    -1,
      -1,    -1,    -1,    -1,    46,    47,    48,    49,    50,    51,
      -1,    53,    54,    25,    26,    27,    28,    -1,    -1,    31,
      32,    33,    34,    35,    36,    37,    -1,    39,    40,    -1,
      -1,    -1,    -1,    -1,    46,    47,    48,    49,    50,    51,
      -1,    -1,    54,    25,    26,    27,    28,    -1,    -1,    31,
      32,    33,    34,    35,    36,    37,    -1,    39,    40,    -1,
      -1,    -1,    -1,    -1,    46,    47,    48,    49,    50,    51,
      25,    26,    54,    -1,    -1,    -1,    -1,    -1,    33,    34,
      35,    36,    37,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    46,    -1,    -1,    49,    -1,    -1,    -1,    -1,    54
};

/* YYSTOS[STATE-NUM] -- The symbol kind of the accessing symbol of
   state STATE-NUM.  */
static const yytype_int8 yystos[] =
{
       0,    25,    26,    27,    28,    29,    30,    31,    32,    33,
      34,    35,    36,    37,    38,    39,    40,    41,    42,    43,
      44,    45,    46,    47,    48,    49,    50,    51,    54,    57,
      62,    63,    64,    66,    67,    69,    70,    72,    73,    75,
      78,    82,    85,    10,    19,    53,    54,    92,    10,    10,
      10,    10,    10,    10,    10,    53,    54,    86,    87,    10,
      10,    79,    10,    76,    82,    82,    10,    16,    53,    54,
      89,    90,     0,    62,     3,     4,     5,     8,    11,    14,
      15,    25,    26,    27,    28,    29,    30,    31,    32,    33,
      34,    35,    36,    37,    38,    39,    40,    41,    42,    43,
      44,    45,    46,    47,    48,    49,    50,    51,    52,    53,
      54,    58,    59,    60,    65,    82,   112,   113,    11,    68,
     112,    11,    71,   112,    11,    74,   112,    20,    25,    45,
      85,     4,    25,    26,    27,    28,    31,    32,    33,    34,
      35,    36,    37,    39,    40,    45,    46,    47,    48,    49,
      50,    51,    53,    54,    58,    98,    99,    25,    26,    33,
      34,    35,    36,    37,    46,    49,    54,    81,    84,    10,
      19,    10,    19,    11,   112,    11,   112,    11,   112,    87,
      87,    93,    11,    80,   112,    11,    77,   112,     8,    13,
      53,   100,   101,   102,   110,     8,    53,    83,   108,   109,
     110,    53,    54,    16,    90,    16,    90,    93,    54,    60,
      20,    59,    59,    82,    82,     6,    12,    22,    18,    23,
      45,    65,   113,    20,    20,    68,    20,    71,    20,    74,
      82,    83,    23,    23,    23,    23,    11,    24,    10,    10,
      25,    84,    98,    81,    98,    81,    11,    11,    11,    93,
      93,    11,    44,    45,    82,    88,    94,    20,    80,    20,
      77,   101,    25,   110,   111,    18,    24,     6,   102,   108,
      20,    24,     6,   109,    17,    17,    53,    54,    93,    53,
      54,    93,    11,    91,     9,   100,    83,    60,    53,    54,
      53,    54,    58,    82,    60,    82,    83,    20,    83,    20,
      60,    61,    61,    61,    61,    11,    99,    11,   112,    98,
      11,    24,    10,    11,    24,    10,    88,    88,    82,    82,
       8,    19,    45,    53,    96,    97,   106,   107,   110,    52,
       9,    25,   110,    25,    53,    85,    95,   101,     7,    60,
       9,   108,    60,    90,    90,    17,    17,    91,    17,    17,
      91,    18,     7,    83,    20,    21,    11,    11,    24,    11,
      98,    11,    98,    52,    52,   100,    83,   106,    60,    82,
      83,    20,    24,    19,     6,   107,     8,     8,    25,    53,
      85,   103,   104,   105,   110,     7,     7,    93,    93,    90,
      90,    90,    90,    95,    60,    11,    11,    24,    11,    24,
       8,     8,    18,    20,     9,    83,    20,    97,    60,    60,
      60,   104,    20,    24,     6,   105,    91,    91,    93,    93,
      93,    93,   103,    11,    11,    60,    60,    95,    20,     7,
       9,     9,   104,     7,    60,    91,    91,    91,    91,     9,
       9,   103,     7,    20
};

/* YYR1[RULE-NUM] -- Symbol kind of the left-hand side of rule RULE-NUM.  */
static const yytype_int8 yyr1[] =
{
       0,    56,    57,    57,    58,    58,    58,    58,    58,    58,
      58,    58,    58,    58,    58,    58,    58,    58,    58,    58,
      58,    58,    58,    58,    58,    58,    58,    58,    58,    58,
      58,    59,    59,    59,    59,    59,    59,    59,    59,    59,
      59,    59,    59,    59,    60,    60,    60,    61,    61,    62,
      62,    62,    62,    62,    62,    62,    62,    62,    62,    62,
      63,    63,    64,    65,    66,    66,    67,    68,    69,    69,
      70,    71,    72,    72,    73,    74,    75,    75,    76,    77,
      78,    78,    79,    80,    81,    81,    81,    81,    82,    82,
      82,    82,    83,    83,    84,    84,    84,    84,    84,    84,
      84,    84,    84,    84,    85,    85,    85,    85,    85,    85,
      85,    85,    85,    85,    85,    85,    85,    85,    85,    85,
      85,    85,    85,    85,    85,    85,    85,    86,    86,    86,
      86,    86,    86,    86,    86,    87,    88,    89,    89,    89,
      89,    89,    89,    89,    89,    89,    89,    89,    89,    89,
      90,    91,    92,    92,    92,    92,    92,    92,    92,    92,
      92,    92,    92,    92,    92,    92,    93,    93,    94,    94,
      94,    94,    94,    95,    95,    95,    95,    95,    95,    96,
      96,    97,    97,    97,    98,    98,    99,    99,    99,    99,
      99,    99,    99,    99,   100,   100,   101,   101,   102,   102,
     102,   102,   102,   103,   103,   104,   104,   105,   105,   105,
     105,   106,   106,   107,   107,   107,   108,   108,   109,   109,
     109,   110,   110,   110,   111,   111,   112,   112,   113,   113,
     113,   113,   113,   113
};

/* YYR2[RULE-NUM] -- Number of symbols on the right-hand side of rule RULE-NUM.  */
static const yytype_int8 yyr2[] =
{
       0,     2,     1,     2,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     3,     4,     3,
       3,     3,     3,     3,     1,     2,     2,     3,     1,     2,
       1,     1,     1,     1,     1,     1,     5,     4,     4,     7,
       2,     3,     2,     2,     2,     3,     2,     2,     2,     3,
       2,     2,     2,     3,     2,     2,     3,     4,     1,     2,
       3,     4,     1,     2,     1,     1,     2,     2,     1,     1,
       2,     2,     1,     3,     1,     1,     1,     1,     1,     1,
       1,     1,     3,     4,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     3,     4,     3,
       4,     1,     3,     4,     2,     2,     2,     3,     4,     4,
       1,     1,     7,     8,     8,     1,     1,     3,     6,     6,
       4,     7,     4,     7,     4,     4,     7,     4,     7,     4,
       1,     1,     3,     5,     4,     6,     4,     6,     4,     6,
       5,     7,     1,     5,     7,     1,     0,     2,     3,     5,
       4,     4,     7,     1,     1,     1,     2,     2,     2,     1,
       3,     1,     2,     3,     1,     3,     1,     1,     1,     1,
       3,     3,     3,     3,     1,     3,     1,     2,     0,     1,
       3,     4,     3,     1,     3,     1,     2,     0,     3,     4,
       3,     1,     2,     1,     3,     4,     1,     2,     1,     3,
       4,     1,     2,     3,     1,     2,     2,     3,     3,     3,
       4,     3,     3,     6
};


enum { YYENOMEM = -2 };

#define yyerrok         (yyerrstatus = 0)
#define yyclearin       (yychar = YYEMPTY)

#define YYACCEPT        goto yyacceptlab
#define YYABORT         goto yyabortlab
#define YYERROR         goto yyerrorlab
#define YYNOMEM         goto yyexhaustedlab


#define YYRECOVERING()  (!!yyerrstatus)

#define YYBACKUP(Token, Value)                                    \
  do                                                              \
    if (yychar == YYEMPTY)                                        \
      {                                                           \
        yychar = (Token);                                         \
        yylval = (Value);                                         \
        YYPOPSTACK (yylen);                                       \
        yystate = *yyssp;                                         \
        goto yybackup;                                            \
      }                                                           \
    else                                                          \
      {                                                           \
        yyerror (scanner, yyscanner, YY_("syntax error: cannot back up")); \
        YYERROR;                                                  \
      }                                                           \
  while (0)

/* Backward compatibility with an undocumented macro.
   Use YYerror or YYUNDEF. */
#define YYERRCODE YYUNDEF


/* Enable debugging if requested.  */
#if YYDEBUG

# ifndef YYFPRINTF
#  include <stdio.h> /* INFRINGES ON USER NAME SPACE */
#  define YYFPRINTF fprintf
# endif

# define YYDPRINTF(Args)                        \
do {                                            \
  if (yydebug)                                  \
    YYFPRINTF Args;                             \
} while (0)




# define YY_SYMBOL_PRINT(Title, Kind, Value, Location)                    \
do {                                                                      \
  if (yydebug)                                                            \
    {                                                                     \
      YYFPRINTF (stderr, "%s ", Title);                                   \
      yy_symbol_print (stderr,                                            \
                  Kind, Value, scanner, yyscanner); \
      YYFPRINTF (stderr, "\n");                                           \
    }                                                                     \
} while (0)


/*-----------------------------------.
| Print this symbol's value on YYO.  |
`-----------------------------------*/

static void
yy_symbol_value_print (FILE *yyo,
                       yysymbol_kind_t yykind, YYSTYPE const * const yyvaluep, struct ctf_scanner *scanner, yyscan_t yyscanner)
{
  FILE *yyoutput = yyo;
  YY_USE (yyoutput);
  YY_USE (scanner);
  YY_USE (yyscanner);
  if (!yyvaluep)
    return;
  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  YY_USE (yykind);
  YY_IGNORE_MAYBE_UNINITIALIZED_END
}


/*---------------------------.
| Print this symbol on YYO.  |
`---------------------------*/

static void
yy_symbol_print (FILE *yyo,
                 yysymbol_kind_t yykind, YYSTYPE const * const yyvaluep, struct ctf_scanner *scanner, yyscan_t yyscanner)
{
  YYFPRINTF (yyo, "%s %s (",
             yykind < YYNTOKENS ? "token" : "nterm", yysymbol_name (yykind));

  yy_symbol_value_print (yyo, yykind, yyvaluep, scanner, yyscanner);
  YYFPRINTF (yyo, ")");
}

/*------------------------------------------------------------------.
| yy_stack_print -- Print the state stack from its BOTTOM up to its |
| TOP (included).                                                   |
`------------------------------------------------------------------*/

static void
yy_stack_print (yy_state_t *yybottom, yy_state_t *yytop)
{
  YYFPRINTF (stderr, "Stack now");
  for (; yybottom <= yytop; yybottom++)
    {
      int yybot = *yybottom;
      YYFPRINTF (stderr, " %d", yybot);
    }
  YYFPRINTF (stderr, "\n");
}

# define YY_STACK_PRINT(Bottom, Top)                            \
do {                                                            \
  if (yydebug)                                                  \
    yy_stack_print ((Bottom), (Top));                           \
} while (0)


/*------------------------------------------------.
| Report that the YYRULE is going to be reduced.  |
`------------------------------------------------*/

static void
yy_reduce_print (yy_state_t *yyssp, YYSTYPE *yyvsp,
                 int yyrule, struct ctf_scanner *scanner, yyscan_t yyscanner)
{
  int yylno = yyrline[yyrule];
  int yynrhs = yyr2[yyrule];
  int yyi;
  YYFPRINTF (stderr, "Reducing stack by rule %d (line %d):\n",
             yyrule - 1, yylno);
  /* The symbols being reduced.  */
  for (yyi = 0; yyi < yynrhs; yyi++)
    {
      YYFPRINTF (stderr, "   $%d = ", yyi + 1);
      yy_symbol_print (stderr,
                       YY_ACCESSING_SYMBOL (+yyssp[yyi + 1 - yynrhs]),
                       &yyvsp[(yyi + 1) - (yynrhs)], scanner, yyscanner);
      YYFPRINTF (stderr, "\n");
    }
}

# define YY_REDUCE_PRINT(Rule)          \
do {                                    \
  if (yydebug)                          \
    yy_reduce_print (yyssp, yyvsp, Rule, scanner, yyscanner); \
} while (0)

/* Nonzero means print parse trace.  It is left uninitialized so that
   multiple parsers can coexist.  */
int yydebug;
#else /* !YYDEBUG */
# define YYDPRINTF(Args) ((void) 0)
# define YY_SYMBOL_PRINT(Title, Kind, Value, Location)
# define YY_STACK_PRINT(Bottom, Top)
# define YY_REDUCE_PRINT(Rule)
#endif /* !YYDEBUG */


/* YYINITDEPTH -- initial size of the parser's stacks.  */
#ifndef YYINITDEPTH
# define YYINITDEPTH 200
#endif

/* YYMAXDEPTH -- maximum size the stacks can grow to (effective only
   if the built-in stack extension method is used).

   Do not make this value too large; the results are undefined if
   YYSTACK_ALLOC_MAXIMUM < YYSTACK_BYTES (YYMAXDEPTH)
   evaluated with infinite-precision integer arithmetic.  */

#ifndef YYMAXDEPTH
# define YYMAXDEPTH 10000
#endif


/* Context of a parse error.  */
typedef struct
{
  yy_state_t *yyssp;
  yysymbol_kind_t yytoken;
} yypcontext_t;

/* Put in YYARG at most YYARGN of the expected tokens given the
   current YYCTX, and return the number of tokens stored in YYARG.  If
   YYARG is null, return the number of expected tokens (guaranteed to
   be less than YYNTOKENS).  Return YYENOMEM on memory exhaustion.
   Return 0 if there are more than YYARGN expected tokens, yet fill
   YYARG up to YYARGN. */
static int
yypcontext_expected_tokens (const yypcontext_t *yyctx,
                            yysymbol_kind_t yyarg[], int yyargn)
{
  /* Actual size of YYARG. */
  int yycount = 0;
  int yyn = yypact[+*yyctx->yyssp];
  if (!yypact_value_is_default (yyn))
    {
      /* Start YYX at -YYN if negative to avoid negative indexes in
         YYCHECK.  In other words, skip the first -YYN actions for
         this state because they are default actions.  */
      int yyxbegin = yyn < 0 ? -yyn : 0;
      /* Stay within bounds of both yycheck and yytname.  */
      int yychecklim = YYLAST - yyn + 1;
      int yyxend = yychecklim < YYNTOKENS ? yychecklim : YYNTOKENS;
      int yyx;
      for (yyx = yyxbegin; yyx < yyxend; ++yyx)
        if (yycheck[yyx + yyn] == yyx && yyx != YYSYMBOL_YYerror
            && !yytable_value_is_error (yytable[yyx + yyn]))
          {
            if (!yyarg)
              ++yycount;
            else if (yycount == yyargn)
              return 0;
            else
              yyarg[yycount++] = YY_CAST (yysymbol_kind_t, yyx);
          }
    }
  if (yyarg && yycount == 0 && 0 < yyargn)
    yyarg[0] = YYSYMBOL_YYEMPTY;
  return yycount;
}




#ifndef yystrlen
# if defined __GLIBC__ && defined _STRING_H
#  define yystrlen(S) (YY_CAST (YYPTRDIFF_T, strlen (S)))
# else
/* Return the length of YYSTR.  */
static YYPTRDIFF_T
yystrlen (const char *yystr)
{
  YYPTRDIFF_T yylen;
  for (yylen = 0; yystr[yylen]; yylen++)
    continue;
  return yylen;
}
# endif
#endif

#ifndef yystpcpy
# if defined __GLIBC__ && defined _STRING_H && defined _GNU_SOURCE
#  define yystpcpy stpcpy
# else
/* Copy YYSRC to YYDEST, returning the address of the terminating '\0' in
   YYDEST.  */
static char *
yystpcpy (char *yydest, const char *yysrc)
{
  char *yyd = yydest;
  const char *yys = yysrc;

  while ((*yyd++ = *yys++) != '\0')
    continue;

  return yyd - 1;
}
# endif
#endif

#ifndef yytnamerr
/* Copy to YYRES the contents of YYSTR after stripping away unnecessary
   quotes and backslashes, so that it's suitable for yyerror.  The
   heuristic is that double-quoting is unnecessary unless the string
   contains an apostrophe, a comma, or backslash (other than
   backslash-backslash).  YYSTR is taken from yytname.  If YYRES is
   null, do not copy; instead, return the length of what the result
   would have been.  */
static YYPTRDIFF_T
yytnamerr (char *yyres, const char *yystr)
{
  if (*yystr == '"')
    {
      YYPTRDIFF_T yyn = 0;
      char const *yyp = yystr;
      for (;;)
        switch (*++yyp)
          {
          case '\'':
          case ',':
            goto do_not_strip_quotes;

          case '\\':
            if (*++yyp != '\\')
              goto do_not_strip_quotes;
            else
              goto append;

          append:
          default:
            if (yyres)
              yyres[yyn] = *yyp;
            yyn++;
            break;

          case '"':
            if (yyres)
              yyres[yyn] = '\0';
            return yyn;
          }
    do_not_strip_quotes: ;
    }

  if (yyres)
    return yystpcpy (yyres, yystr) - yyres;
  else
    return yystrlen (yystr);
}
#endif


static int
yy_syntax_error_arguments (const yypcontext_t *yyctx,
                           yysymbol_kind_t yyarg[], int yyargn)
{
  /* Actual size of YYARG. */
  int yycount = 0;
  /* There are many possibilities here to consider:
     - If this state is a consistent state with a default action, then
       the only way this function was invoked is if the default action
       is an error action.  In that case, don't check for expected
       tokens because there are none.
     - The only way there can be no lookahead present (in yychar) is if
       this state is a consistent state with a default action.  Thus,
       detecting the absence of a lookahead is sufficient to determine
       that there is no unexpected or expected token to report.  In that
       case, just report a simple "syntax error".
     - Don't assume there isn't a lookahead just because this state is a
       consistent state with a default action.  There might have been a
       previous inconsistent state, consistent state with a non-default
       action, or user semantic action that manipulated yychar.
     - Of course, the expected token list depends on states to have
       correct lookahead information, and it depends on the parser not
       to perform extra reductions after fetching a lookahead from the
       scanner and before detecting a syntax error.  Thus, state merging
       (from LALR or IELR) and default reductions corrupt the expected
       token list.  However, the list is correct for canonical LR with
       one exception: it will still contain any token that will not be
       accepted due to an error action in a later state.
  */
  if (yyctx->yytoken != YYSYMBOL_YYEMPTY)
    {
      int yyn;
      if (yyarg)
        yyarg[yycount] = yyctx->yytoken;
      ++yycount;
      yyn = yypcontext_expected_tokens (yyctx,
                                        yyarg ? yyarg + 1 : yyarg, yyargn - 1);
      if (yyn == YYENOMEM)
        return YYENOMEM;
      else
        yycount += yyn;
    }
  return yycount;
}

/* Copy into *YYMSG, which is of size *YYMSG_ALLOC, an error message
   about the unexpected token YYTOKEN for the state stack whose top is
   YYSSP.

   Return 0 if *YYMSG was successfully written.  Return -1 if *YYMSG is
   not large enough to hold the message.  In that case, also set
   *YYMSG_ALLOC to the required number of bytes.  Return YYENOMEM if the
   required number of bytes is too large to store.  */
static int
yysyntax_error (YYPTRDIFF_T *yymsg_alloc, char **yymsg,
                const yypcontext_t *yyctx)
{
  enum { YYARGS_MAX = 5 };
  /* Internationalized format string. */
  const char *yyformat = YY_NULLPTR;
  /* Arguments of yyformat: reported tokens (one for the "unexpected",
     one per "expected"). */
  yysymbol_kind_t yyarg[YYARGS_MAX];
  /* Cumulated lengths of YYARG.  */
  YYPTRDIFF_T yysize = 0;

  /* Actual size of YYARG. */
  int yycount = yy_syntax_error_arguments (yyctx, yyarg, YYARGS_MAX);
  if (yycount == YYENOMEM)
    return YYENOMEM;

  switch (yycount)
    {
#define YYCASE_(N, S)                       \
      case N:                               \
        yyformat = S;                       \
        break
    default: /* Avoid compiler warnings. */
      YYCASE_(0, YY_("syntax error"));
      YYCASE_(1, YY_("syntax error, unexpected %s"));
      YYCASE_(2, YY_("syntax error, unexpected %s, expecting %s"));
      YYCASE_(3, YY_("syntax error, unexpected %s, expecting %s or %s"));
      YYCASE_(4, YY_("syntax error, unexpected %s, expecting %s or %s or %s"));
      YYCASE_(5, YY_("syntax error, unexpected %s, expecting %s or %s or %s or %s"));
#undef YYCASE_
    }

  /* Compute error message size.  Don't count the "%s"s, but reserve
     room for the terminator.  */
  yysize = yystrlen (yyformat) - 2 * yycount + 1;
  {
    int yyi;
    for (yyi = 0; yyi < yycount; ++yyi)
      {
        YYPTRDIFF_T yysize1
          = yysize + yytnamerr (YY_NULLPTR, yytname[yyarg[yyi]]);
        if (yysize <= yysize1 && yysize1 <= YYSTACK_ALLOC_MAXIMUM)
          yysize = yysize1;
        else
          return YYENOMEM;
      }
  }

  if (*yymsg_alloc < yysize)
    {
      *yymsg_alloc = 2 * yysize;
      if (! (yysize <= *yymsg_alloc
             && *yymsg_alloc <= YYSTACK_ALLOC_MAXIMUM))
        *yymsg_alloc = YYSTACK_ALLOC_MAXIMUM;
      return -1;
    }

  /* Avoid sprintf, as that infringes on the user's name space.
     Don't have undefined behavior even if the translation
     produced a string with the wrong number of "%s"s.  */
  {
    char *yyp = *yymsg;
    int yyi = 0;
    while ((*yyp = *yyformat) != '\0')
      if (*yyp == '%' && yyformat[1] == 's' && yyi < yycount)
        {
          yyp += yytnamerr (yyp, yytname[yyarg[yyi++]]);
          yyformat += 2;
        }
      else
        {
          ++yyp;
          ++yyformat;
        }
  }
  return 0;
}


/*-----------------------------------------------.
| Release the memory associated to this symbol.  |
`-----------------------------------------------*/

static void
yydestruct (const char *yymsg,
            yysymbol_kind_t yykind, YYSTYPE *yyvaluep, struct ctf_scanner *scanner, yyscan_t yyscanner)
{
  YY_USE (yyvaluep);
  YY_USE (scanner);
  YY_USE (yyscanner);
  if (!yymsg)
    yymsg = "Deleting";
  YY_SYMBOL_PRINT (yymsg, yykind, yyvaluep, yylocationp);

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  YY_USE (yykind);
  YY_IGNORE_MAYBE_UNINITIALIZED_END
}






/*----------.
| yyparse.  |
`----------*/

int
yyparse (struct ctf_scanner *scanner, yyscan_t yyscanner)
{
/* Lookahead token kind.  */
int yychar;


/* The semantic value of the lookahead symbol.  */
/* Default value used for initialization, for pacifying older GCCs
   or non-GCC compilers.  */
YY_INITIAL_VALUE (static YYSTYPE yyval_default;)
YYSTYPE yylval YY_INITIAL_VALUE (= yyval_default);

    /* Number of syntax errors so far.  */
    int yynerrs = 0;

    yy_state_fast_t yystate = 0;
    /* Number of tokens to shift before error messages enabled.  */
    int yyerrstatus = 0;

    /* Refer to the stacks through separate pointers, to allow yyoverflow
       to reallocate them elsewhere.  */

    /* Their size.  */
    YYPTRDIFF_T yystacksize = YYINITDEPTH;

    /* The state stack: array, bottom, top.  */
    yy_state_t yyssa[YYINITDEPTH];
    yy_state_t *yyss = yyssa;
    yy_state_t *yyssp = yyss;

    /* The semantic value stack: array, bottom, top.  */
    YYSTYPE yyvsa[YYINITDEPTH];
    YYSTYPE *yyvs = yyvsa;
    YYSTYPE *yyvsp = yyvs;

  int yyn;
  /* The return value of yyparse.  */
  int yyresult;
  /* Lookahead symbol kind.  */
  yysymbol_kind_t yytoken = YYSYMBOL_YYEMPTY;
  /* The variables used to return semantic value and location from the
     action routines.  */
  YYSTYPE yyval;

  /* Buffer for error messages, and its allocated size.  */
  char yymsgbuf[128];
  char *yymsg = yymsgbuf;
  YYPTRDIFF_T yymsg_alloc = sizeof yymsgbuf;

#define YYPOPSTACK(N)   (yyvsp -= (N), yyssp -= (N))

  /* The number of symbols on the RHS of the reduced rule.
     Keep to zero when no symbol should be popped.  */
  int yylen = 0;

  YYDPRINTF ((stderr, "Starting parse\n"));

  yychar = YYEMPTY; /* Cause a token to be read.  */

  goto yysetstate;


/*------------------------------------------------------------.
| yynewstate -- push a new state, which is found in yystate.  |
`------------------------------------------------------------*/
yynewstate:
  /* In all cases, when you get here, the value and location stacks
     have just been pushed.  So pushing a state here evens the stacks.  */
  yyssp++;


/*--------------------------------------------------------------------.
| yysetstate -- set current state (the top of the stack) to yystate.  |
`--------------------------------------------------------------------*/
yysetstate:
  YYDPRINTF ((stderr, "Entering state %d\n", yystate));
  YY_ASSERT (0 <= yystate && yystate < YYNSTATES);
  YY_IGNORE_USELESS_CAST_BEGIN
  *yyssp = YY_CAST (yy_state_t, yystate);
  YY_IGNORE_USELESS_CAST_END
  YY_STACK_PRINT (yyss, yyssp);

  if (yyss + yystacksize - 1 <= yyssp)
#if !defined yyoverflow && !defined YYSTACK_RELOCATE
    YYNOMEM;
#else
    {
      /* Get the current used size of the three stacks, in elements.  */
      YYPTRDIFF_T yysize = yyssp - yyss + 1;

# if defined yyoverflow
      {
        /* Give user a chance to reallocate the stack.  Use copies of
           these so that the &'s don't force the real ones into
           memory.  */
        yy_state_t *yyss1 = yyss;
        YYSTYPE *yyvs1 = yyvs;

        /* Each stack pointer address is followed by the size of the
           data in use in that stack, in bytes.  This used to be a
           conditional around just the two extra args, but that might
           be undefined if yyoverflow is a macro.  */
        yyoverflow (YY_("memory exhausted"),
                    &yyss1, yysize * YYSIZEOF (*yyssp),
                    &yyvs1, yysize * YYSIZEOF (*yyvsp),
                    &yystacksize);
        yyss = yyss1;
        yyvs = yyvs1;
      }
# else /* defined YYSTACK_RELOCATE */
      /* Extend the stack our own way.  */
      if (YYMAXDEPTH <= yystacksize)
        YYNOMEM;
      yystacksize *= 2;
      if (YYMAXDEPTH < yystacksize)
        yystacksize = YYMAXDEPTH;

      {
        yy_state_t *yyss1 = yyss;
        union yyalloc *yyptr =
          YY_CAST (union yyalloc *,
                   YYSTACK_ALLOC (YY_CAST (YYSIZE_T, YYSTACK_BYTES (yystacksize))));
        if (! yyptr)
          YYNOMEM;
        YYSTACK_RELOCATE (yyss_alloc, yyss);
        YYSTACK_RELOCATE (yyvs_alloc, yyvs);
#  undef YYSTACK_RELOCATE
        if (yyss1 != yyssa)
          YYSTACK_FREE (yyss1);
      }
# endif

      yyssp = yyss + yysize - 1;
      yyvsp = yyvs + yysize - 1;

      YY_IGNORE_USELESS_CAST_BEGIN
      YYDPRINTF ((stderr, "Stack size increased to %ld\n",
                  YY_CAST (long, yystacksize)));
      YY_IGNORE_USELESS_CAST_END

      if (yyss + yystacksize - 1 <= yyssp)
        YYABORT;
    }
#endif /* !defined yyoverflow && !defined YYSTACK_RELOCATE */


  if (yystate == YYFINAL)
    YYACCEPT;

  goto yybackup;


/*-----------.
| yybackup.  |
`-----------*/
yybackup:
  /* Do appropriate processing given the current state.  Read a
     lookahead token if we need one and don't already have one.  */

  /* First try to decide what to do without reference to lookahead token.  */
  yyn = yypact[yystate];
  if (yypact_value_is_default (yyn))
    goto yydefault;

  /* Not known => get a lookahead token if don't already have one.  */

  /* YYCHAR is either empty, or end-of-input, or a valid lookahead.  */
  if (yychar == YYEMPTY)
    {
      YYDPRINTF ((stderr, "Reading a token\n"));
      yychar = yylex (&yylval, yyscanner);
    }

  if (yychar <= YYEOF)
    {
      yychar = YYEOF;
      yytoken = YYSYMBOL_YYEOF;
      YYDPRINTF ((stderr, "Now at end of input.\n"));
    }
  else if (yychar == YYerror)
    {
      /* The scanner already issued an error message, process directly
         to error recovery.  But do not keep the error token as
         lookahead, it is too special and may lead us to an endless
         loop in error recovery. */
      yychar = YYUNDEF;
      yytoken = YYSYMBOL_YYerror;
      goto yyerrlab1;
    }
  else
    {
      yytoken = YYTRANSLATE (yychar);
      YY_SYMBOL_PRINT ("Next token is", yytoken, &yylval, &yylloc);
    }

  /* If the proper action on seeing token YYTOKEN is to reduce or to
     detect an error, take that action.  */
  yyn += yytoken;
  if (yyn < 0 || YYLAST < yyn || yycheck[yyn] != yytoken)
    goto yydefault;
  yyn = yytable[yyn];
  if (yyn <= 0)
    {
      if (yytable_value_is_error (yyn))
        goto yyerrlab;
      yyn = -yyn;
      goto yyreduce;
    }

  /* Count tokens shifted since error; after three, turn off error
     status.  */
  if (yyerrstatus)
    yyerrstatus--;

  /* Shift the lookahead token.  */
  YY_SYMBOL_PRINT ("Shifting", yytoken, &yylval, &yylloc);
  yystate = yyn;
  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END

  /* Discard the shifted token.  */
  yychar = YYEMPTY;
  goto yynewstate;


/*-----------------------------------------------------------.
| yydefault -- do the default action for the current state.  |
`-----------------------------------------------------------*/
yydefault:
  yyn = yydefact[yystate];
  if (yyn == 0)
    goto yyerrlab;
  goto yyreduce;


/*-----------------------------.
| yyreduce -- do a reduction.  |
`-----------------------------*/
yyreduce:
  /* yyn is the number of a rule to reduce with.  */
  yylen = yyr2[yyn];

  /* If YYLEN is nonzero, implement the default value of the action:
     '$$ = $1'.

     Otherwise, the following line sets YYVAL to garbage.
     This behavior is undocumented and Bison
     users should not rely upon it.  Assigning to YYVAL
     unconditionally makes the parser a bit smaller, and it avoids a
     GCC warning that YYVAL may be used uninitialized.  */
  yyval = yyvsp[1-yylen];


  YY_REDUCE_PRINT (yyn);
  switch (yyn)
    {
  case 2: /* file: declaration  */
#line 1146 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {
			if (set_parent_node((yyvsp[0].n), &ctf_scanner_get_ast(scanner)->root))
				reparent_error(scanner, "error reparenting to root");
		}
#line 3360 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 3: /* file: file declaration  */
#line 1151 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {
			if (set_parent_node((yyvsp[0].n), &ctf_scanner_get_ast(scanner)->root))
				reparent_error(scanner, "error reparenting to root");
		}
#line 3369 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 4: /* keywords: CTF_VOID  */
#line 1159 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {	(yyval.s) = yylval.s;		}
#line 3375 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 5: /* keywords: CTF_CHAR  */
#line 1161 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {	(yyval.s) = yylval.s;		}
#line 3381 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 6: /* keywords: CTF_SHORT  */
#line 1163 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {	(yyval.s) = yylval.s;		}
#line 3387 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 7: /* keywords: CTF_INT  */
#line 1165 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {	(yyval.s) = yylval.s;		}
#line 3393 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 8: /* keywords: CTF_LONG  */
#line 1167 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {	(yyval.s) = yylval.s;		}
#line 3399 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 9: /* keywords: CTF_FLOAT  */
#line 1169 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {	(yyval.s) = yylval.s;		}
#line 3405 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 10: /* keywords: CTF_DOUBLE  */
#line 1171 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {	(yyval.s) = yylval.s;		}
#line 3411 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 11: /* keywords: CTF_SIGNED  */
#line 1173 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {	(yyval.s) = yylval.s;		}
#line 3417 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 12: /* keywords: CTF_UNSIGNED  */
#line 1175 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {	(yyval.s) = yylval.s;		}
#line 3423 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 13: /* keywords: CTF_BOOL  */
#line 1177 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {	(yyval.s) = yylval.s;		}
#line 3429 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 14: /* keywords: CTF_COMPLEX  */
#line 1179 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {	(yyval.s) = yylval.s;		}
#line 3435 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 15: /* keywords: CTF_IMAGINARY  */
#line 1181 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {	(yyval.s) = yylval.s;		}
#line 3441 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 16: /* keywords: CTF_FLOATING_POINT  */
#line 1183 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {	(yyval.s) = yylval.s;		}
#line 3447 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 17: /* keywords: CTF_INTEGER  */
#line 1185 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {	(yyval.s) = yylval.s;		}
#line 3453 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 18: /* keywords: CTF_STRING  */
#line 1187 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {	(yyval.s) = yylval.s;		}
#line 3459 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 19: /* keywords: CTF_ENUM  */
#line 1189 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {	(yyval.s) = yylval.s;		}
#line 3465 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 20: /* keywords: CTF_VARIANT  */
#line 1191 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {	(yyval.s) = yylval.s;		}
#line 3471 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 21: /* keywords: CTF_STRUCT  */
#line 1193 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {	(yyval.s) = yylval.s;		}
#line 3477 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 22: /* keywords: CTF_CONST  */
#line 1195 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {	(yyval.s) = yylval.s;		}
#line 3483 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 23: /* keywords: CTF_TYPEDEF  */
#line 1197 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {	(yyval.s) = yylval.s;		}
#line 3489 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 24: /* keywords: CTF_EVENT  */
#line 1199 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {	(yyval.s) = yylval.s;		}
#line 3495 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 25: /* keywords: CTF_STREAM  */
#line 1201 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {	(yyval.s) = yylval.s;		}
#line 3501 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 26: /* keywords: CTF_ENV  */
#line 1203 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {	(yyval.s) = yylval.s;		}
#line 3507 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 27: /* keywords: CTF_TRACE  */
#line 1205 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {	(yyval.s) = yylval.s;		}
#line 3513 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 28: /* keywords: CTF_CLOCK  */
#line 1207 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {	(yyval.s) = yylval.s;		}
#line 3519 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 29: /* keywords: CTF_CALLSITE  */
#line 1209 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {	(yyval.s) = yylval.s;		}
#line 3525 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 30: /* keywords: CTF_TOK_ALIGN  */
#line 1211 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {	(yyval.s) = yylval.s;		}
#line 3531 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 31: /* postfix_expression: IDENTIFIER  */
#line 1219 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {
			(yyval.n) = make_node(scanner, NODE_UNARY_EXPRESSION);
			(yyval.n)->u.unary_expression.type = UNARY_STRING;
			(yyval.n)->u.unary_expression.u.string = yylval.s;
		}
#line 3541 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 32: /* postfix_expression: ID_TYPE  */
#line 1225 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {
			(yyval.n) = make_node(scanner, NODE_UNARY_EXPRESSION);
			(yyval.n)->u.unary_expression.type = UNARY_STRING;
			(yyval.n)->u.unary_expression.u.string = yylval.s;
		}
#line 3551 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 33: /* postfix_expression: keywords  */
#line 1231 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {
			(yyval.n) = make_node(scanner, NODE_UNARY_EXPRESSION);
			(yyval.n)->u.unary_expression.type = UNARY_STRING;
			(yyval.n)->u.unary_expression.u.string = yylval.s;
		}
#line 3561 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 34: /* postfix_expression: CTF_INTEGER_LITERAL  */
#line 1237 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {
			(yyval.n) = make_node(scanner, NODE_UNARY_EXPRESSION);
			(yyval.n)->u.unary_expression.type = UNARY_UNSIGNED_CONSTANT;
			(yyval.n)->u.unary_expression.u.unsigned_constant = (yyvsp[0].ull);
		}
#line 3571 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 35: /* postfix_expression: CTF_STRING_LITERAL  */
#line 1243 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {
			(yyval.n) = make_node(scanner, NODE_UNARY_EXPRESSION);
			(yyval.n)->u.unary_expression.type = UNARY_STRING;
			(yyval.n)->u.unary_expression.u.string = (yyvsp[0].s);
		}
#line 3581 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 36: /* postfix_expression: CTF_CHARACTER_LITERAL  */
#line 1249 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {
			(yyval.n) = make_node(scanner, NODE_UNARY_EXPRESSION);
			(yyval.n)->u.unary_expression.type = UNARY_STRING;
			(yyval.n)->u.unary_expression.u.string = (yyvsp[0].s);
		}
#line 3591 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 37: /* postfix_expression: CTF_LPAREN unary_expression CTF_RPAREN  */
#line 1255 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {
			(yyval.n) = (yyvsp[-1].n);
		}
#line 3599 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 38: /* postfix_expression: postfix_expression CTF_LSBRAC unary_expression CTF_RSBRAC  */
#line 1259 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {
			(yyval.n) = make_node(scanner, NODE_UNARY_EXPRESSION);
			(yyval.n)->u.unary_expression.type = UNARY_SBRAC;
			(yyval.n)->u.unary_expression.u.sbrac_exp = (yyvsp[-1].n);
			bt_list_splice(&((yyvsp[-3].n))->tmp_head, &((yyval.n))->tmp_head);
			bt_list_add_tail(&((yyval.n))->siblings, &((yyval.n))->tmp_head);
		}
#line 3611 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 39: /* postfix_expression: postfix_expression CTF_DOT IDENTIFIER  */
#line 1267 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {
			(yyval.n) = make_node(scanner, NODE_UNARY_EXPRESSION);
			(yyval.n)->u.unary_expression.type = UNARY_STRING;
			(yyval.n)->u.unary_expression.u.string = yylval.s;
			(yyval.n)->u.unary_expression.link = UNARY_DOTLINK;
			bt_list_splice(&((yyvsp[-2].n))->tmp_head, &((yyval.n))->tmp_head);
			bt_list_add_tail(&((yyval.n))->siblings, &((yyval.n))->tmp_head);
		}
#line 3624 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 40: /* postfix_expression: postfix_expression CTF_DOT ID_TYPE  */
#line 1276 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {
			(yyval.n) = make_node(scanner, NODE_UNARY_EXPRESSION);
			(yyval.n)->u.unary_expression.type = UNARY_STRING;
			(yyval.n)->u.unary_expression.u.string = yylval.s;
			(yyval.n)->u.unary_expression.link = UNARY_DOTLINK;
			bt_list_splice(&((yyvsp[-2].n))->tmp_head, &((yyval.n))->tmp_head);
			bt_list_add_tail(&((yyval.n))->siblings, &((yyval.n))->tmp_head);
		}
#line 3637 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 41: /* postfix_expression: postfix_expression CTF_DOT keywords  */
#line 1285 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {
			(yyval.n) = make_node(scanner, NODE_UNARY_EXPRESSION);
			(yyval.n)->u.unary_expression.type = UNARY_STRING;
			(yyval.n)->u.unary_expression.u.string = yylval.s;
			(yyval.n)->u.unary_expression.link = UNARY_DOTLINK;
			bt_list_splice(&((yyvsp[-2].n))->tmp_head, &((yyval.n))->tmp_head);
			bt_list_add_tail(&((yyval.n))->siblings, &((yyval.n))->tmp_head);
		}
#line 3650 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 42: /* postfix_expression: postfix_expression CTF_RARROW IDENTIFIER  */
#line 1294 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {
			(yyval.n) = make_node(scanner, NODE_UNARY_EXPRESSION);
			(yyval.n)->u.unary_expression.type = UNARY_STRING;
			(yyval.n)->u.unary_expression.u.string = yylval.s;
			(yyval.n)->u.unary_expression.link = UNARY_ARROWLINK;
			bt_list_splice(&((yyvsp[-2].n))->tmp_head, &((yyval.n))->tmp_head);
			bt_list_add_tail(&((yyval.n))->siblings, &((yyval.n))->tmp_head);
		}
#line 3663 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 43: /* postfix_expression: postfix_expression CTF_RARROW ID_TYPE  */
#line 1303 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {
			(yyval.n) = make_node(scanner, NODE_UNARY_EXPRESSION);
			(yyval.n)->u.unary_expression.type = UNARY_STRING;
			(yyval.n)->u.unary_expression.u.string = yylval.s;
			(yyval.n)->u.unary_expression.link = UNARY_ARROWLINK;
			bt_list_splice(&((yyvsp[-2].n))->tmp_head, &((yyval.n))->tmp_head);
			bt_list_add_tail(&((yyval.n))->siblings, &((yyval.n))->tmp_head);
		}
#line 3676 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 44: /* unary_expression: postfix_expression  */
#line 1315 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {	(yyval.n) = (yyvsp[0].n);				}
#line 3682 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 45: /* unary_expression: CTF_PLUS postfix_expression  */
#line 1317 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {
			(yyval.n) = (yyvsp[0].n);
			if ((yyval.n)->u.unary_expression.type != UNARY_UNSIGNED_CONSTANT
				&& (yyval.n)->u.unary_expression.type != UNARY_SIGNED_CONSTANT) {
				reparent_error(scanner, "expecting numeric constant");
			}
		}
#line 3694 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 46: /* unary_expression: CTF_MINUS postfix_expression  */
#line 1325 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {
			(yyval.n) = (yyvsp[0].n);
			if ((yyval.n)->u.unary_expression.type == UNARY_UNSIGNED_CONSTANT) {
				(yyval.n)->u.unary_expression.type = UNARY_SIGNED_CONSTANT;
				(yyval.n)->u.unary_expression.u.signed_constant =
					-((yyval.n)->u.unary_expression.u.unsigned_constant);
			} else if ((yyval.n)->u.unary_expression.type == UNARY_SIGNED_CONSTANT) {
				(yyval.n)->u.unary_expression.u.signed_constant =
					-((yyval.n)->u.unary_expression.u.signed_constant);
			} else {
				reparent_error(scanner, "expecting numeric constant");
			}
		}
#line 3712 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 47: /* unary_expression_or_range: unary_expression CTF_DOTDOTDOT unary_expression  */
#line 1342 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {
			(yyval.n) = (yyvsp[-2].n);
			_bt_list_splice_tail(&((yyvsp[0].n))->tmp_head, &((yyval.n))->tmp_head);
			(yyvsp[0].n)->u.unary_expression.link = UNARY_DOTDOTDOT;
		}
#line 3722 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 48: /* unary_expression_or_range: unary_expression  */
#line 1348 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {	(yyval.n) = (yyvsp[0].n);		}
#line 3728 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 49: /* declaration: declaration_specifiers CTF_SEMICOLON  */
#line 1355 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {	(yyval.n) = (yyvsp[-1].n);	}
#line 3734 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 50: /* declaration: event_declaration  */
#line 1357 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {	(yyval.n) = (yyvsp[0].n);	}
#line 3740 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 51: /* declaration: stream_declaration  */
#line 1359 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {	(yyval.n) = (yyvsp[0].n);	}
#line 3746 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 52: /* declaration: env_declaration  */
#line 1361 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {	(yyval.n) = (yyvsp[0].n);	}
#line 3752 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 53: /* declaration: trace_declaration  */
#line 1363 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {	(yyval.n) = (yyvsp[0].n);	}
#line 3758 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 54: /* declaration: clock_declaration  */
#line 1365 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {	(yyval.n) = (yyvsp[0].n);	}
#line 3764 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 55: /* declaration: callsite_declaration  */
#line 1367 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {	(yyval.n) = (yyvsp[0].n);	}
#line 3770 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 56: /* declaration: declaration_specifiers CTF_TYPEDEF declaration_specifiers field_class_declarator_list CTF_SEMICOLON  */
#line 1369 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {
			struct ctf_node *list;

			(yyval.n) = make_node(scanner, NODE_TYPEDEF);
			list = make_node(scanner, NODE_TYPE_SPECIFIER_LIST);
			(yyval.n)->u.field_class_def.field_class_specifier_list = list;
			_bt_list_splice_tail(&((yyvsp[-4].n))->u.field_class_specifier_list.head, &list->u.field_class_specifier_list.head);
			_bt_list_splice_tail(&((yyvsp[-2].n))->u.field_class_specifier_list.head, &list->u.field_class_specifier_list.head);
			_bt_list_splice_tail(&((yyvsp[-1].n))->tmp_head, &((yyval.n))->u.field_class_def.field_class_declarators);
		}
#line 3785 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 57: /* declaration: CTF_TYPEDEF declaration_specifiers field_class_declarator_list CTF_SEMICOLON  */
#line 1380 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {
			struct ctf_node *list;

			(yyval.n) = make_node(scanner, NODE_TYPEDEF);
			list = make_node(scanner, NODE_TYPE_SPECIFIER_LIST);
			(yyval.n)->u.field_class_def.field_class_specifier_list = list;
			_bt_list_splice_tail(&((yyvsp[-2].n))->u.field_class_specifier_list.head, &list->u.field_class_specifier_list.head);
			_bt_list_splice_tail(&((yyvsp[-1].n))->tmp_head, &((yyval.n))->u.field_class_def.field_class_declarators);
		}
#line 3799 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 58: /* declaration: declaration_specifiers CTF_TYPEDEF field_class_declarator_list CTF_SEMICOLON  */
#line 1390 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {
			struct ctf_node *list;

			(yyval.n) = make_node(scanner, NODE_TYPEDEF);
			list = make_node(scanner, NODE_TYPE_SPECIFIER_LIST);
			(yyval.n)->u.field_class_def.field_class_specifier_list = list;
			_bt_list_splice_tail(&((yyvsp[-3].n))->u.field_class_specifier_list.head, &list->u.field_class_specifier_list.head);
			_bt_list_splice_tail(&((yyvsp[-1].n))->tmp_head, &((yyval.n))->u.field_class_def.field_class_declarators);
		}
#line 3813 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 59: /* declaration: CTF_TYPEALIAS declaration_specifiers abstract_declarator_list CTF_TYPEASSIGN alias_declaration_specifiers alias_abstract_declarator_list CTF_SEMICOLON  */
#line 1400 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {
			struct ctf_node *list;

			(yyval.n) = make_node(scanner, NODE_TYPEALIAS);
			(yyval.n)->u.field_class_alias.target = make_node(scanner, NODE_TYPEALIAS_TARGET);
			(yyval.n)->u.field_class_alias.alias = make_node(scanner, NODE_TYPEALIAS_ALIAS);

			list = make_node(scanner, NODE_TYPE_SPECIFIER_LIST);
			(yyval.n)->u.field_class_alias.target->u.field_class_alias_target.field_class_specifier_list = list;
			_bt_list_splice_tail(&((yyvsp[-5].n))->u.field_class_specifier_list.head, &list->u.field_class_specifier_list.head);
			_bt_list_splice_tail(&((yyvsp[-4].n))->tmp_head, &((yyval.n))->u.field_class_alias.target->u.field_class_alias_target.field_class_declarators);

			list = make_node(scanner, NODE_TYPE_SPECIFIER_LIST);
			(yyval.n)->u.field_class_alias.alias->u.field_class_alias_name.field_class_specifier_list = list;
			_bt_list_splice_tail(&((yyvsp[-2].n))->u.field_class_specifier_list.head, &list->u.field_class_specifier_list.head);
			_bt_list_splice_tail(&((yyvsp[-1].n))->tmp_head, &((yyval.n))->u.field_class_alias.alias->u.field_class_alias_name.field_class_declarators);
		}
#line 3835 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 60: /* event_declaration: event_declaration_begin event_declaration_end  */
#line 1421 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {
			(yyval.n) = make_node(scanner, NODE_EVENT);
		}
#line 3843 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 61: /* event_declaration: event_declaration_begin ctf_assignment_expression_list event_declaration_end  */
#line 1425 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {
			(yyval.n) = make_node(scanner, NODE_EVENT);
			if (set_parent_node((yyvsp[-1].n), (yyval.n)))
				reparent_error(scanner, "event_declaration");
		}
#line 3853 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 62: /* event_declaration_begin: CTF_EVENT CTF_LBRAC  */
#line 1434 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {	push_scope(scanner);	}
#line 3859 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 63: /* event_declaration_end: CTF_RBRAC CTF_SEMICOLON  */
#line 1439 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {	pop_scope(scanner);	}
#line 3865 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 64: /* stream_declaration: stream_declaration_begin stream_declaration_end  */
#line 1445 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {
			(yyval.n) = make_node(scanner, NODE_STREAM);
		}
#line 3873 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 65: /* stream_declaration: stream_declaration_begin ctf_assignment_expression_list stream_declaration_end  */
#line 1449 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {
			(yyval.n) = make_node(scanner, NODE_STREAM);
			if (set_parent_node((yyvsp[-1].n), (yyval.n)))
				reparent_error(scanner, "stream_declaration");
		}
#line 3883 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 66: /* stream_declaration_begin: CTF_STREAM CTF_LBRAC  */
#line 1458 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {	push_scope(scanner);	}
#line 3889 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 67: /* stream_declaration_end: CTF_RBRAC CTF_SEMICOLON  */
#line 1463 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {	pop_scope(scanner);	}
#line 3895 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 68: /* env_declaration: env_declaration_begin env_declaration_end  */
#line 1468 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {
			(yyval.n) = make_node(scanner, NODE_ENV);
		}
#line 3903 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 69: /* env_declaration: env_declaration_begin ctf_assignment_expression_list env_declaration_end  */
#line 1472 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {
			(yyval.n) = make_node(scanner, NODE_ENV);
			if (set_parent_node((yyvsp[-1].n), (yyval.n)))
				reparent_error(scanner, "env declaration");
		}
#line 3913 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 70: /* env_declaration_begin: CTF_ENV CTF_LBRAC  */
#line 1481 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {	push_scope(scanner);	}
#line 3919 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 71: /* env_declaration_end: CTF_RBRAC CTF_SEMICOLON  */
#line 1486 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {	pop_scope(scanner);	}
#line 3925 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 72: /* trace_declaration: trace_declaration_begin trace_declaration_end  */
#line 1491 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {
			(yyval.n) = make_node(scanner, NODE_TRACE);
		}
#line 3933 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 73: /* trace_declaration: trace_declaration_begin ctf_assignment_expression_list trace_declaration_end  */
#line 1495 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {
			(yyval.n) = make_node(scanner, NODE_TRACE);
			if (set_parent_node((yyvsp[-1].n), (yyval.n)))
				reparent_error(scanner, "trace_declaration");
		}
#line 3943 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 74: /* trace_declaration_begin: CTF_TRACE CTF_LBRAC  */
#line 1504 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {	push_scope(scanner);	}
#line 3949 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 75: /* trace_declaration_end: CTF_RBRAC CTF_SEMICOLON  */
#line 1509 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {	pop_scope(scanner);	}
#line 3955 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 76: /* clock_declaration: CTF_CLOCK clock_declaration_begin clock_declaration_end  */
#line 1514 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {
			(yyval.n) = make_node(scanner, NODE_CLOCK);
		}
#line 3963 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 77: /* clock_declaration: CTF_CLOCK clock_declaration_begin ctf_assignment_expression_list clock_declaration_end  */
#line 1518 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {
			(yyval.n) = make_node(scanner, NODE_CLOCK);
			if (set_parent_node((yyvsp[-1].n), (yyval.n)))
				reparent_error(scanner, "trace_declaration");
		}
#line 3973 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 78: /* clock_declaration_begin: CTF_LBRAC  */
#line 1527 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {	push_scope(scanner);	}
#line 3979 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 79: /* clock_declaration_end: CTF_RBRAC CTF_SEMICOLON  */
#line 1532 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {	pop_scope(scanner);	}
#line 3985 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 80: /* callsite_declaration: CTF_CALLSITE callsite_declaration_begin callsite_declaration_end  */
#line 1537 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {
			(yyval.n) = make_node(scanner, NODE_CALLSITE);
		}
#line 3993 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 81: /* callsite_declaration: CTF_CALLSITE callsite_declaration_begin ctf_assignment_expression_list callsite_declaration_end  */
#line 1541 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {
			(yyval.n) = make_node(scanner, NODE_CALLSITE);
			if (set_parent_node((yyvsp[-1].n), (yyval.n)))
				reparent_error(scanner, "trace_declaration");
		}
#line 4003 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 82: /* callsite_declaration_begin: CTF_LBRAC  */
#line 1550 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {	push_scope(scanner);	}
#line 4009 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 83: /* callsite_declaration_end: CTF_RBRAC CTF_SEMICOLON  */
#line 1555 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {	pop_scope(scanner);	}
#line 4015 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 84: /* integer_declaration_specifiers: CTF_CONST  */
#line 1560 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {
			struct ctf_node *node;

			(yyval.n) = make_node(scanner, NODE_TYPE_SPECIFIER_LIST);
			node = make_node(scanner, NODE_TYPE_SPECIFIER);
			node->u.field_class_specifier.type = TYPESPEC_CONST;
			bt_list_add_tail(&node->siblings, &((yyval.n))->u.field_class_specifier_list.head);
		}
#line 4028 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 85: /* integer_declaration_specifiers: integer_field_class_specifier  */
#line 1569 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {
			struct ctf_node *node;

			(yyval.n) = make_node(scanner, NODE_TYPE_SPECIFIER_LIST);
			node = (yyvsp[0].n);
			bt_list_add_tail(&node->siblings, &((yyval.n))->u.field_class_specifier_list.head);
		}
#line 4040 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 86: /* integer_declaration_specifiers: integer_declaration_specifiers CTF_CONST  */
#line 1577 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {
			struct ctf_node *node;

			(yyval.n) = (yyvsp[-1].n);
			node = make_node(scanner, NODE_TYPE_SPECIFIER);
			node->u.field_class_specifier.type = TYPESPEC_CONST;
			bt_list_add_tail(&node->siblings, &((yyval.n))->u.field_class_specifier_list.head);
		}
#line 4053 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 87: /* integer_declaration_specifiers: integer_declaration_specifiers integer_field_class_specifier  */
#line 1586 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {
			(yyval.n) = (yyvsp[-1].n);
			bt_list_add_tail(&((yyvsp[0].n))->siblings, &((yyval.n))->u.field_class_specifier_list.head);
		}
#line 4062 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 88: /* declaration_specifiers: CTF_CONST  */
#line 1594 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {
			struct ctf_node *node;

			(yyval.n) = make_node(scanner, NODE_TYPE_SPECIFIER_LIST);
			node = make_node(scanner, NODE_TYPE_SPECIFIER);
			node->u.field_class_specifier.type = TYPESPEC_CONST;
			bt_list_add_tail(&node->siblings, &((yyval.n))->u.field_class_specifier_list.head);
		}
#line 4075 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 89: /* declaration_specifiers: field_class_specifier  */
#line 1603 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {
			struct ctf_node *node;

			(yyval.n) = make_node(scanner, NODE_TYPE_SPECIFIER_LIST);
			node = (yyvsp[0].n);
			bt_list_add_tail(&node->siblings, &((yyval.n))->u.field_class_specifier_list.head);
		}
#line 4087 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 90: /* declaration_specifiers: declaration_specifiers CTF_CONST  */
#line 1611 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {
			struct ctf_node *node;

			(yyval.n) = (yyvsp[-1].n);
			node = make_node(scanner, NODE_TYPE_SPECIFIER);
			node->u.field_class_specifier.type = TYPESPEC_CONST;
			bt_list_add_tail(&node->siblings, &((yyval.n))->u.field_class_specifier_list.head);
		}
#line 4100 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 91: /* declaration_specifiers: declaration_specifiers field_class_specifier  */
#line 1620 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {
			(yyval.n) = (yyvsp[-1].n);
			bt_list_add_tail(&((yyvsp[0].n))->siblings, &((yyval.n))->u.field_class_specifier_list.head);
		}
#line 4109 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 92: /* field_class_declarator_list: field_class_declarator  */
#line 1628 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {	(yyval.n) = (yyvsp[0].n);	}
#line 4115 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 93: /* field_class_declarator_list: field_class_declarator_list CTF_COMMA field_class_declarator  */
#line 1630 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {
			(yyval.n) = (yyvsp[-2].n);
			bt_list_add_tail(&((yyvsp[0].n))->siblings, &((yyval.n))->tmp_head);
		}
#line 4124 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 94: /* integer_field_class_specifier: CTF_CHAR  */
#line 1638 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {
			(yyval.n) = make_node(scanner, NODE_TYPE_SPECIFIER);
			(yyval.n)->u.field_class_specifier.type = TYPESPEC_CHAR;
		}
#line 4133 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 95: /* integer_field_class_specifier: CTF_SHORT  */
#line 1643 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {
			(yyval.n) = make_node(scanner, NODE_TYPE_SPECIFIER);
			(yyval.n)->u.field_class_specifier.type = TYPESPEC_SHORT;
		}
#line 4142 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 96: /* integer_field_class_specifier: CTF_INT  */
#line 1648 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {
			(yyval.n) = make_node(scanner, NODE_TYPE_SPECIFIER);
			(yyval.n)->u.field_class_specifier.type = TYPESPEC_INT;
		}
#line 4151 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 97: /* integer_field_class_specifier: CTF_LONG  */
#line 1653 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {
			(yyval.n) = make_node(scanner, NODE_TYPE_SPECIFIER);
			(yyval.n)->u.field_class_specifier.type = TYPESPEC_LONG;
		}
#line 4160 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 98: /* integer_field_class_specifier: CTF_SIGNED  */
#line 1658 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {
			(yyval.n) = make_node(scanner, NODE_TYPE_SPECIFIER);
			(yyval.n)->u.field_class_specifier.type = TYPESPEC_SIGNED;
		}
#line 4169 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 99: /* integer_field_class_specifier: CTF_UNSIGNED  */
#line 1663 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {
			(yyval.n) = make_node(scanner, NODE_TYPE_SPECIFIER);
			(yyval.n)->u.field_class_specifier.type = TYPESPEC_UNSIGNED;
		}
#line 4178 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 100: /* integer_field_class_specifier: CTF_BOOL  */
#line 1668 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {
			(yyval.n) = make_node(scanner, NODE_TYPE_SPECIFIER);
			(yyval.n)->u.field_class_specifier.type = TYPESPEC_BOOL;
		}
#line 4187 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 101: /* integer_field_class_specifier: ID_TYPE  */
#line 1673 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {
			(yyval.n) = make_node(scanner, NODE_TYPE_SPECIFIER);
			(yyval.n)->u.field_class_specifier.type = TYPESPEC_ID_TYPE;
			(yyval.n)->u.field_class_specifier.id_type = yylval.s;
		}
#line 4197 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 102: /* integer_field_class_specifier: CTF_INTEGER CTF_LBRAC CTF_RBRAC  */
#line 1679 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {
			(yyval.n) = make_node(scanner, NODE_TYPE_SPECIFIER);
			(yyval.n)->u.field_class_specifier.type = TYPESPEC_INTEGER;
			(yyval.n)->u.field_class_specifier.node = make_node(scanner, NODE_INTEGER);
		}
#line 4207 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 103: /* integer_field_class_specifier: CTF_INTEGER CTF_LBRAC ctf_assignment_expression_list CTF_RBRAC  */
#line 1685 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {
			(yyval.n) = make_node(scanner, NODE_TYPE_SPECIFIER);
			(yyval.n)->u.field_class_specifier.type = TYPESPEC_INTEGER;
			(yyval.n)->u.field_class_specifier.node = make_node(scanner, NODE_INTEGER);
			if (set_parent_node((yyvsp[-1].n), (yyval.n)->u.field_class_specifier.node))
				reparent_error(scanner, "integer reparent error");
		}
#line 4219 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 104: /* field_class_specifier: CTF_VOID  */
#line 1696 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {
			(yyval.n) = make_node(scanner, NODE_TYPE_SPECIFIER);
			(yyval.n)->u.field_class_specifier.type = TYPESPEC_VOID;
		}
#line 4228 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 105: /* field_class_specifier: CTF_CHAR  */
#line 1701 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {
			(yyval.n) = make_node(scanner, NODE_TYPE_SPECIFIER);
			(yyval.n)->u.field_class_specifier.type = TYPESPEC_CHAR;
		}
#line 4237 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 106: /* field_class_specifier: CTF_SHORT  */
#line 1706 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {
			(yyval.n) = make_node(scanner, NODE_TYPE_SPECIFIER);
			(yyval.n)->u.field_class_specifier.type = TYPESPEC_SHORT;
		}
#line 4246 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 107: /* field_class_specifier: CTF_INT  */
#line 1711 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {
			(yyval.n) = make_node(scanner, NODE_TYPE_SPECIFIER);
			(yyval.n)->u.field_class_specifier.type = TYPESPEC_INT;
		}
#line 4255 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 108: /* field_class_specifier: CTF_LONG  */
#line 1716 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {
			(yyval.n) = make_node(scanner, NODE_TYPE_SPECIFIER);
			(yyval.n)->u.field_class_specifier.type = TYPESPEC_LONG;
		}
#line 4264 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 109: /* field_class_specifier: CTF_FLOAT  */
#line 1721 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {
			(yyval.n) = make_node(scanner, NODE_TYPE_SPECIFIER);
			(yyval.n)->u.field_class_specifier.type = TYPESPEC_FLOAT;
		}
#line 4273 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 110: /* field_class_specifier: CTF_DOUBLE  */
#line 1726 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {
			(yyval.n) = make_node(scanner, NODE_TYPE_SPECIFIER);
			(yyval.n)->u.field_class_specifier.type = TYPESPEC_DOUBLE;
		}
#line 4282 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 111: /* field_class_specifier: CTF_SIGNED  */
#line 1731 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {
			(yyval.n) = make_node(scanner, NODE_TYPE_SPECIFIER);
			(yyval.n)->u.field_class_specifier.type = TYPESPEC_SIGNED;
		}
#line 4291 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 112: /* field_class_specifier: CTF_UNSIGNED  */
#line 1736 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {
			(yyval.n) = make_node(scanner, NODE_TYPE_SPECIFIER);
			(yyval.n)->u.field_class_specifier.type = TYPESPEC_UNSIGNED;
		}
#line 4300 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 113: /* field_class_specifier: CTF_BOOL  */
#line 1741 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {
			(yyval.n) = make_node(scanner, NODE_TYPE_SPECIFIER);
			(yyval.n)->u.field_class_specifier.type = TYPESPEC_BOOL;
		}
#line 4309 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 114: /* field_class_specifier: CTF_COMPLEX  */
#line 1746 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {
			(yyval.n) = make_node(scanner, NODE_TYPE_SPECIFIER);
			(yyval.n)->u.field_class_specifier.type = TYPESPEC_COMPLEX;
		}
#line 4318 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 115: /* field_class_specifier: CTF_IMAGINARY  */
#line 1751 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {
			(yyval.n) = make_node(scanner, NODE_TYPE_SPECIFIER);
			(yyval.n)->u.field_class_specifier.type = TYPESPEC_IMAGINARY;
		}
#line 4327 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 116: /* field_class_specifier: ID_TYPE  */
#line 1756 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {
			(yyval.n) = make_node(scanner, NODE_TYPE_SPECIFIER);
			(yyval.n)->u.field_class_specifier.type = TYPESPEC_ID_TYPE;
			(yyval.n)->u.field_class_specifier.id_type = yylval.s;
		}
#line 4337 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 117: /* field_class_specifier: CTF_FLOATING_POINT CTF_LBRAC CTF_RBRAC  */
#line 1762 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {
			(yyval.n) = make_node(scanner, NODE_TYPE_SPECIFIER);
			(yyval.n)->u.field_class_specifier.type = TYPESPEC_FLOATING_POINT;
			(yyval.n)->u.field_class_specifier.node = make_node(scanner, NODE_FLOATING_POINT);
		}
#line 4347 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 118: /* field_class_specifier: CTF_FLOATING_POINT CTF_LBRAC ctf_assignment_expression_list CTF_RBRAC  */
#line 1768 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {
			(yyval.n) = make_node(scanner, NODE_TYPE_SPECIFIER);
			(yyval.n)->u.field_class_specifier.type = TYPESPEC_FLOATING_POINT;
			(yyval.n)->u.field_class_specifier.node = make_node(scanner, NODE_FLOATING_POINT);
			if (set_parent_node((yyvsp[-1].n), (yyval.n)->u.field_class_specifier.node))
				reparent_error(scanner, "floating point reparent error");
		}
#line 4359 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 119: /* field_class_specifier: CTF_INTEGER CTF_LBRAC CTF_RBRAC  */
#line 1776 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {
			(yyval.n) = make_node(scanner, NODE_TYPE_SPECIFIER);
			(yyval.n)->u.field_class_specifier.type = TYPESPEC_INTEGER;
			(yyval.n)->u.field_class_specifier.node = make_node(scanner, NODE_INTEGER);
		}
#line 4369 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 120: /* field_class_specifier: CTF_INTEGER CTF_LBRAC ctf_assignment_expression_list CTF_RBRAC  */
#line 1782 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {
			(yyval.n) = make_node(scanner, NODE_TYPE_SPECIFIER);
			(yyval.n)->u.field_class_specifier.type = TYPESPEC_INTEGER;
			(yyval.n)->u.field_class_specifier.node = make_node(scanner, NODE_INTEGER);
			if (set_parent_node((yyvsp[-1].n), (yyval.n)->u.field_class_specifier.node))
				reparent_error(scanner, "integer reparent error");
		}
#line 4381 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 121: /* field_class_specifier: CTF_STRING  */
#line 1790 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {
			(yyval.n) = make_node(scanner, NODE_TYPE_SPECIFIER);
			(yyval.n)->u.field_class_specifier.type = TYPESPEC_STRING;
			(yyval.n)->u.field_class_specifier.node = make_node(scanner, NODE_STRING);
		}
#line 4391 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 122: /* field_class_specifier: CTF_STRING CTF_LBRAC CTF_RBRAC  */
#line 1796 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {
			(yyval.n) = make_node(scanner, NODE_TYPE_SPECIFIER);
			(yyval.n)->u.field_class_specifier.type = TYPESPEC_STRING;
			(yyval.n)->u.field_class_specifier.node = make_node(scanner, NODE_STRING);
		}
#line 4401 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 123: /* field_class_specifier: CTF_STRING CTF_LBRAC ctf_assignment_expression_list CTF_RBRAC  */
#line 1802 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {
			(yyval.n) = make_node(scanner, NODE_TYPE_SPECIFIER);
			(yyval.n)->u.field_class_specifier.type = TYPESPEC_STRING;
			(yyval.n)->u.field_class_specifier.node = make_node(scanner, NODE_STRING);
			if (set_parent_node((yyvsp[-1].n), (yyval.n)->u.field_class_specifier.node))
				reparent_error(scanner, "string reparent error");
		}
#line 4413 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 124: /* field_class_specifier: CTF_ENUM enum_field_class_specifier  */
#line 1810 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {
			(yyval.n) = make_node(scanner, NODE_TYPE_SPECIFIER);
			(yyval.n)->u.field_class_specifier.type = TYPESPEC_ENUM;
			(yyval.n)->u.field_class_specifier.node = (yyvsp[0].n);
		}
#line 4423 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 125: /* field_class_specifier: CTF_VARIANT variant_field_class_specifier  */
#line 1816 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {
			(yyval.n) = make_node(scanner, NODE_TYPE_SPECIFIER);
			(yyval.n)->u.field_class_specifier.type = TYPESPEC_VARIANT;
			(yyval.n)->u.field_class_specifier.node = (yyvsp[0].n);
		}
#line 4433 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 126: /* field_class_specifier: CTF_STRUCT struct_class_specifier  */
#line 1822 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {
			(yyval.n) = make_node(scanner, NODE_TYPE_SPECIFIER);
			(yyval.n)->u.field_class_specifier.type = TYPESPEC_STRUCT;
			(yyval.n)->u.field_class_specifier.node = (yyvsp[0].n);
		}
#line 4443 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 127: /* struct_class_specifier: struct_declaration_begin struct_or_variant_declaration_list struct_declaration_end  */
#line 1831 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {
			(yyval.n) = make_node(scanner, NODE_STRUCT);
			(yyval.n)->u._struct.has_body = 1;
			if ((yyvsp[-1].n) && set_parent_node((yyvsp[-1].n), (yyval.n)))
				reparent_error(scanner, "struct reparent error");
		}
#line 4454 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 128: /* struct_class_specifier: IDENTIFIER struct_declaration_begin struct_or_variant_declaration_list struct_declaration_end  */
#line 1838 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {
			(yyval.n) = make_node(scanner, NODE_STRUCT);
			(yyval.n)->u._struct.has_body = 1;
			(yyval.n)->u._struct.name = (yyvsp[-3].s);
			if ((yyvsp[-1].n) && set_parent_node((yyvsp[-1].n), (yyval.n)))
				reparent_error(scanner, "struct reparent error");
		}
#line 4466 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 129: /* struct_class_specifier: ID_TYPE struct_declaration_begin struct_or_variant_declaration_list struct_declaration_end  */
#line 1846 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {
			(yyval.n) = make_node(scanner, NODE_STRUCT);
			(yyval.n)->u._struct.has_body = 1;
			(yyval.n)->u._struct.name = (yyvsp[-3].s);
			if ((yyvsp[-1].n) && set_parent_node((yyvsp[-1].n), (yyval.n)))
				reparent_error(scanner, "struct reparent error");
		}
#line 4478 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 130: /* struct_class_specifier: IDENTIFIER  */
#line 1854 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {
			(yyval.n) = make_node(scanner, NODE_STRUCT);
			(yyval.n)->u._struct.has_body = 0;
			(yyval.n)->u._struct.name = (yyvsp[0].s);
		}
#line 4488 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 131: /* struct_class_specifier: ID_TYPE  */
#line 1860 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {
			(yyval.n) = make_node(scanner, NODE_STRUCT);
			(yyval.n)->u._struct.has_body = 0;
			(yyval.n)->u._struct.name = (yyvsp[0].s);
		}
#line 4498 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 132: /* struct_class_specifier: struct_declaration_begin struct_or_variant_declaration_list struct_declaration_end CTF_TOK_ALIGN CTF_LPAREN unary_expression CTF_RPAREN  */
#line 1866 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {
			(yyval.n) = make_node(scanner, NODE_STRUCT);
			(yyval.n)->u._struct.has_body = 1;
			bt_list_add_tail(&((yyvsp[-1].n))->siblings, &(yyval.n)->u._struct.min_align);
			if ((yyvsp[-5].n) && set_parent_node((yyvsp[-5].n), (yyval.n)))
				reparent_error(scanner, "struct reparent error");
		}
#line 4510 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 133: /* struct_class_specifier: IDENTIFIER struct_declaration_begin struct_or_variant_declaration_list struct_declaration_end CTF_TOK_ALIGN CTF_LPAREN unary_expression CTF_RPAREN  */
#line 1874 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {
			(yyval.n) = make_node(scanner, NODE_STRUCT);
			(yyval.n)->u._struct.has_body = 1;
			(yyval.n)->u._struct.name = (yyvsp[-7].s);
			bt_list_add_tail(&((yyvsp[-1].n))->siblings, &(yyval.n)->u._struct.min_align);
			if ((yyvsp[-5].n) && set_parent_node((yyvsp[-5].n), (yyval.n)))
				reparent_error(scanner, "struct reparent error");
		}
#line 4523 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 134: /* struct_class_specifier: ID_TYPE struct_declaration_begin struct_or_variant_declaration_list struct_declaration_end CTF_TOK_ALIGN CTF_LPAREN unary_expression CTF_RPAREN  */
#line 1883 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {
			(yyval.n) = make_node(scanner, NODE_STRUCT);
			(yyval.n)->u._struct.has_body = 1;
			(yyval.n)->u._struct.name = (yyvsp[-7].s);
			bt_list_add_tail(&((yyvsp[-1].n))->siblings, &(yyval.n)->u._struct.min_align);
			if ((yyvsp[-5].n) && set_parent_node((yyvsp[-5].n), (yyval.n)))
				reparent_error(scanner, "struct reparent error");
		}
#line 4536 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 135: /* struct_declaration_begin: CTF_LBRAC  */
#line 1895 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {	push_scope(scanner);	}
#line 4542 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 136: /* struct_declaration_end: CTF_RBRAC  */
#line 1900 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {	pop_scope(scanner);	}
#line 4548 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 137: /* variant_field_class_specifier: variant_declaration_begin struct_or_variant_declaration_list variant_declaration_end  */
#line 1905 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {
			(yyval.n) = make_node(scanner, NODE_VARIANT);
			(yyval.n)->u.variant.has_body = 1;
			if ((yyvsp[-1].n) && set_parent_node((yyvsp[-1].n), (yyval.n)))
				reparent_error(scanner, "variant reparent error");
		}
#line 4559 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 138: /* variant_field_class_specifier: CTF_LT IDENTIFIER CTF_GT variant_declaration_begin struct_or_variant_declaration_list variant_declaration_end  */
#line 1912 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {
			(yyval.n) = make_node(scanner, NODE_VARIANT);
			(yyval.n)->u.variant.has_body = 1;
			(yyval.n)->u.variant.choice = (yyvsp[-4].s);
			if ((yyvsp[-1].n) && set_parent_node((yyvsp[-1].n), (yyval.n)))
				reparent_error(scanner, "variant reparent error");
		}
#line 4571 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 139: /* variant_field_class_specifier: CTF_LT ID_TYPE CTF_GT variant_declaration_begin struct_or_variant_declaration_list variant_declaration_end  */
#line 1920 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {
			(yyval.n) = make_node(scanner, NODE_VARIANT);
			(yyval.n)->u.variant.has_body = 1;
			(yyval.n)->u.variant.choice = (yyvsp[-4].s);
			if ((yyvsp[-1].n) && set_parent_node((yyvsp[-1].n), (yyval.n)))
				reparent_error(scanner, "variant reparent error");
		}
#line 4583 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 140: /* variant_field_class_specifier: IDENTIFIER variant_declaration_begin struct_or_variant_declaration_list variant_declaration_end  */
#line 1928 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {
			(yyval.n) = make_node(scanner, NODE_VARIANT);
			(yyval.n)->u.variant.has_body = 1;
			(yyval.n)->u.variant.name = (yyvsp[-3].s);
			if ((yyvsp[-1].n) && set_parent_node((yyvsp[-1].n), (yyval.n)))
				reparent_error(scanner, "variant reparent error");
		}
#line 4595 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 141: /* variant_field_class_specifier: IDENTIFIER CTF_LT IDENTIFIER CTF_GT variant_declaration_begin struct_or_variant_declaration_list variant_declaration_end  */
#line 1936 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {
			(yyval.n) = make_node(scanner, NODE_VARIANT);
			(yyval.n)->u.variant.has_body = 1;
			(yyval.n)->u.variant.name = (yyvsp[-6].s);
			(yyval.n)->u.variant.choice = (yyvsp[-4].s);
			if ((yyvsp[-1].n) && set_parent_node((yyvsp[-1].n), (yyval.n)))
				reparent_error(scanner, "variant reparent error");
		}
#line 4608 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 142: /* variant_field_class_specifier: IDENTIFIER CTF_LT IDENTIFIER CTF_GT  */
#line 1945 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {
			(yyval.n) = make_node(scanner, NODE_VARIANT);
			(yyval.n)->u.variant.has_body = 0;
			(yyval.n)->u.variant.name = (yyvsp[-3].s);
			(yyval.n)->u.variant.choice = (yyvsp[-1].s);
		}
#line 4619 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 143: /* variant_field_class_specifier: IDENTIFIER CTF_LT ID_TYPE CTF_GT variant_declaration_begin struct_or_variant_declaration_list variant_declaration_end  */
#line 1952 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {
			(yyval.n) = make_node(scanner, NODE_VARIANT);
			(yyval.n)->u.variant.has_body = 1;
			(yyval.n)->u.variant.name = (yyvsp[-6].s);
			(yyval.n)->u.variant.choice = (yyvsp[-4].s);
			if ((yyvsp[-1].n) && set_parent_node((yyvsp[-1].n), (yyval.n)))
				reparent_error(scanner, "variant reparent error");
		}
#line 4632 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 144: /* variant_field_class_specifier: IDENTIFIER CTF_LT ID_TYPE CTF_GT  */
#line 1961 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {
			(yyval.n) = make_node(scanner, NODE_VARIANT);
			(yyval.n)->u.variant.has_body = 0;
			(yyval.n)->u.variant.name = (yyvsp[-3].s);
			(yyval.n)->u.variant.choice = (yyvsp[-1].s);
		}
#line 4643 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 145: /* variant_field_class_specifier: ID_TYPE variant_declaration_begin struct_or_variant_declaration_list variant_declaration_end  */
#line 1968 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {
			(yyval.n) = make_node(scanner, NODE_VARIANT);
			(yyval.n)->u.variant.has_body = 1;
			(yyval.n)->u.variant.name = (yyvsp[-3].s);
			if ((yyvsp[-1].n) && set_parent_node((yyvsp[-1].n), (yyval.n)))
				reparent_error(scanner, "variant reparent error");
		}
#line 4655 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 146: /* variant_field_class_specifier: ID_TYPE CTF_LT IDENTIFIER CTF_GT variant_declaration_begin struct_or_variant_declaration_list variant_declaration_end  */
#line 1976 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {
			(yyval.n) = make_node(scanner, NODE_VARIANT);
			(yyval.n)->u.variant.has_body = 1;
			(yyval.n)->u.variant.name = (yyvsp[-6].s);
			(yyval.n)->u.variant.choice = (yyvsp[-4].s);
			if ((yyvsp[-1].n) && set_parent_node((yyvsp[-1].n), (yyval.n)))
				reparent_error(scanner, "variant reparent error");
		}
#line 4668 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 147: /* variant_field_class_specifier: ID_TYPE CTF_LT IDENTIFIER CTF_GT  */
#line 1985 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {
			(yyval.n) = make_node(scanner, NODE_VARIANT);
			(yyval.n)->u.variant.has_body = 0;
			(yyval.n)->u.variant.name = (yyvsp[-3].s);
			(yyval.n)->u.variant.choice = (yyvsp[-1].s);
		}
#line 4679 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 148: /* variant_field_class_specifier: ID_TYPE CTF_LT ID_TYPE CTF_GT variant_declaration_begin struct_or_variant_declaration_list variant_declaration_end  */
#line 1992 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {
			(yyval.n) = make_node(scanner, NODE_VARIANT);
			(yyval.n)->u.variant.has_body = 1;
			(yyval.n)->u.variant.name = (yyvsp[-6].s);
			(yyval.n)->u.variant.choice = (yyvsp[-4].s);
			if ((yyvsp[-1].n) && set_parent_node((yyvsp[-1].n), (yyval.n)))
				reparent_error(scanner, "variant reparent error");
		}
#line 4692 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 149: /* variant_field_class_specifier: ID_TYPE CTF_LT ID_TYPE CTF_GT  */
#line 2001 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {
			(yyval.n) = make_node(scanner, NODE_VARIANT);
			(yyval.n)->u.variant.has_body = 0;
			(yyval.n)->u.variant.name = (yyvsp[-3].s);
			(yyval.n)->u.variant.choice = (yyvsp[-1].s);
		}
#line 4703 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 150: /* variant_declaration_begin: CTF_LBRAC  */
#line 2011 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {	push_scope(scanner);	}
#line 4709 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 151: /* variant_declaration_end: CTF_RBRAC  */
#line 2016 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {	pop_scope(scanner);	}
#line 4715 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 152: /* enum_field_class_specifier: CTF_LBRAC enumerator_list CTF_RBRAC  */
#line 2021 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {
			(yyval.n) = make_node(scanner, NODE_ENUM);
			(yyval.n)->u._enum.has_body = 1;
			_bt_list_splice_tail(&((yyvsp[-1].n))->tmp_head, &((yyval.n))->u._enum.enumerator_list);
		}
#line 4725 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 153: /* enum_field_class_specifier: CTF_COLON integer_declaration_specifiers CTF_LBRAC enumerator_list CTF_RBRAC  */
#line 2027 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {
			(yyval.n) = make_node(scanner, NODE_ENUM);
			(yyval.n)->u._enum.has_body = 1;
			((yyval.n))->u._enum.container_field_class = (yyvsp[-3].n);
			_bt_list_splice_tail(&((yyvsp[-1].n))->tmp_head, &((yyval.n))->u._enum.enumerator_list);
		}
#line 4736 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 154: /* enum_field_class_specifier: IDENTIFIER CTF_LBRAC enumerator_list CTF_RBRAC  */
#line 2034 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {
			(yyval.n) = make_node(scanner, NODE_ENUM);
			(yyval.n)->u._enum.has_body = 1;
			(yyval.n)->u._enum.enum_id = (yyvsp[-3].s);
			_bt_list_splice_tail(&((yyvsp[-1].n))->tmp_head, &((yyval.n))->u._enum.enumerator_list);
		}
#line 4747 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 155: /* enum_field_class_specifier: IDENTIFIER CTF_COLON integer_declaration_specifiers CTF_LBRAC enumerator_list CTF_RBRAC  */
#line 2041 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {
			(yyval.n) = make_node(scanner, NODE_ENUM);
			(yyval.n)->u._enum.has_body = 1;
			(yyval.n)->u._enum.enum_id = (yyvsp[-5].s);
			((yyval.n))->u._enum.container_field_class = (yyvsp[-3].n);
			_bt_list_splice_tail(&((yyvsp[-1].n))->tmp_head, &((yyval.n))->u._enum.enumerator_list);
		}
#line 4759 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 156: /* enum_field_class_specifier: ID_TYPE CTF_LBRAC enumerator_list CTF_RBRAC  */
#line 2049 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {
			(yyval.n) = make_node(scanner, NODE_ENUM);
			(yyval.n)->u._enum.has_body = 1;
			(yyval.n)->u._enum.enum_id = (yyvsp[-3].s);
			_bt_list_splice_tail(&((yyvsp[-1].n))->tmp_head, &((yyval.n))->u._enum.enumerator_list);
		}
#line 4770 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 157: /* enum_field_class_specifier: ID_TYPE CTF_COLON integer_declaration_specifiers CTF_LBRAC enumerator_list CTF_RBRAC  */
#line 2056 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {
			(yyval.n) = make_node(scanner, NODE_ENUM);
			(yyval.n)->u._enum.has_body = 1;
			(yyval.n)->u._enum.enum_id = (yyvsp[-5].s);
			((yyval.n))->u._enum.container_field_class = (yyvsp[-3].n);
			_bt_list_splice_tail(&((yyvsp[-1].n))->tmp_head, &((yyval.n))->u._enum.enumerator_list);
		}
#line 4782 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 158: /* enum_field_class_specifier: CTF_LBRAC enumerator_list CTF_COMMA CTF_RBRAC  */
#line 2064 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {
			(yyval.n) = make_node(scanner, NODE_ENUM);
			(yyval.n)->u._enum.has_body = 1;
			_bt_list_splice_tail(&((yyvsp[-2].n))->tmp_head, &((yyval.n))->u._enum.enumerator_list);
		}
#line 4792 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 159: /* enum_field_class_specifier: CTF_COLON integer_declaration_specifiers CTF_LBRAC enumerator_list CTF_COMMA CTF_RBRAC  */
#line 2070 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {
			(yyval.n) = make_node(scanner, NODE_ENUM);
			(yyval.n)->u._enum.has_body = 1;
			((yyval.n))->u._enum.container_field_class = (yyvsp[-4].n);
			_bt_list_splice_tail(&((yyvsp[-2].n))->tmp_head, &((yyval.n))->u._enum.enumerator_list);
		}
#line 4803 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 160: /* enum_field_class_specifier: IDENTIFIER CTF_LBRAC enumerator_list CTF_COMMA CTF_RBRAC  */
#line 2077 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {
			(yyval.n) = make_node(scanner, NODE_ENUM);
			(yyval.n)->u._enum.has_body = 1;
			(yyval.n)->u._enum.enum_id = (yyvsp[-4].s);
			_bt_list_splice_tail(&((yyvsp[-2].n))->tmp_head, &((yyval.n))->u._enum.enumerator_list);
		}
#line 4814 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 161: /* enum_field_class_specifier: IDENTIFIER CTF_COLON integer_declaration_specifiers CTF_LBRAC enumerator_list CTF_COMMA CTF_RBRAC  */
#line 2084 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {
			(yyval.n) = make_node(scanner, NODE_ENUM);
			(yyval.n)->u._enum.has_body = 1;
			(yyval.n)->u._enum.enum_id = (yyvsp[-6].s);
			((yyval.n))->u._enum.container_field_class = (yyvsp[-4].n);
			_bt_list_splice_tail(&((yyvsp[-2].n))->tmp_head, &((yyval.n))->u._enum.enumerator_list);
		}
#line 4826 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 162: /* enum_field_class_specifier: IDENTIFIER  */
#line 2092 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {
			(yyval.n) = make_node(scanner, NODE_ENUM);
			(yyval.n)->u._enum.has_body = 0;
			(yyval.n)->u._enum.enum_id = (yyvsp[0].s);
		}
#line 4836 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 163: /* enum_field_class_specifier: ID_TYPE CTF_LBRAC enumerator_list CTF_COMMA CTF_RBRAC  */
#line 2098 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {
			(yyval.n) = make_node(scanner, NODE_ENUM);
			(yyval.n)->u._enum.has_body = 1;
			(yyval.n)->u._enum.enum_id = (yyvsp[-4].s);
			_bt_list_splice_tail(&((yyvsp[-2].n))->tmp_head, &((yyval.n))->u._enum.enumerator_list);
		}
#line 4847 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 164: /* enum_field_class_specifier: ID_TYPE CTF_COLON integer_declaration_specifiers CTF_LBRAC enumerator_list CTF_COMMA CTF_RBRAC  */
#line 2105 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {
			(yyval.n) = make_node(scanner, NODE_ENUM);
			(yyval.n)->u._enum.has_body = 1;
			(yyval.n)->u._enum.enum_id = (yyvsp[-6].s);
			((yyval.n))->u._enum.container_field_class = (yyvsp[-4].n);
			_bt_list_splice_tail(&((yyvsp[-2].n))->tmp_head, &((yyval.n))->u._enum.enumerator_list);
		}
#line 4859 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 165: /* enum_field_class_specifier: ID_TYPE  */
#line 2113 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {
			(yyval.n) = make_node(scanner, NODE_ENUM);
			(yyval.n)->u._enum.has_body = 0;
			(yyval.n)->u._enum.enum_id = (yyvsp[0].s);
		}
#line 4869 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 166: /* struct_or_variant_declaration_list: %empty  */
#line 2122 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {	(yyval.n) = NULL;	}
#line 4875 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 167: /* struct_or_variant_declaration_list: struct_or_variant_declaration_list struct_or_variant_declaration  */
#line 2124 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {
			if ((yyvsp[-1].n)) {
				(yyval.n) = (yyvsp[-1].n);
				bt_list_add_tail(&((yyvsp[0].n))->siblings, &((yyval.n))->tmp_head);
			} else {
				(yyval.n) = (yyvsp[0].n);
				bt_list_add_tail(&((yyval.n))->siblings, &((yyval.n))->tmp_head);
			}
		}
#line 4889 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 168: /* struct_or_variant_declaration: declaration_specifiers struct_or_variant_declarator_list CTF_SEMICOLON  */
#line 2137 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {
			struct ctf_node *list;

			list = make_node(scanner, NODE_TYPE_SPECIFIER_LIST);
			_bt_list_splice_tail(&((yyvsp[-2].n))->u.field_class_specifier_list.head, &list->u.field_class_specifier_list.head);
			(yyval.n) = make_node(scanner, NODE_STRUCT_OR_VARIANT_DECLARATION);
			((yyval.n))->u.struct_or_variant_declaration.field_class_specifier_list = list;
			_bt_list_splice_tail(&((yyvsp[-1].n))->tmp_head, &((yyval.n))->u.struct_or_variant_declaration.field_class_declarators);
		}
#line 4903 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 169: /* struct_or_variant_declaration: declaration_specifiers CTF_TYPEDEF declaration_specifiers field_class_declarator_list CTF_SEMICOLON  */
#line 2147 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {
			struct ctf_node *list;

			(yyval.n) = make_node(scanner, NODE_TYPEDEF);
			list = make_node(scanner, NODE_TYPE_SPECIFIER_LIST);
			(yyval.n)->u.field_class_def.field_class_specifier_list = list;
			_bt_list_splice_tail(&((yyvsp[-4].n))->u.field_class_specifier_list.head, &list->u.field_class_specifier_list.head);
			_bt_list_splice_tail(&((yyvsp[-2].n))->u.field_class_specifier_list.head, &list->u.field_class_specifier_list.head);
			_bt_list_splice_tail(&((yyvsp[-1].n))->tmp_head, &((yyval.n))->u.field_class_def.field_class_declarators);
		}
#line 4918 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 170: /* struct_or_variant_declaration: CTF_TYPEDEF declaration_specifiers field_class_declarator_list CTF_SEMICOLON  */
#line 2158 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {
			struct ctf_node *list;

			(yyval.n) = make_node(scanner, NODE_TYPEDEF);
			list = make_node(scanner, NODE_TYPE_SPECIFIER_LIST);
			(yyval.n)->u.field_class_def.field_class_specifier_list = list;
			_bt_list_splice_tail(&((yyvsp[-2].n))->u.field_class_specifier_list.head, &list->u.field_class_specifier_list.head);
			_bt_list_splice_tail(&((yyvsp[-1].n))->tmp_head, &((yyval.n))->u.field_class_def.field_class_declarators);
		}
#line 4932 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 171: /* struct_or_variant_declaration: declaration_specifiers CTF_TYPEDEF field_class_declarator_list CTF_SEMICOLON  */
#line 2168 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {
			struct ctf_node *list;

			list = make_node(scanner, NODE_TYPE_SPECIFIER_LIST);
			_bt_list_splice_tail(&((yyvsp[-3].n))->u.field_class_specifier_list.head, &list->u.field_class_specifier_list.head);
			(yyval.n) = make_node(scanner, NODE_TYPEDEF);
			((yyval.n))->u.struct_or_variant_declaration.field_class_specifier_list = list;
			_bt_list_splice_tail(&((yyvsp[-1].n))->tmp_head, &((yyval.n))->u.field_class_def.field_class_declarators);
		}
#line 4946 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 172: /* struct_or_variant_declaration: CTF_TYPEALIAS declaration_specifiers abstract_declarator_list CTF_TYPEASSIGN alias_declaration_specifiers alias_abstract_declarator_list CTF_SEMICOLON  */
#line 2178 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {
			struct ctf_node *list;

			(yyval.n) = make_node(scanner, NODE_TYPEALIAS);
			(yyval.n)->u.field_class_alias.target = make_node(scanner, NODE_TYPEALIAS_TARGET);
			(yyval.n)->u.field_class_alias.alias = make_node(scanner, NODE_TYPEALIAS_ALIAS);

			list = make_node(scanner, NODE_TYPE_SPECIFIER_LIST);
			(yyval.n)->u.field_class_alias.target->u.field_class_alias_target.field_class_specifier_list = list;
			_bt_list_splice_tail(&((yyvsp[-5].n))->u.field_class_specifier_list.head, &list->u.field_class_specifier_list.head);
			_bt_list_splice_tail(&((yyvsp[-4].n))->tmp_head, &((yyval.n))->u.field_class_alias.target->u.field_class_alias_target.field_class_declarators);

			list = make_node(scanner, NODE_TYPE_SPECIFIER_LIST);
			(yyval.n)->u.field_class_alias.alias->u.field_class_alias_name.field_class_specifier_list = list;
			_bt_list_splice_tail(&((yyvsp[-2].n))->u.field_class_specifier_list.head, &list->u.field_class_specifier_list.head);
			_bt_list_splice_tail(&((yyvsp[-1].n))->tmp_head, &((yyval.n))->u.field_class_alias.alias->u.field_class_alias_name.field_class_declarators);
		}
#line 4968 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 173: /* alias_declaration_specifiers: CTF_CONST  */
#line 2199 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {
			struct ctf_node *node;

			(yyval.n) = make_node(scanner, NODE_TYPE_SPECIFIER_LIST);
			node = make_node(scanner, NODE_TYPE_SPECIFIER);
			node->u.field_class_specifier.type = TYPESPEC_CONST;
			bt_list_add_tail(&node->siblings, &((yyval.n))->u.field_class_specifier_list.head);
		}
#line 4981 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 174: /* alias_declaration_specifiers: field_class_specifier  */
#line 2208 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {
			struct ctf_node *node;

			(yyval.n) = make_node(scanner, NODE_TYPE_SPECIFIER_LIST);
			node = (yyvsp[0].n);
			bt_list_add_tail(&node->siblings, &((yyval.n))->u.field_class_specifier_list.head);
		}
#line 4993 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 175: /* alias_declaration_specifiers: IDENTIFIER  */
#line 2216 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {
			struct ctf_node *node;

			add_type(scanner, (yyvsp[0].s));
			(yyval.n) = make_node(scanner, NODE_TYPE_SPECIFIER_LIST);
			node = make_node(scanner, NODE_TYPE_SPECIFIER);
			node->u.field_class_specifier.type = TYPESPEC_ID_TYPE;
			node->u.field_class_specifier.id_type = yylval.s;
			bt_list_add_tail(&node->siblings, &((yyval.n))->u.field_class_specifier_list.head);
		}
#line 5008 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 176: /* alias_declaration_specifiers: alias_declaration_specifiers CTF_CONST  */
#line 2227 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {
			struct ctf_node *node;

			(yyval.n) = (yyvsp[-1].n);
			node = make_node(scanner, NODE_TYPE_SPECIFIER);
			node->u.field_class_specifier.type = TYPESPEC_CONST;
			bt_list_add_tail(&node->siblings, &((yyval.n))->u.field_class_specifier_list.head);
		}
#line 5021 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 177: /* alias_declaration_specifiers: alias_declaration_specifiers field_class_specifier  */
#line 2236 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {
			(yyval.n) = (yyvsp[-1].n);
			bt_list_add_tail(&((yyvsp[0].n))->siblings, &((yyval.n))->u.field_class_specifier_list.head);
		}
#line 5030 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 178: /* alias_declaration_specifiers: alias_declaration_specifiers IDENTIFIER  */
#line 2241 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {
			struct ctf_node *node;

			add_type(scanner, (yyvsp[0].s));
			(yyval.n) = (yyvsp[-1].n);
			node = make_node(scanner, NODE_TYPE_SPECIFIER);
			node->u.field_class_specifier.type = TYPESPEC_ID_TYPE;
			node->u.field_class_specifier.id_type = yylval.s;
			bt_list_add_tail(&node->siblings, &((yyval.n))->u.field_class_specifier_list.head);
		}
#line 5045 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 179: /* struct_or_variant_declarator_list: struct_or_variant_declarator  */
#line 2255 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {	(yyval.n) = (yyvsp[0].n);	}
#line 5051 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 180: /* struct_or_variant_declarator_list: struct_or_variant_declarator_list CTF_COMMA struct_or_variant_declarator  */
#line 2257 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {
			(yyval.n) = (yyvsp[-2].n);
			bt_list_add_tail(&((yyvsp[0].n))->siblings, &((yyval.n))->tmp_head);
		}
#line 5060 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 181: /* struct_or_variant_declarator: declarator  */
#line 2265 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {	(yyval.n) = (yyvsp[0].n);	}
#line 5066 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 182: /* struct_or_variant_declarator: CTF_COLON unary_expression  */
#line 2267 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {	(yyval.n) = (yyvsp[0].n);	}
#line 5072 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 183: /* struct_or_variant_declarator: declarator CTF_COLON unary_expression  */
#line 2269 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {
			(yyval.n) = (yyvsp[-2].n);
			if (set_parent_node((yyvsp[0].n), (yyvsp[-2].n)))
				reparent_error(scanner, "struct_or_variant_declarator");
		}
#line 5082 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 184: /* enumerator_list: enumerator  */
#line 2278 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {	(yyval.n) = (yyvsp[0].n);	}
#line 5088 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 185: /* enumerator_list: enumerator_list CTF_COMMA enumerator  */
#line 2280 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {
			(yyval.n) = (yyvsp[-2].n);
			bt_list_add_tail(&((yyvsp[0].n))->siblings, &((yyval.n))->tmp_head);
		}
#line 5097 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 186: /* enumerator: IDENTIFIER  */
#line 2288 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {
			(yyval.n) = make_node(scanner, NODE_ENUMERATOR);
			(yyval.n)->u.enumerator.id = (yyvsp[0].s);
		}
#line 5106 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 187: /* enumerator: ID_TYPE  */
#line 2293 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {
			(yyval.n) = make_node(scanner, NODE_ENUMERATOR);
			(yyval.n)->u.enumerator.id = (yyvsp[0].s);
		}
#line 5115 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 188: /* enumerator: keywords  */
#line 2298 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {
			(yyval.n) = make_node(scanner, NODE_ENUMERATOR);
			(yyval.n)->u.enumerator.id = (yyvsp[0].s);
		}
#line 5124 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 189: /* enumerator: CTF_STRING_LITERAL  */
#line 2303 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {
			(yyval.n) = make_node(scanner, NODE_ENUMERATOR);
			(yyval.n)->u.enumerator.id = (yyvsp[0].s);
		}
#line 5133 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 190: /* enumerator: IDENTIFIER CTF_EQUAL unary_expression_or_range  */
#line 2308 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {
			(yyval.n) = make_node(scanner, NODE_ENUMERATOR);
			(yyval.n)->u.enumerator.id = (yyvsp[-2].s);
			bt_list_splice(&((yyvsp[0].n))->tmp_head, &((yyval.n))->u.enumerator.values);
		}
#line 5143 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 191: /* enumerator: ID_TYPE CTF_EQUAL unary_expression_or_range  */
#line 2314 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {
			(yyval.n) = make_node(scanner, NODE_ENUMERATOR);
			(yyval.n)->u.enumerator.id = (yyvsp[-2].s);
			bt_list_splice(&((yyvsp[0].n))->tmp_head, &((yyval.n))->u.enumerator.values);
		}
#line 5153 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 192: /* enumerator: keywords CTF_EQUAL unary_expression_or_range  */
#line 2320 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {
			(yyval.n) = make_node(scanner, NODE_ENUMERATOR);
			(yyval.n)->u.enumerator.id = (yyvsp[-2].s);
			bt_list_splice(&((yyvsp[0].n))->tmp_head, &((yyval.n))->u.enumerator.values);
		}
#line 5163 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 193: /* enumerator: CTF_STRING_LITERAL CTF_EQUAL unary_expression_or_range  */
#line 2326 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {
			(yyval.n) = make_node(scanner, NODE_ENUMERATOR);
			(yyval.n)->u.enumerator.id = (yyvsp[-2].s);
			bt_list_splice(&((yyvsp[0].n))->tmp_head, &((yyval.n))->u.enumerator.values);
		}
#line 5173 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 194: /* abstract_declarator_list: abstract_declarator  */
#line 2335 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {	(yyval.n) = (yyvsp[0].n);	}
#line 5179 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 195: /* abstract_declarator_list: abstract_declarator_list CTF_COMMA abstract_declarator  */
#line 2337 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {
			(yyval.n) = (yyvsp[-2].n);
			bt_list_add_tail(&((yyvsp[0].n))->siblings, &((yyval.n))->tmp_head);
		}
#line 5188 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 196: /* abstract_declarator: direct_abstract_declarator  */
#line 2345 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {	(yyval.n) = (yyvsp[0].n);	}
#line 5194 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 197: /* abstract_declarator: pointer direct_abstract_declarator  */
#line 2347 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {
			(yyval.n) = (yyvsp[0].n);
			bt_list_splice(&((yyvsp[-1].n))->tmp_head, &((yyval.n))->u.field_class_declarator.pointers);
		}
#line 5203 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 198: /* direct_abstract_declarator: %empty  */
#line 2355 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {
			(yyval.n) = make_node(scanner, NODE_TYPE_DECLARATOR);
                        (yyval.n)->u.field_class_declarator.type = TYPEDEC_ID;
			/* id is NULL */
		}
#line 5213 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 199: /* direct_abstract_declarator: IDENTIFIER  */
#line 2361 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {
			(yyval.n) = make_node(scanner, NODE_TYPE_DECLARATOR);
			(yyval.n)->u.field_class_declarator.type = TYPEDEC_ID;
			(yyval.n)->u.field_class_declarator.u.id = (yyvsp[0].s);
		}
#line 5223 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 200: /* direct_abstract_declarator: CTF_LPAREN abstract_declarator CTF_RPAREN  */
#line 2367 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {
			(yyval.n) = make_node(scanner, NODE_TYPE_DECLARATOR);
			(yyval.n)->u.field_class_declarator.type = TYPEDEC_NESTED;
			(yyval.n)->u.field_class_declarator.u.nested.field_class_declarator = (yyvsp[-1].n);
		}
#line 5233 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 201: /* direct_abstract_declarator: direct_abstract_declarator CTF_LSBRAC unary_expression CTF_RSBRAC  */
#line 2373 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {
			(yyval.n) = make_node(scanner, NODE_TYPE_DECLARATOR);
			(yyval.n)->u.field_class_declarator.type = TYPEDEC_NESTED;
			(yyval.n)->u.field_class_declarator.u.nested.field_class_declarator = (yyvsp[-3].n);
			BT_INIT_LIST_HEAD(&((yyval.n))->u.field_class_declarator.u.nested.length);
			_bt_list_splice_tail(&((yyvsp[-1].n))->tmp_head, &((yyval.n))->u.field_class_declarator.u.nested.length);
		}
#line 5245 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 202: /* direct_abstract_declarator: direct_abstract_declarator CTF_LSBRAC CTF_RSBRAC  */
#line 2381 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {
			(yyval.n) = make_node(scanner, NODE_TYPE_DECLARATOR);
			(yyval.n)->u.field_class_declarator.type = TYPEDEC_NESTED;
			(yyval.n)->u.field_class_declarator.u.nested.field_class_declarator = (yyvsp[-2].n);
			(yyval.n)->u.field_class_declarator.u.nested.abstract_array = 1;
		}
#line 5256 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 203: /* alias_abstract_declarator_list: alias_abstract_declarator  */
#line 2391 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {	(yyval.n) = (yyvsp[0].n);	}
#line 5262 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 204: /* alias_abstract_declarator_list: alias_abstract_declarator_list CTF_COMMA alias_abstract_declarator  */
#line 2393 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {
			(yyval.n) = (yyvsp[-2].n);
			bt_list_add_tail(&((yyvsp[0].n))->siblings, &((yyval.n))->tmp_head);
		}
#line 5271 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 205: /* alias_abstract_declarator: direct_alias_abstract_declarator  */
#line 2401 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {	(yyval.n) = (yyvsp[0].n);	}
#line 5277 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 206: /* alias_abstract_declarator: pointer direct_alias_abstract_declarator  */
#line 2403 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {
			(yyval.n) = (yyvsp[0].n);
			bt_list_splice(&((yyvsp[-1].n))->tmp_head, &((yyval.n))->u.field_class_declarator.pointers);
		}
#line 5286 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 207: /* direct_alias_abstract_declarator: %empty  */
#line 2411 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {
			(yyval.n) = make_node(scanner, NODE_TYPE_DECLARATOR);
                        (yyval.n)->u.field_class_declarator.type = TYPEDEC_ID;
			/* id is NULL */
		}
#line 5296 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 208: /* direct_alias_abstract_declarator: CTF_LPAREN alias_abstract_declarator CTF_RPAREN  */
#line 2417 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {
			(yyval.n) = make_node(scanner, NODE_TYPE_DECLARATOR);
			(yyval.n)->u.field_class_declarator.type = TYPEDEC_NESTED;
			(yyval.n)->u.field_class_declarator.u.nested.field_class_declarator = (yyvsp[-1].n);
		}
#line 5306 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 209: /* direct_alias_abstract_declarator: direct_alias_abstract_declarator CTF_LSBRAC unary_expression CTF_RSBRAC  */
#line 2423 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {
			(yyval.n) = make_node(scanner, NODE_TYPE_DECLARATOR);
			(yyval.n)->u.field_class_declarator.type = TYPEDEC_NESTED;
			(yyval.n)->u.field_class_declarator.u.nested.field_class_declarator = (yyvsp[-3].n);
			BT_INIT_LIST_HEAD(&((yyval.n))->u.field_class_declarator.u.nested.length);
			_bt_list_splice_tail(&((yyvsp[-1].n))->tmp_head, &((yyval.n))->u.field_class_declarator.u.nested.length);
		}
#line 5318 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 210: /* direct_alias_abstract_declarator: direct_alias_abstract_declarator CTF_LSBRAC CTF_RSBRAC  */
#line 2431 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {
			(yyval.n) = make_node(scanner, NODE_TYPE_DECLARATOR);
			(yyval.n)->u.field_class_declarator.type = TYPEDEC_NESTED;
			(yyval.n)->u.field_class_declarator.u.nested.field_class_declarator = (yyvsp[-2].n);
			(yyval.n)->u.field_class_declarator.u.nested.abstract_array = 1;
		}
#line 5329 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 211: /* declarator: direct_declarator  */
#line 2441 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {	(yyval.n) = (yyvsp[0].n);	}
#line 5335 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 212: /* declarator: pointer direct_declarator  */
#line 2443 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {
			(yyval.n) = (yyvsp[0].n);
			bt_list_splice(&((yyvsp[-1].n))->tmp_head, &((yyval.n))->u.field_class_declarator.pointers);
		}
#line 5344 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 213: /* direct_declarator: IDENTIFIER  */
#line 2451 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {
			(yyval.n) = make_node(scanner, NODE_TYPE_DECLARATOR);
			(yyval.n)->u.field_class_declarator.type = TYPEDEC_ID;
			(yyval.n)->u.field_class_declarator.u.id = (yyvsp[0].s);
		}
#line 5354 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 214: /* direct_declarator: CTF_LPAREN declarator CTF_RPAREN  */
#line 2457 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {
			(yyval.n) = make_node(scanner, NODE_TYPE_DECLARATOR);
			(yyval.n)->u.field_class_declarator.type = TYPEDEC_NESTED;
			(yyval.n)->u.field_class_declarator.u.nested.field_class_declarator = (yyvsp[-1].n);
		}
#line 5364 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 215: /* direct_declarator: direct_declarator CTF_LSBRAC unary_expression CTF_RSBRAC  */
#line 2463 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {
			(yyval.n) = make_node(scanner, NODE_TYPE_DECLARATOR);
			(yyval.n)->u.field_class_declarator.type = TYPEDEC_NESTED;
			(yyval.n)->u.field_class_declarator.u.nested.field_class_declarator = (yyvsp[-3].n);
			BT_INIT_LIST_HEAD(&((yyval.n))->u.field_class_declarator.u.nested.length);
			_bt_list_splice_tail(&((yyvsp[-1].n))->tmp_head, &((yyval.n))->u.field_class_declarator.u.nested.length);
		}
#line 5376 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 216: /* field_class_declarator: direct_field_class_declarator  */
#line 2474 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {	(yyval.n) = (yyvsp[0].n);	}
#line 5382 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 217: /* field_class_declarator: pointer direct_field_class_declarator  */
#line 2476 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {
			(yyval.n) = (yyvsp[0].n);
			bt_list_splice(&((yyvsp[-1].n))->tmp_head, &((yyval.n))->u.field_class_declarator.pointers);
		}
#line 5391 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 218: /* direct_field_class_declarator: IDENTIFIER  */
#line 2484 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {
			add_type(scanner, (yyvsp[0].s));
			(yyval.n) = make_node(scanner, NODE_TYPE_DECLARATOR);
			(yyval.n)->u.field_class_declarator.type = TYPEDEC_ID;
			(yyval.n)->u.field_class_declarator.u.id = (yyvsp[0].s);
		}
#line 5402 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 219: /* direct_field_class_declarator: CTF_LPAREN field_class_declarator CTF_RPAREN  */
#line 2491 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {
			(yyval.n) = make_node(scanner, NODE_TYPE_DECLARATOR);
			(yyval.n)->u.field_class_declarator.type = TYPEDEC_NESTED;
			(yyval.n)->u.field_class_declarator.u.nested.field_class_declarator = (yyvsp[-1].n);
		}
#line 5412 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 220: /* direct_field_class_declarator: direct_field_class_declarator CTF_LSBRAC unary_expression CTF_RSBRAC  */
#line 2497 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {
			(yyval.n) = make_node(scanner, NODE_TYPE_DECLARATOR);
			(yyval.n)->u.field_class_declarator.type = TYPEDEC_NESTED;
			(yyval.n)->u.field_class_declarator.u.nested.field_class_declarator = (yyvsp[-3].n);
			BT_INIT_LIST_HEAD(&((yyval.n))->u.field_class_declarator.u.nested.length);
			_bt_list_splice_tail(&((yyvsp[-1].n))->tmp_head, &((yyval.n))->u.field_class_declarator.u.nested.length);
		}
#line 5424 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 221: /* pointer: CTF_STAR  */
#line 2508 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {
			(yyval.n) = make_node(scanner, NODE_POINTER);
		}
#line 5432 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 222: /* pointer: CTF_STAR pointer  */
#line 2512 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {
			(yyval.n) = make_node(scanner, NODE_POINTER);
			bt_list_splice(&((yyvsp[0].n))->tmp_head, &((yyval.n))->tmp_head);
		}
#line 5441 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 223: /* pointer: CTF_STAR type_qualifier_list pointer  */
#line 2517 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {
			(yyval.n) = make_node(scanner, NODE_POINTER);
			(yyval.n)->u.pointer.const_qualifier = 1;
			bt_list_splice(&((yyvsp[0].n))->tmp_head, &((yyval.n))->tmp_head);
		}
#line 5451 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 226: /* ctf_assignment_expression_list: ctf_assignment_expression CTF_SEMICOLON  */
#line 2534 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {	(yyval.n) = (yyvsp[-1].n);	}
#line 5457 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 227: /* ctf_assignment_expression_list: ctf_assignment_expression_list ctf_assignment_expression CTF_SEMICOLON  */
#line 2536 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {
			(yyval.n) = (yyvsp[-2].n);
			bt_list_add_tail(&((yyvsp[-1].n))->siblings, &((yyval.n))->tmp_head);
		}
#line 5466 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 228: /* ctf_assignment_expression: unary_expression CTF_EQUAL unary_expression  */
#line 2544 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {
			/*
			 * Because we have left and right, cannot use
			 * set_parent_node.
			 */
			(yyval.n) = make_node(scanner, NODE_CTF_EXPRESSION);
			_bt_list_splice_tail(&((yyvsp[-2].n))->tmp_head, &((yyval.n))->u.ctf_expression.left);
			if ((yyvsp[-2].n)->u.unary_expression.type != UNARY_STRING)
				reparent_error(scanner, "ctf_assignment_expression left expects string");
			_bt_list_splice_tail(&((yyvsp[0].n))->tmp_head, &((yyval.n))->u.ctf_expression.right);
		}
#line 5482 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 229: /* ctf_assignment_expression: unary_expression CTF_TYPEASSIGN declaration_specifiers  */
#line 2556 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {
			/*
			 * Because we have left and right, cannot use
			 * set_parent_node.
			 */
			(yyval.n) = make_node(scanner, NODE_CTF_EXPRESSION);
			_bt_list_splice_tail(&((yyvsp[-2].n))->tmp_head, &((yyval.n))->u.ctf_expression.left);
			if ((yyvsp[-2].n)->u.unary_expression.type != UNARY_STRING)
				reparent_error(scanner, "ctf_assignment_expression left expects string");
			bt_list_add_tail(&((yyvsp[0].n))->siblings, &((yyval.n))->u.ctf_expression.right);
		}
#line 5498 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 230: /* ctf_assignment_expression: declaration_specifiers CTF_TYPEDEF declaration_specifiers field_class_declarator_list  */
#line 2568 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {
			struct ctf_node *list;

			list = make_node(scanner, NODE_TYPE_SPECIFIER_LIST);
			_bt_list_splice_tail(&((yyvsp[-3].n))->u.field_class_specifier_list.head, &list->u.field_class_specifier_list.head);
			_bt_list_splice_tail(&((yyvsp[-1].n))->u.field_class_specifier_list.head, &list->u.field_class_specifier_list.head);
			(yyval.n) = make_node(scanner, NODE_TYPEDEF);
			((yyval.n))->u.struct_or_variant_declaration.field_class_specifier_list = list;
			_bt_list_splice_tail(&((yyvsp[0].n))->tmp_head, &((yyval.n))->u.field_class_def.field_class_declarators);
		}
#line 5513 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 231: /* ctf_assignment_expression: CTF_TYPEDEF declaration_specifiers field_class_declarator_list  */
#line 2579 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {
			struct ctf_node *list;

			(yyval.n) = make_node(scanner, NODE_TYPEDEF);
			list = make_node(scanner, NODE_TYPE_SPECIFIER_LIST);
			(yyval.n)->u.field_class_def.field_class_specifier_list = list;
			_bt_list_splice_tail(&((yyvsp[-1].n))->u.field_class_specifier_list.head, &list->u.field_class_specifier_list.head);
			_bt_list_splice_tail(&((yyvsp[0].n))->tmp_head, &((yyval.n))->u.field_class_def.field_class_declarators);
		}
#line 5527 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 232: /* ctf_assignment_expression: declaration_specifiers CTF_TYPEDEF field_class_declarator_list  */
#line 2589 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {
			struct ctf_node *list;

			list = make_node(scanner, NODE_TYPE_SPECIFIER_LIST);
			_bt_list_splice_tail(&((yyvsp[-2].n))->u.field_class_specifier_list.head, &list->u.field_class_specifier_list.head);
			(yyval.n) = make_node(scanner, NODE_TYPEDEF);
			((yyval.n))->u.struct_or_variant_declaration.field_class_specifier_list = list;
			_bt_list_splice_tail(&((yyvsp[0].n))->tmp_head, &((yyval.n))->u.field_class_def.field_class_declarators);
		}
#line 5541 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;

  case 233: /* ctf_assignment_expression: CTF_TYPEALIAS declaration_specifiers abstract_declarator_list CTF_TYPEASSIGN alias_declaration_specifiers alias_abstract_declarator_list  */
#line 2599 "plugins/ctf/common/src/metadata/tsdl/parser.ypp"
                {
			struct ctf_node *list;

			(yyval.n) = make_node(scanner, NODE_TYPEALIAS);
			(yyval.n)->u.field_class_alias.target = make_node(scanner, NODE_TYPEALIAS_TARGET);
			(yyval.n)->u.field_class_alias.alias = make_node(scanner, NODE_TYPEALIAS_ALIAS);

			list = make_node(scanner, NODE_TYPE_SPECIFIER_LIST);
			(yyval.n)->u.field_class_alias.target->u.field_class_alias_target.field_class_specifier_list = list;
			_bt_list_splice_tail(&((yyvsp[-4].n))->u.field_class_specifier_list.head, &list->u.field_class_specifier_list.head);
			_bt_list_splice_tail(&((yyvsp[-3].n))->tmp_head, &((yyval.n))->u.field_class_alias.target->u.field_class_alias_target.field_class_declarators);

			list = make_node(scanner, NODE_TYPE_SPECIFIER_LIST);
			(yyval.n)->u.field_class_alias.alias->u.field_class_alias_name.field_class_specifier_list = list;
			_bt_list_splice_tail(&((yyvsp[-1].n))->u.field_class_specifier_list.head, &list->u.field_class_specifier_list.head);
			_bt_list_splice_tail(&((yyvsp[0].n))->tmp_head, &((yyval.n))->u.field_class_alias.alias->u.field_class_alias_name.field_class_declarators);
		}
#line 5563 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"
    break;


#line 5567 "plugins/ctf/common/src/metadata/tsdl/parser.cpp"

      default: break;
    }
  /* User semantic actions sometimes alter yychar, and that requires
     that yytoken be updated with the new translation.  We take the
     approach of translating immediately before every use of yytoken.
     One alternative is translating here after every semantic action,
     but that translation would be missed if the semantic action invokes
     YYABORT, YYACCEPT, or YYERROR immediately after altering yychar or
     if it invokes YYBACKUP.  In the case of YYABORT or YYACCEPT, an
     incorrect destructor might then be invoked immediately.  In the
     case of YYERROR or YYBACKUP, subsequent parser actions might lead
     to an incorrect destructor call or verbose syntax error message
     before the lookahead is translated.  */
  YY_SYMBOL_PRINT ("-> $$ =", YY_CAST (yysymbol_kind_t, yyr1[yyn]), &yyval, &yyloc);

  YYPOPSTACK (yylen);
  yylen = 0;

  *++yyvsp = yyval;

  /* Now 'shift' the result of the reduction.  Determine what state
     that goes to, based on the state we popped back to and the rule
     number reduced by.  */
  {
    const int yylhs = yyr1[yyn] - YYNTOKENS;
    const int yyi = yypgoto[yylhs] + *yyssp;
    yystate = (0 <= yyi && yyi <= YYLAST && yycheck[yyi] == *yyssp
               ? yytable[yyi]
               : yydefgoto[yylhs]);
  }

  goto yynewstate;


/*--------------------------------------.
| yyerrlab -- here on detecting error.  |
`--------------------------------------*/
yyerrlab:
  /* Make sure we have latest lookahead translation.  See comments at
     user semantic actions for why this is necessary.  */
  yytoken = yychar == YYEMPTY ? YYSYMBOL_YYEMPTY : YYTRANSLATE (yychar);
  /* If not already recovering from an error, report this error.  */
  if (!yyerrstatus)
    {
      ++yynerrs;
      {
        yypcontext_t yyctx
          = {yyssp, yytoken};
        char const *yymsgp = YY_("syntax error");
        int yysyntax_error_status;
        yysyntax_error_status = yysyntax_error (&yymsg_alloc, &yymsg, &yyctx);
        if (yysyntax_error_status == 0)
          yymsgp = yymsg;
        else if (yysyntax_error_status == -1)
          {
            if (yymsg != yymsgbuf)
              YYSTACK_FREE (yymsg);
            yymsg = YY_CAST (char *,
                             YYSTACK_ALLOC (YY_CAST (YYSIZE_T, yymsg_alloc)));
            if (yymsg)
              {
                yysyntax_error_status
                  = yysyntax_error (&yymsg_alloc, &yymsg, &yyctx);
                yymsgp = yymsg;
              }
            else
              {
                yymsg = yymsgbuf;
                yymsg_alloc = sizeof yymsgbuf;
                yysyntax_error_status = YYENOMEM;
              }
          }
        yyerror (scanner, yyscanner, yymsgp);
        if (yysyntax_error_status == YYENOMEM)
          YYNOMEM;
      }
    }

  if (yyerrstatus == 3)
    {
      /* If just tried and failed to reuse lookahead token after an
         error, discard it.  */

      if (yychar <= YYEOF)
        {
          /* Return failure if at end of input.  */
          if (yychar == YYEOF)
            YYABORT;
        }
      else
        {
          yydestruct ("Error: discarding",
                      yytoken, &yylval, scanner, yyscanner);
          yychar = YYEMPTY;
        }
    }

  /* Else will try to reuse lookahead token after shifting the error
     token.  */
  goto yyerrlab1;


/*---------------------------------------------------.
| yyerrorlab -- error raised explicitly by YYERROR.  |
`---------------------------------------------------*/
yyerrorlab:
  /* Pacify compilers when the user code never invokes YYERROR and the
     label yyerrorlab therefore never appears in user code.  */
  if (0)
    YYERROR;
  ++yynerrs;

  /* Do not reclaim the symbols of the rule whose action triggered
     this YYERROR.  */
  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);
  yystate = *yyssp;
  goto yyerrlab1;


/*-------------------------------------------------------------.
| yyerrlab1 -- common code for both syntax error and YYERROR.  |
`-------------------------------------------------------------*/
yyerrlab1:
  yyerrstatus = 3;      /* Each real token shifted decrements this.  */

  /* Pop stack until we find a state that shifts the error token.  */
  for (;;)
    {
      yyn = yypact[yystate];
      if (!yypact_value_is_default (yyn))
        {
          yyn += YYSYMBOL_YYerror;
          if (0 <= yyn && yyn <= YYLAST && yycheck[yyn] == YYSYMBOL_YYerror)
            {
              yyn = yytable[yyn];
              if (0 < yyn)
                break;
            }
        }

      /* Pop the current state because it cannot handle the error token.  */
      if (yyssp == yyss)
        YYABORT;


      yydestruct ("Error: popping",
                  YY_ACCESSING_SYMBOL (yystate), yyvsp, scanner, yyscanner);
      YYPOPSTACK (1);
      yystate = *yyssp;
      YY_STACK_PRINT (yyss, yyssp);
    }

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END


  /* Shift the error token.  */
  YY_SYMBOL_PRINT ("Shifting", YY_ACCESSING_SYMBOL (yyn), yyvsp, yylsp);

  yystate = yyn;
  goto yynewstate;


/*-------------------------------------.
| yyacceptlab -- YYACCEPT comes here.  |
`-------------------------------------*/
yyacceptlab:
  yyresult = 0;
  goto yyreturnlab;


/*-----------------------------------.
| yyabortlab -- YYABORT comes here.  |
`-----------------------------------*/
yyabortlab:
  yyresult = 1;
  goto yyreturnlab;


/*-----------------------------------------------------------.
| yyexhaustedlab -- YYNOMEM (memory exhaustion) comes here.  |
`-----------------------------------------------------------*/
yyexhaustedlab:
  yyerror (scanner, yyscanner, YY_("memory exhausted"));
  yyresult = 2;
  goto yyreturnlab;


/*----------------------------------------------------------.
| yyreturnlab -- parsing is finished, clean up and return.  |
`----------------------------------------------------------*/
yyreturnlab:
  if (yychar != YYEMPTY)
    {
      /* Make sure we have latest lookahead translation.  See comments at
         user semantic actions for why this is necessary.  */
      yytoken = YYTRANSLATE (yychar);
      yydestruct ("Cleanup: discarding lookahead",
                  yytoken, &yylval, scanner, yyscanner);
    }
  /* Do not reclaim the symbols of the rule whose action triggered
     this YYABORT or YYACCEPT.  */
  YYPOPSTACK (yylen);
  YY_STACK_PRINT (yyss, yyssp);
  while (yyssp != yyss)
    {
      yydestruct ("Cleanup: popping",
                  YY_ACCESSING_SYMBOL (+*yyssp), yyvsp, scanner, yyscanner);
      YYPOPSTACK (1);
    }
#ifndef yyoverflow
  if (yyss != yyssa)
    YYSTACK_FREE (yyss);
#endif
  if (yymsg != yymsgbuf)
    YYSTACK_FREE (yymsg);
  return yyresult;
}

