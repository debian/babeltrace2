'\" t
.\"     Title: babeltrace2-sink.ctf.fs
.\"    Author: [see the "AUTHORS" section]
.\" Generator: DocBook XSL Stylesheets vsnapshot <http://docbook.sf.net/>
.\"      Date: 16 October 2024
.\"    Manual: Babeltrace\ \&2 manual
.\"    Source: Babeltrace 2.1.0
.\"  Language: English
.\"
.TH "BABELTRACE2\-SINK\&." "7" "16 October 2024" "Babeltrace 2\&.1\&.0" "Babeltrace\ \&2 manual"
.\" -----------------------------------------------------------------
.\" * Define some portability stuff
.\" -----------------------------------------------------------------
.\" ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
.\" http://bugs.debian.org/507673
.\" http://lists.gnu.org/archive/html/groff/2009-02/msg00013.html
.\" ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
.ie \n(.g .ds Aq \(aq
.el       .ds Aq '
.\" -----------------------------------------------------------------
.\" * set default formatting
.\" -----------------------------------------------------------------
.\" disable hyphenation
.nh
.\" disable justification (adjust text to left margin only)
.ad l
.\" -----------------------------------------------------------------
.\" * MAIN CONTENT STARTS HERE *
.\" -----------------------------------------------------------------
.SH "NAME"
babeltrace2-sink.ctf.fs \- Babeltrace 2: File system CTF sink component class
.SH "DESCRIPTION"
.sp
A Babeltrace\ \&2 \fBsink.ctf.fs\fR component writes the messages it consumes to one or more CTF (see <https://diamon.org/ctf/>)\ \&1\&.8 or CTF\ \&2 traces on the file system\&.
.sp
.if n \{\
.RS 4
.\}
.nf
            +\-\-\-\-\-\-\-\-\-\-\-\-\-+
            | sink\&.ctf\&.fs |
            |             +\-\-> CTF trace(s) on
Messages \-\->@ in          |    the file system
            +\-\-\-\-\-\-\-\-\-\-\-\-\-+
.fi
.if n \{\
.RE
.\}
.sp
See \fBbabeltrace2-intro\fR(7) to learn more about the Babeltrace\ \&2 project and its core concepts\&.
.sp
The \fBctf-version\fR parameter controls the CTF version of the resulting traces\&.
.sp
As of this version, a \fBsink.ctf.fs\fR component may only write:
.PP
CTF\ \&1\&.8 traces
.RS 4
When working under version\ \&0 of the Message Interchange Protocol (MIP)\&.
.RE
.PP
CTF\ \&2 traces
.RS 4
When working under version\ \&1 of the MIP\&.
.RE
.sp
Therefore, the "get supported MIP versions" method of a \fBsink.ctf.fs\fR component class reports to support only MIP\ \&0 with the \fBctf-version\fR parameter set to\ \&\fB1\fR and only MIP\ \&1 with \fBctf-version\fR set to\ \&\fB2\fR\&.
.sp
A \fBsink.ctf.fs\fR component doesn\(cqt merge traces: it writes the messages of different input traces to different output traces\&.
.SS "Trace IR to CTF 1\&.8 translation"
.sp
A \fBsink.ctf.fs\fR component makes a best effort to write CTF\ \&1\&.8 traces that are semantically equivalent to the input traces\&. As of this version, with the \fBctf-version\fR parameter set to \fB1\fR, the following field class translations can occur:
.sp
.RS 4
.ie n \{\
\h'-04'\(bu\h'+03'\c
.\}
.el \{\
.sp -1
.IP \(bu 2.3
.\}
The component translates a boolean field class to a CTF unsigned 8\-bit integer field class\&.
.sp
The value of the unsigned integer field is\ \&0 when the value of the boolean field is false and\ \&1 when it\(cqs true\&.
.RE
.sp
.RS 4
.ie n \{\
\h'-04'\(bu\h'+03'\c
.\}
.el \{\
.sp -1
.IP \(bu 2.3
.\}
The component translates a bit array field to a CTF unsigned integer field class having the same length\&.
.RE
.sp
.RS 4
.ie n \{\
\h'-04'\(bu\h'+03'\c
.\}
.el \{\
.sp -1
.IP \(bu 2.3
.\}
The component translates an option field class to a CTF variant field class where the options are an empty structure field class and the optional field class itself\&.
.sp
The empty structure field is selected when the option field has no field\&.
.RE
.sp
In all the cases above, the component adds a comment in the metadata stream, above the field class, to indicate that a special translation occurred\&.
.sp
The component doesn\(cqt translate any user attribute: a CTF\ \&1\&.8 metadata stream has no way to represent them\&.
.SS "Trace IR to CTF 2 translation"
.sp
As of this version, with the \fBctf-version\fR parameter set to \fB2\fR:
.sp
.RS 4
.ie n \{\
\h'-04'\(bu\h'+03'\c
.\}
.el \{\
.sp -1
.IP \(bu 2.3
.\}
The component sets the
\fBsink.ctf.fs\fR
attribute, under the
\fBbabeltrace.org,2020\fR
namespace, of the preamble fragment to
\fBtrue\fR\&.
.RE
.sp
.RS 4
.ie n \{\
\h'-04'\(bu\h'+03'\c
.\}
.el \{\
.sp -1
.IP \(bu 2.3
.\}
The structure member class names of packet context field classes having a CTF\ \&2 role have the metadata stream UUID as a prefix to avoid clashes with user member classes\&.
.sp
For example, the packet content length member class could be named
\fBafe9ed0e-ce73-413f-8956-83663bab2047-content_size\fR\&.
.RE
.sp
.RS 4
.ie n \{\
\h'-04'\(bu\h'+03'\c
.\}
.el \{\
.sp -1
.IP \(bu 2.3
.\}
The component translates an event class log level to the
\fBlog-level\fR
attribute under the
\fBbabeltrace.org,2020\fR
namespace as such:
.PP
\fBBT_EVENT_CLASS_LOG_LEVEL_EMERGENCY\fR
.RS 4
\fB"emergency"\fR
.RE
.PP
\fBBT_EVENT_CLASS_LOG_LEVEL_ALERT\fR
.RS 4
\fB"alert"\fR
.RE
.PP
\fBBT_EVENT_CLASS_LOG_LEVEL_CRITICAL\fR
.RS 4
\fB"critical"\fR
.RE
.PP
\fBBT_EVENT_CLASS_LOG_LEVEL_ERROR\fR
.RS 4
\fB"error"\fR
.RE
.PP
\fBBT_EVENT_CLASS_LOG_LEVEL_WARNING\fR
.RS 4
\fB"warning"\fR
.RE
.PP
\fBBT_EVENT_CLASS_LOG_LEVEL_NOTICE\fR
.RS 4
\fB"notice"\fR
.RE
.PP
\fBBT_EVENT_CLASS_LOG_LEVEL_INFO\fR
.RS 4
\fB"info"\fR
.RE
.PP
\fBBT_EVENT_CLASS_LOG_LEVEL_DEBUG_SYSTEM\fR
.RS 4
\fB"debug:system"\fR
.RE
.PP
\fBBT_EVENT_CLASS_LOG_LEVEL_DEBUG_PROGRAM\fR
.RS 4
\fB"debug:program"\fR
.RE
.PP
\fBBT_EVENT_CLASS_LOG_LEVEL_DEBUG_PROCESS\fR
.RS 4
\fB"debug:process"\fR
.RE
.PP
\fBBT_EVENT_CLASS_LOG_LEVEL_DEBUG_MODULE\fR
.RS 4
\fB"debug:module"\fR
.RE
.PP
\fBBT_EVENT_CLASS_LOG_LEVEL_DEBUG_UNIT\fR
.RS 4
\fB"debug:unit"\fR
.RE
.PP
\fBBT_EVENT_CLASS_LOG_LEVEL_DEBUG_FUNCTION\fR
.RS 4
\fB"debug:function"\fR
.RE
.PP
\fBBT_EVENT_CLASS_LOG_LEVEL_DEBUG_LINE\fR
.RS 4
\fB"debug:line"\fR
.RE
.PP
\fBBT_EVENT_CLASS_LOG_LEVEL_DEBUG\fR
.RS 4
\fB"debug:debug"\fR
.RE
.RE
.sp
.RS 4
.ie n \{\
\h'-04'\(bu\h'+03'\c
.\}
.el \{\
.sp -1
.IP \(bu 2.3
.\}
The component translates an event class EMF URI to the
\fBemf-uri\fR
attribute under the
\fBbabeltrace.org,2020\fR
namespace as a JSON string\&.
.RE
.sp
.RS 4
.ie n \{\
\h'-04'\(bu\h'+03'\c
.\}
.el \{\
.sp -1
.IP \(bu 2.3
.\}
When the component needs to generate a length/selector field class
\fI\fBF\fR\fR
for dynamic field classes without a length/selector field:
.sp
.RS 4
.ie n \{\
\h'-04'\(bu\h'+03'\c
.\}
.el \{\
.sp -1
.IP \(bu 2.3
.\}
The structure member name of
\fI\fBF\fR\fR
is unspecified\&.
.RE
.sp
.RS 4
.ie n \{\
\h'-04'\(bu\h'+03'\c
.\}
.el \{\
.sp -1
.IP \(bu 2.3
.\}
\fI\fBF\fR\fR
contains the attribute
\fBis-key-only\fR, under the
\fBbabeltrace.org,2020\fR
namespace, set to
\fBtrue\fR\&.
.RE
.RE
.sp
.RS 4
.ie n \{\
\h'-04'\(bu\h'+03'\c
.\}
.el \{\
.sp -1
.IP \(bu 2.3
.\}
The component doesn\(cqt translate trace and stream user attributes: a CTF\ \&2 metadata stream has no way to represent them\&.
.RE
.SS "Input message constraints"
.sp
Because of limitations in CTF regarding how discarded events and packets are encoded:
.sp
.RS 4
.ie n \{\
\h'-04'\(bu\h'+03'\c
.\}
.el \{\
.sp -1
.IP \(bu 2.3
.\}
If a stream class supports discarded events and the
\fBignore-discarded-events\fR
parameter is NOT true:
.sp
.RS 4
.ie n \{\
\h'-04'\(bu\h'+03'\c
.\}
.el \{\
.sp -1
.IP \(bu 2.3
.\}
The stream class must support packets\&.
.RE
.sp
.RS 4
.ie n \{\
\h'-04'\(bu\h'+03'\c
.\}
.el \{\
.sp -1
.IP \(bu 2.3
.\}
Discarded events messages must have times\&.
.RE
.sp
.RS 4
.ie n \{\
\h'-04'\(bu\h'+03'\c
.\}
.el \{\
.sp -1
.IP \(bu 2.3
.\}
Any discarded events message must occur between a packet end and a packet beginning message\&.
.RE
.sp
.RS 4
.ie n \{\
\h'-04'\(bu\h'+03'\c
.\}
.el \{\
.sp -1
.IP \(bu 2.3
.\}
The beginning time of a discarded events message must be the same as the time of the last packet end message\&.
.RE
.sp
.RS 4
.ie n \{\
\h'-04'\(bu\h'+03'\c
.\}
.el \{\
.sp -1
.IP \(bu 2.3
.\}
The end time of a discarded events message must be the same as the time of the next packet end message\&.
.RE
.sp
.RS 4
.ie n \{\
\h'-04'\(bu\h'+03'\c
.\}
.el \{\
.sp -1
.IP \(bu 2.3
.\}
Time ranges of discarded events messages must not overlap\&.
.RE
.RE
.sp
.RS 4
.ie n \{\
\h'-04'\(bu\h'+03'\c
.\}
.el \{\
.sp -1
.IP \(bu 2.3
.\}
If a stream class supports discarded packets and the
\fBignore-discarded-packets\fR
parameter is NOT true:
.sp
.RS 4
.ie n \{\
\h'-04'\(bu\h'+03'\c
.\}
.el \{\
.sp -1
.IP \(bu 2.3
.\}
The stream class must support packets\&.
.RE
.sp
.RS 4
.ie n \{\
\h'-04'\(bu\h'+03'\c
.\}
.el \{\
.sp -1
.IP \(bu 2.3
.\}
Discarded packets messages must have times\&.
.RE
.sp
.RS 4
.ie n \{\
\h'-04'\(bu\h'+03'\c
.\}
.el \{\
.sp -1
.IP \(bu 2.3
.\}
The beginning time of a discarded events message must be the same as the time of the last packet end message\&.
.RE
.sp
.RS 4
.ie n \{\
\h'-04'\(bu\h'+03'\c
.\}
.el \{\
.sp -1
.IP \(bu 2.3
.\}
The end time of a discarded events message must be the same as the time of the next packet beginning message\&.
.RE
.sp
.RS 4
.ie n \{\
\h'-04'\(bu\h'+03'\c
.\}
.el \{\
.sp -1
.IP \(bu 2.3
.\}
Time ranges of discarded packets messages must not overlap\&.
.RE
.RE
.sp
The messages which a \fBsource.ctf.fs\fR component creates satisfy all the requirements above\&.
.sp
If a discarded events or packets message has no events/packets count, then the \fBsink.ctf.fs\fR component adds\ \&1 to the counter of the corresponding CTF stream\&.
.SS "Alignment and byte order"
.sp
A \fBsink.ctf.fs\fR component always aligns data fields as such:
.PP
Fixed\-length bit array fields with a size which isn\(cqt a multiple of\ \&8
.RS 4
1\-bit\&.
.RE
.PP
All other scalar fields (fixed\-length bit array, null\-terminated string, BLOB)
.RS 4
8\-bit\&.
.RE
.sp
The component writes fields using the native byte order of the machine\&. As of this version, there\(cqs no way to force a custom byte order\&.
.SS "Output path"
.sp
The path of a CTF trace is the directory which directly contains the metadata and data stream files\&.
.sp
The current strategy to build a path in which to write the streams of a given input trace is, in this order:
.sp
.RS 4
.ie n \{\
\h'-04' 1.\h'+01'\c
.\}
.el \{\
.sp -1
.IP "  1." 4.2
.\}
If the
\fBassume-single-trace\fR
parameter is true, then the output trace path to use for the single input trace is the directory specified by the
\fBpath\fR
parameter\&.
.RE
.sp
.RS 4
.ie n \{\
\h'-04' 2.\h'+01'\c
.\}
.el \{\
.sp -1
.IP "  2." 4.2
.\}
If the component recognizes the input trace as an LTTng (2\&.11 or greater) trace, then it checks specific trace environment values to build a trace path relative to the directory specified by the
\fBpath\fR
parameter:
.PP
Linux kernel domain
.RS 4
.sp
.if n \{\
.RS 4
.\}
.nf
\fIHOST\fR/\fISNAME\fR\-\fISTIME\fR/kernel
.fi
.if n \{\
.RE
.\}
.RE
.PP
User space domain, per\-UID buffering
.RS 4
.sp
.if n \{\
.RS 4
.\}
.nf
\fIHOST\fR/\fISNAME\fR\-\fISTIME\fR/ust/uid/\fIUID\fR/\fIARCHW\fR\-bit
.fi
.if n \{\
.RE
.\}
.RE
.PP
User space domain, per\-PID buffering
.RS 4
.sp
.if n \{\
.RS 4
.\}
.nf
\fIHOST\fR/\fISNAME\fR\-\fISTIME\fR/ust/pid/\fIPNAME\fR\-\fIPID\fR\-\fIPTIME\fR
.fi
.if n \{\
.RE
.\}
.RE
.sp
With:
.PP
\fIHOST\fR
.RS 4
Target hostname\&.
.RE
.PP
\fISNAME\fR
.RS 4
Tracing session name\&.
.RE
.PP
\fISTIME\fR
.RS 4
Tracing session creation date/time\&.
.RE
.PP
\fIUID\fR
.RS 4
User ID\&.
.RE
.PP
\fIARCHW\fR
.RS 4
Architecture width (\fB32\fR
or
\fB64\fR)\&.
.RE
.PP
\fIPNAME\fR
.RS 4
Process name\&.
.RE
.PP
\fIPID\fR
.RS 4
Process ID\&.
.RE
.PP
\fIPTIME\fR
.RS 4
Process date/time\&.
.RE
.RE
.sp
.RS 4
.ie n \{\
\h'-04' 3.\h'+01'\c
.\}
.el \{\
.sp -1
.IP "  3." 4.2
.\}
If the input trace has a name, then the component sanitizes this name and uses it as a relative path to the directory specified by the
\fBpath\fR
parameter\&.
.sp
The trace name sanitization operation:
.sp
.RS 4
.ie n \{\
\h'-04'\(bu\h'+03'\c
.\}
.el \{\
.sp -1
.IP \(bu 2.3
.\}
Replaces
\fB.\fR
subdirectories with
\fB_\fR\&.
.RE
.sp
.RS 4
.ie n \{\
\h'-04'\(bu\h'+03'\c
.\}
.el \{\
.sp -1
.IP \(bu 2.3
.\}
Replaces
\fB..\fR
subdirectories with
\fB__\fR\&.
.RE
.sp
.RS 4
.ie n \{\
\h'-04'\(bu\h'+03'\c
.\}
.el \{\
.sp -1
.IP \(bu 2.3
.\}
Removes any trailing
\fB/\fR
character\&.
.RE
.RE
.sp
.RS 4
.ie n \{\
\h'-04' 4.\h'+01'\c
.\}
.el \{\
.sp -1
.IP "  4." 4.2
.\}
The component uses the subdirectory
\fBtrace\fR
relative to the directory specified by the
\fBpath\fR
parameter\&.
.RE
.sp
In all the cases above, if the effective output trace path already exists on the file system, then the component appends a numeric suffix to the name of the last subdirectory\&. The suffix starts at\ \&0 and increments until the path doesn\(cqt exist\&.
.SH "INITIALIZATION PARAMETERS"
.PP
\fBassume-single-trace\fR=\fIVAL\fR [optional boolean]
.RS 4
If
\fIVAL\fR
is true, then assume that the component only receives messages related to a single input trace\&.
.sp
This parameter affects how the component builds the output trace path (see
\(lqOutput path\(rq)\&.
.sp
Default: false\&.
.RE
.PP
\fBctf-version\fR=\fIVERSION\fR [optional string]
.RS 4
Write traces following version
\fIVERSION\fR
of CTF, where
\fIVERSION\fR
is one of:
.PP
\fB1\fR, \fB1.8\fR
.RS 4
CTF\ \&1\&.8\&.
.RE
.PP
\fB2\fR, \fB2.0\fR
.RS 4
CTF\ \&2\&.
.RE
.sp
Default:
\fB2\fR\&.
.RE
.PP
\fBignore-discarded-events\fR=\fIVAL\fR [optional boolean]
.RS 4
If
\fIVAL\fR
is true, then ignore discarded events messages\&.
.sp
Default: false\&.
.RE
.PP
\fBignore-discarded-packets\fR=\fIVAL\fR [optional boolean]
.RS 4
If
\fIVAL\fR
is true, then ignore discarded packets messages\&.
.sp
Default: false\&.
.RE
.PP
\fBpath\fR=\fIPATH\fR [string]
.RS 4
Base output path\&.
.sp
See
\(lqOutput path\(rq
to learn how the component uses this parameter to build the output path for a given input trace\&.
.RE
.PP
\fBquiet\fR=\fIVAL\fR [optional boolean]
.RS 4
If
\fIVAL\fR
is true, then don\(cqt write anything to the standard output\&.
.sp
Default: false\&.
.RE
.SH "PORTS"
.sp
.if n \{\
.RS 4
.\}
.nf
+\-\-\-\-\-\-\-\-\-\-\-\-\-+
| sink\&.ctf\&.fs |
|             |
@ in          |
+\-\-\-\-\-\-\-\-\-\-\-\-\-+
.fi
.if n \{\
.RE
.\}
.SS "Input"
.PP
\fBin\fR
.RS 4
Single input port\&.
.RE
.SH "BUGS"
.sp
If you encounter any issue or usability problem, please report it on the Babeltrace bug tracker (see <https://bugs.lttng.org/projects/babeltrace>)\&.
.SH "RESOURCES"
.sp
The Babeltrace project shares some communication channels with the LTTng project (see <https://lttng.org/>)\&.
.sp
.RS 4
.ie n \{\
\h'-04'\(bu\h'+03'\c
.\}
.el \{\
.sp -1
.IP \(bu 2.3
.\}
Babeltrace website (see <https://babeltrace.org/>)
.RE
.sp
.RS 4
.ie n \{\
\h'-04'\(bu\h'+03'\c
.\}
.el \{\
.sp -1
.IP \(bu 2.3
.\}
Mailing list (see <https://lists.lttng.org>)
for support and development:
\fBlttng-dev@lists.lttng.org\fR
.RE
.sp
.RS 4
.ie n \{\
\h'-04'\(bu\h'+03'\c
.\}
.el \{\
.sp -1
.IP \(bu 2.3
.\}
IRC channel (see <irc://irc.oftc.net/lttng>):
\fB#lttng\fR
on
\fBirc.oftc.net\fR
.RE
.sp
.RS 4
.ie n \{\
\h'-04'\(bu\h'+03'\c
.\}
.el \{\
.sp -1
.IP \(bu 2.3
.\}
Bug tracker (see <https://bugs.lttng.org/projects/babeltrace>)
.RE
.sp
.RS 4
.ie n \{\
\h'-04'\(bu\h'+03'\c
.\}
.el \{\
.sp -1
.IP \(bu 2.3
.\}
Git repository (see <https://git.efficios.com/?p=babeltrace.git>)
.RE
.sp
.RS 4
.ie n \{\
\h'-04'\(bu\h'+03'\c
.\}
.el \{\
.sp -1
.IP \(bu 2.3
.\}
GitHub project (see <https://github.com/efficios/babeltrace>)
.RE
.sp
.RS 4
.ie n \{\
\h'-04'\(bu\h'+03'\c
.\}
.el \{\
.sp -1
.IP \(bu 2.3
.\}
Continuous integration (see <https://ci.lttng.org/view/Babeltrace/>)
.RE
.sp
.RS 4
.ie n \{\
\h'-04'\(bu\h'+03'\c
.\}
.el \{\
.sp -1
.IP \(bu 2.3
.\}
Code review (see <https://review.lttng.org/q/project:babeltrace>)
.RE
.SH "AUTHORS"
.sp
The Babeltrace\ \&2 project is the result of hard work by many regular developers and occasional contributors\&.
.sp
The current project maintainer is J\('er\('emie Galarneau <mailto:jeremie.galarneau@efficios.com>\&.
.SH "COPYRIGHT"
.sp
This component class is part of the Babeltrace\ \&2 project\&.
.sp
Babeltrace is distributed under the MIT license (see <https://opensource.org/licenses/MIT>)\&.
.SH "SEE ALSO"
.sp
\fBbabeltrace2-intro\fR(7), \fBbabeltrace2-plugin-ctf\fR(7)
