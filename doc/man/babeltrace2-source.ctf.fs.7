'\" t
.\"     Title: babeltrace2-source.ctf.fs
.\"    Author: [see the "AUTHORS" section]
.\" Generator: DocBook XSL Stylesheets vsnapshot <http://docbook.sf.net/>
.\"      Date: 21 January 2025
.\"    Manual: Babeltrace\ \&2 manual
.\"    Source: Babeltrace 2.1.0
.\"  Language: English
.\"
.TH "BABELTRACE2\-SOURCE\" "7" "21 January 2025" "Babeltrace 2\&.1\&.0" "Babeltrace\ \&2 manual"
.\" -----------------------------------------------------------------
.\" * Define some portability stuff
.\" -----------------------------------------------------------------
.\" ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
.\" http://bugs.debian.org/507673
.\" http://lists.gnu.org/archive/html/groff/2009-02/msg00013.html
.\" ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
.ie \n(.g .ds Aq \(aq
.el       .ds Aq '
.\" -----------------------------------------------------------------
.\" * set default formatting
.\" -----------------------------------------------------------------
.\" disable hyphenation
.nh
.\" disable justification (adjust text to left margin only)
.ad l
.\" -----------------------------------------------------------------
.\" * MAIN CONTENT STARTS HERE *
.\" -----------------------------------------------------------------
.SH "NAME"
babeltrace2-source.ctf.fs \- Babeltrace 2: File system CTF source component class
.SH "DESCRIPTION"
.sp
A Babeltrace\ \&2 \fBsource.ctf.fs\fR message iterator reads one or more CTF\ \&1\&.8 (see <https://diamon.org/ctf/v1.8.3/>) or CTF\ \&2 (see <https://diamon.org/ctf/>) data streams on the file system and emits corresponding messages\&.
.sp
.if n \{\
.RS 4
.\}
.nf
CTF data streams on
the file system
  |
  |   +\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-+
  |   |      src\&.ctf\&.fs     |
  |   |                     |
  \*(Aq\-\->|    \&.\&.\&.5c847 | 0 | 0 @\-\-> Stream 0 messages
      |    \&.\&.\&.5c847 | 0 | 1 @\-\-> Stream 1 messages
      |    \&.\&.\&.5c847 | 0 | 2 @\-\-> Stream 2 messages
      +\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-+
.fi
.if n \{\
.RE
.\}
.sp
See \fBbabeltrace2-intro\fR(7) to learn more about the Babeltrace\ \&2 project and its core concepts\&.
.SS "Input"
.sp
A \fBsource.ctf.fs\fR component opens a single \fIlogical\fR CTF trace\&. A logical CTF trace contains one or more \fIphysical\fR CTF traces\&. A physical CTF trace on the file system is a directory which contains:
.sp
.RS 4
.ie n \{\
\h'-04'\(bu\h'+03'\c
.\}
.el \{\
.sp -1
.IP \(bu 2.3
.\}
One metadata stream file named
\fBmetadata\fR\&.
.RE
.sp
.RS 4
.ie n \{\
\h'-04'\(bu\h'+03'\c
.\}
.el \{\
.sp -1
.IP \(bu 2.3
.\}
One or more data stream files, that is, any file with a name that does not start with
\fB.\fR
and which isn\(cqt
\fBmetadata\fR\&.
.RE
.sp
.RS 4
.ie n \{\
\h'-04'\(bu\h'+03'\c
.\}
.el \{\
.sp -1
.IP \(bu 2.3
.\}
\fBOptional\fR: One
LTTng (see <https://lttng.org/>)
index directory named
\fBindex\fR\&.
.RE
.sp
If the logical CTF trace to handle contains more than one physical CTF trace, then all the physical CTF traces, depending on the CTF version:
.PP
CTF 1\&.8
.RS 4
Must have a trace UUID and all UUIDs must be the same\&.
.RE
.PP
CTF 2
.RS 4
Must have a trace name and a trace unique ID (UID), and all trace namespaces (if any), names, and UIDs must be the same\&.
.RE
.sp
Opening more than one physical CTF trace to constitute a single logical CTF trace is needed to support LTTng\(cqs tracing session rotation feature, for example (see \fBlttng-rotate\fR(1) starting from LTTng\ \&2\&.11)\&. In that case, the component elects the largest metadata stream, considering the total count of data stream classes and event record classes, as the one to use to decode the data streams of all the physical traces\&.
.sp
You specify which physical CTF traces to open and read with the \fBinputs\fR array parameter\&. Each entry in this array is the path to a physical CTF trace directory, that is, the directory directly containing the stream files\&.
.sp
A \fBsource.ctf.fs\fR component doesn\(cqt recurse into directories to find CTF traces\&. However, the component class provides the \fBbabeltrace.support-info\fR query object which indicates whether or not a given directory looks like a CTF trace directory (see \(lq\fBbabeltrace.support-info\fR\(rq)\&.
.sp
The component creates one output port for each logical CTF data stream\&. More than one physical CTF data stream file can support a single logical CTF data stream (LTTng\(cqs trace file rotation and tracing session rotation can cause this)\&.
.sp
If two or more data stream files contain the same packets, then a \fBsource.ctf.fs\fR message iterator reads each of them only once so that it never emits duplicated messages\&. This feature makes it possible, for example, to open overlapping LTTng snapshots (see <https://lttng.org/docs/#doc-taking-a-snapshot>) with a single \fBsource.ctf.fs\fR component and silently discard the duplicated packets\&.
.SS "Trace quirks"
.sp
Many tracers produce CTF traces\&. A \fBsource.ctf.fs\fR component makes some effort to support as many CTF traces as possible, even those with malformed streams\&.
.sp
In general, for a CTF\ \&1\&.8 trace:
.sp
.RS 4
.ie n \{\
\h'-04'\(bu\h'+03'\c
.\}
.el \{\
.sp -1
.IP \(bu 2.3
.\}
If the
\fBtimestamp_begin\fR
or
\fBtimestamp_end\fR
packet context field class exists, but it\(cqs not mapped to a clock class, and there\(cqs only one clock class at this point in the metadata stream, then the component maps the field class to this unique clock class\&.
.RE
.sp
A \fBsource.ctf.fs\fR component has special quirk handling for some LTTng (see <https://lttng.org/>) and barectf (see <https://lttng.org/>) traces, depending on the tracer version:
.PP
All LTTng versions
.RS 4
.sp
.RS 4
.ie n \{\
\h'-04'\(bu\h'+03'\c
.\}
.el \{\
.sp -1
.IP \(bu 2.3
.\}
For CTF\ \&1\&.8 traces, the component sets the origin of the
\fBmonotonic\fR
clock class to the Unix epoch so that different LTTng traces are always correlatable\&.
.sp
This is the equivalent of setting the
\fBforce-clock-class-origin-unix-epoch\fR
parameter to true\&.
.RE
.sp
.RS 4
.ie n \{\
\h'-04'\(bu\h'+03'\c
.\}
.el \{\
.sp -1
.IP \(bu 2.3
.\}
For a given data stream, for all the contiguous last packets of which the
\fBtimestamp_end\fR
context field is\ \&0, the message iterator uses the time of the last event record of the packet as the time of the packet end message\&.
.sp
This is useful for the traces which
\fBlttng-crash\fR(1)
generates\&.
.RE
.RE
.PP
LTTng\-UST up to, but excluding, 2\&.11\&.0, LTTng\-modules up to, but excluding, 2\&.9\&.13, LTTng\-modules from 2\&.10\&.0 to 2\&.10\&.9
.RS 4
.sp
.RS 4
.ie n \{\
\h'-04'\(bu\h'+03'\c
.\}
.el \{\
.sp -1
.IP \(bu 2.3
.\}
For a given packet, the message iterator uses the time of the last event record of the packet as the time of the packet end message, ignoring the
\fBtimestamp_end\fR
packet context field\&.
.RE
.RE
.PP
barectf up to, but excluding, 2\&.3\&.1
.RS 4
.sp
.RS 4
.ie n \{\
\h'-04'\(bu\h'+03'\c
.\}
.el \{\
.sp -1
.IP \(bu 2.3
.\}
For a given packet, the message iterator uses the time of the first event record of the packet as the time of the packet beginning message, ignoring the
\fBtimestamp_begin\fR
packet context field\&.
.RE
.RE
.SS "Message Interchange Protocol (MIP) specifics"
.sp
A \fBsource.ctf.fs\fR component can open and decode, depending on the effective MIP version:
.PP
MIP\ \&0
.RS 4
CTF\ \&1\&.8 traces\&.
.RE
.PP
MIP\ \&1
.RS 4
CTF\ \&1\&.8 and CTF\ \&2 traces\&.
.RE
.sp
While the CTF\ \&1\&.8 metadata stream (TSDL) features naturally map to the MIP\ \&0 API and the CTF\ \&2 ones to the MIP\ \&1 API, a \fBsource.ctf.fs\fR component makes a best effort to represent CTF\ \&1\&.8 features when working with MIP\ \&1\&. In particular, a \fBsource.ctf.fs\fR component:
.sp
.RS 4
.ie n \{\
\h'-04'\(bu\h'+03'\c
.\}
.el \{\
.sp -1
.IP \(bu 2.3
.\}
For an LTTng trace:
.sp
.RS 4
.ie n \{\
\h'-04'\(bu\h'+03'\c
.\}
.el \{\
.sp -1
.IP \(bu 2.3
.\}
Sets the namespace of any libbabeltrace2 trace to
\fBlttng.org,2009\fR\&.
.RE
.sp
.RS 4
.ie n \{\
\h'-04'\(bu\h'+03'\c
.\}
.el \{\
.sp -1
.IP \(bu 2.3
.\}
Sets the origin of any libbabeltrace2 clock class to the Unix epoch\&.
.RE
.RE
.sp
.RS 4
.ie n \{\
\h'-04'\(bu\h'+03'\c
.\}
.el \{\
.sp -1
.IP \(bu 2.3
.\}
If the UUID of a CTF trace is available, then sets the UID of the corresponding libbabeltrace2 trace to the textual representation of the trace UUID and its name to an empty string\&.
.RE
.sp
.RS 4
.ie n \{\
\h'-04'\(bu\h'+03'\c
.\}
.el \{\
.sp -1
.IP \(bu 2.3
.\}
Sets the UID of a libbabeltrace2 clock class to the textual representation of the UUID of its corresponding CTF clock class, if available\&.
.RE
.sp
.RS 4
.ie n \{\
\h'-04'\(bu\h'+03'\c
.\}
.el \{\
.sp -1
.IP \(bu 2.3
.\}
Makes the accuracy of any libbabeltrace2 clock class unknown\&.
.RE
.sp
When reading a CTF\ \&2 metadata stream (therefore working with MIP\ \&1), a \fBsource.ctf.fs\fR component:
.sp
.RS 4
.ie n \{\
\h'-04'\(bu\h'+03'\c
.\}
.el \{\
.sp -1
.IP \(bu 2.3
.\}
Sets the log level of a libbabeltrace2 event class from the value of the
\fBlog-level\fR
attribute, under either the
\fBlttng.org,2009\fR
or
\fBbabeltrace.org,2020\fR
namespace, of its corresponding CTF event record class as such:
.PP
\fB"emergency"\fR
.RS 4
\fBBT_EVENT_CLASS_LOG_LEVEL_EMERGENCY\fR
.RE
.PP
\fB"alert"\fR
.RS 4
\fBBT_EVENT_CLASS_LOG_LEVEL_ALERT\fR
.RE
.PP
\fB"critical"\fR
.RS 4
\fBBT_EVENT_CLASS_LOG_LEVEL_CRITICAL\fR
.RE
.PP
\fB"error"\fR
.RS 4
\fBBT_EVENT_CLASS_LOG_LEVEL_ERROR\fR
.RE
.PP
\fB"warning"\fR
.RS 4
\fBBT_EVENT_CLASS_LOG_LEVEL_WARNING\fR
.RE
.PP
\fB"notice"\fR
.RS 4
\fBBT_EVENT_CLASS_LOG_LEVEL_NOTICE\fR
.RE
.PP
\fB"info"\fR
.RS 4
\fBBT_EVENT_CLASS_LOG_LEVEL_INFO\fR
.RE
.PP
\fB"debug:system"\fR
.RS 4
\fBBT_EVENT_CLASS_LOG_LEVEL_DEBUG_SYSTEM\fR
.RE
.PP
\fB"debug:program"\fR
.RS 4
\fBBT_EVENT_CLASS_LOG_LEVEL_DEBUG_PROGRAM\fR
.RE
.PP
\fB"debug:process"\fR
.RS 4
\fBBT_EVENT_CLASS_LOG_LEVEL_DEBUG_PROCESS\fR
.RE
.PP
\fB"debug:module"\fR
.RS 4
\fBBT_EVENT_CLASS_LOG_LEVEL_DEBUG_MODULE\fR
.RE
.PP
\fB"debug:unit"\fR
.RS 4
\fBBT_EVENT_CLASS_LOG_LEVEL_DEBUG_UNIT\fR
.RE
.PP
\fB"debug:function"\fR
.RS 4
\fBBT_EVENT_CLASS_LOG_LEVEL_DEBUG_FUNCTION\fR
.RE
.PP
\fB"debug:line"\fR
.RS 4
\fBBT_EVENT_CLASS_LOG_LEVEL_DEBUG_LINE\fR
.RE
.PP
\fB"debug:debug"\fR
.RS 4
\fBBT_EVENT_CLASS_LOG_LEVEL_DEBUG\fR
.RE
.RE
.sp
.RS 4
.ie n \{\
\h'-04'\(bu\h'+03'\c
.\}
.el \{\
.sp -1
.IP \(bu 2.3
.\}
Sets the EMF URI of a libbabeltrace2 event class from the value of the
\fBemf-uri\fR
attribute, under either the
\fBlttng.org,2009\fR
or
\fBbabeltrace.org,2020\fR
namespace, of its corresponding CTF event record class\&.
.RE
.SS "CTF compliance"
.sp
A \fBsource.ctf.fs\fR component decodes traces as per CTF\ \&1\&.8 (see <https://diamon.org/ctf/v1.8.3/>) or CTF\ \&2 (see <https://diamon.org/ctf/>), except:
.sp
.RS 4
.ie n \{\
\h'-04'\(bu\h'+03'\c
.\}
.el \{\
.sp -1
.IP \(bu 2.3
.\}
It only supports fixed\-length bit array field classes with sizes from 1\ \&to\ \&64\ \&bits\&.
.sp
This includes fixed\-length integers and fixed\-length floating point numbers:
.PP
CTF\ \&1\&.8 (TSDL)
.RS 4
\fBinteger\fR,
\fBenum\fR, and
\fBfloating_point\fR
blocks\&.
.RE
.PP
CTF\ \&2
.RS 4
\fBfixed-length-bit-array\fR,
\fBfixed-length-bit-map\fR,
\fBfixed-length-boolean\fR,
\fBfixed-length-unsigned-integer\fR,
\fBfixed-length-signed-integer\fR, and
\fBfixed-length-floating-point-number\fR
types\&.
.RE
.RE
.sp
.RS 4
.ie n \{\
\h'-04'\(bu\h'+03'\c
.\}
.el \{\
.sp -1
.IP \(bu 2.3
.\}
It only supports the following CTF\ \&2 variable\-length integer field values:
.PP
\fBvariable-length-unsigned-integer\fR
.RS 4
0 to 18,446,744,073,709,551,615 (unsigned 64\-bit integer range)\&.
.RE
.PP
\fBvariable-length-signed-integer\fR
.RS 4
\-9,223,372,036,854,775,808 to 9,223,372,036,854,775,807 (signed 64\-bit integer range)\&.
.RE
.RE
.sp
.RS 4
.ie n \{\
\h'-04'\(bu\h'+03'\c
.\}
.el \{\
.sp -1
.IP \(bu 2.3
.\}
It only supports 32\-bit and 64\-bit fixed\-length floating point number classes:
.PP
CTF\ \&1\&.8 (TSDL)
.RS 4
\fBfloating_point\fR
block\&.
.RE
.PP
CTF\ \&2
.RS 4
\fBfixed-length-floating-point-number\fR
type\&.
.RE
.RE
.sp
.RS 4
.ie n \{\
\h'-04'\(bu\h'+03'\c
.\}
.el \{\
.sp -1
.IP \(bu 2.3
.\}
It doesn\(cqt support TSDL \(sc\ \&4\&.1\&.6 (\(lqGNU/C bitfields\(rq)\&.
.RE
.sp
.RS 4
.ie n \{\
\h'-04'\(bu\h'+03'\c
.\}
.el \{\
.sp -1
.IP \(bu 2.3
.\}
It doesn\(cqt support TSDL
\fBcallsite\fR
blocks: the CTF\ \&1\&.8 parser simply ignores them\&.
.RE
.sp
.RS 4
.ie n \{\
\h'-04'\(bu\h'+03'\c
.\}
.el \{\
.sp -1
.IP \(bu 2.3
.\}
It only supports a single clock class (TSDL
\fBclock\fR
block or CTF\ \&2
\fBclock-class\fR
fragment) reference per data stream class (TSDL
\fBstream\fR
block or CTF\ \&2
\fBdata-stream-class\fR
fragment)\&.
.RE
.sp
.RS 4
.ie n \{\
\h'-04'\(bu\h'+03'\c
.\}
.el \{\
.sp -1
.IP \(bu 2.3
.\}
It doesn\(cqt support the checksum, compression, and encryption features of CTF\ \&1\&.8 metadata stream packets\&.
.RE
.SH "INITIALIZATION PARAMETERS"
.PP
\fBclock-class-offset-ns\fR=\fINS\fR [optional signed integer]
.RS 4
Add
\fINS\fR
nanoseconds to the offset of all the clock classes that the component creates\&.
.sp
You can combine this parameter with the
\fBclock-class-offset-s\fR
parameter\&.
.RE
.PP
\fBclock-class-offset-s\fR=\fISEC\fR [optional signed integer]
.RS 4
Add
\fISEC\fR
seconds to the offset of all the clock classes that the component creates\&.
.sp
You can combine this parameter with the
\fBclock-class-offset-ns\fR
parameter\&.
.RE
.PP
\fBforce-clock-class-origin-unix-epoch\fR=\fIVAL\fR [optional boolean]
.RS 4
If
\fIVAL\fR
is true, then force the origin of all clock classes that the component creates to have a Unix epoch origin, whatever the detected tracer\&.
.sp
Default: false\&.
.RE
.PP
\fBinputs\fR=\fIDIRS\fR [array of strings]
.RS 4
Open and read the physical CTF traces located in
\fIDIRS\fR\&.
.sp
Each element of
\fIDIRS\fR
is the path to a physical CTF trace directory containing the trace stream files\&.
.sp
All the specified physical CTF traces must belong to the same logical CTF trace\&. See
\(lqInput\(rq
to learn more about logical and physical CTF traces\&.
.RE
.PP
\fBtrace-name\fR=\fINAME\fR [optional string]
.RS 4
Set the name of the trace object that the component creates to
\fINAME\fR\&.
.RE
.SH "PORTS"
.sp
.if n \{\
.RS 4
.\}
.nf
+\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-+
|     src\&.ctf\&.fs     |
|                    |
|   \&.\&.\&.5c847 | 0 | 1 @
|                \&.\&.\&. @
+\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-+
.fi
.if n \{\
.RE
.\}
.SS "Output"
.sp
A \fBsource.ctf.fs\fR component creates one output port for each logical CTF data stream\&. See \(lqInput\(rq to learn more about logical and physical CTF data streams\&.
.sp
Each output port name has one of the following forms:
.sp
.if n \{\
.RS 4
.\}
.nf
\fITRACE\-ID\fR | \fISTREAM\-CLASS\-ID\fR | \fISTREAM\-ID\fR
\fITRACE\-ID\fR | \fISTREAM\-ID\fR
.fi
.if n \{\
.RE
.\}
.sp
The component uses the second form when the stream class ID isn\(cqt available\&.
.PP
\fITRACE\-ID\fR
.RS 4
If the namespace
\fBNS\fR, name
\fBNAME\fR, and UID
\fBUID\fR
of the trace are available, then the exact string:
.sp
.if n \{\
.RS 4
.\}
.nf
{namespace: `NS`, name: `NAME`, uid: `UID`}
.fi
.if n \{\
.RE
.\}
.sp
If the namespace of the trace isn\(cqt available, but the name
\fBNAME\fR
and UID
\fBUID\fR
are, then the exact string:
.sp
.if n \{\
.RS 4
.\}
.nf
{name: `NAME`, uid: `UID`}
.fi
.if n \{\
.RE
.\}
.sp
Otherwise, the absolute directory path of the trace\&.
.if n \{\
.sp
.\}
.RS 4
.it 1 an-trap
.nr an-no-space-flag 1
.nr an-break-flag 1
.br
.ps +1
\fBNote\fR
.ps -1
.br
The namespace of any LTTng trace is
\fBlttng.org,2009\fR\&. If a CTF\ \&1\&.8 trace has a UUID, then its corresponding libbabeltrace2 trace always has a name and a UID (the textual representation of the UUID)\&.
.sp .5v
.RE
.RE
.PP
\fISTREAM\-CLASS\-ID\fR
.RS 4
Stream class ID\&.
.RE
.PP
\fISTREAM\-ID\fR
.RS 4
Stream ID if available, otherwise the absolute file path of the stream\&.
.RE
.SH "QUERY OBJECTS"
.SS "babeltrace\&.support\-info"
.sp
See \fBbabeltrace2-query-babeltrace.support-info\fR(7) to learn more about this query object\&.
.sp
For a directory input which is the path to a CTF trace directory, the result object contains:
.PP
\fBweight\fR
.RS 4
0\&.75
.RE
.PP
\fBgroup\fR
.RS 4
If the namespace
\fBNS\fR, name
\fBNAME\fR, and UID
\fBUID\fR
of the trace are available, then the exact string:
.sp
.if n \{\
.RS 4
.\}
.nf
namespace: NS, name: NAME, uid: UID
.fi
.if n \{\
.RE
.\}
.sp
If the namespace of the trace isn\(cqt available, but the name
\fBNAME\fR
and UID
\fBUID\fR
are, then the exact string:
.sp
.if n \{\
.RS 4
.\}
.nf
name: NAME, uid: UID
.fi
.if n \{\
.RE
.\}
.sp
Otherwise, the entry doesn\(cqt exist\&.
.if n \{\
.sp
.\}
.RS 4
.it 1 an-trap
.nr an-no-space-flag 1
.nr an-break-flag 1
.br
.ps +1
\fBNote\fR
.ps -1
.br
The namespace of any LTTng trace is
\fBlttng.org,2009\fR\&. If a CTF\ \&1\&.8 trace has a UUID, then its corresponding libbabeltrace2 trace always has a name and a UID (the textual representation of the UUID)\&.
.sp .5v
.RE
.RE
.sp
You can leverage the \fBgroup\fR entry of this query object to assemble many physical CTF traces as a single logical CTF trace (see \(lqInput\(rq to learn more about logical and physical CTF traces)\&. This is how the \fBbabeltrace2-convert\fR(1) command makes it possible to specify as non\-option arguments the paths to multiple physical CTF traces which belong to the same logical CTF trace and create a single \fBsource.ctf.fs\fR component\&.
.SS "babeltrace\&.trace\-infos"
.sp
See \fBbabeltrace2-query-babeltrace.trace-infos\fR(7) to learn more about this query object\&.
.SS "metadata\-info"
.sp
You can query the \fBmetadata-info\fR object for a specific CTF trace to get its plain text metadata stream as well as whether or not it\(cqs packetized\&.
.sp
Parameters:
.PP
\fBpath\fR=\fIPATH\fR [string]
.RS 4
Path to the physical CTF trace directory which contains the
\fBmetadata\fR
file\&.
.RE
.sp
Result object (map):
.PP
\fBis-packetized\fR [boolean]
.RS 4
True if the metadata stream file is packetized\&.
.sp
This is only relevant for a CTF\ \&1\&.8 metadata stream\&. Always false with a CTF\ \&2 metadata stream\&.
.RE
.PP
\fBtext\fR [string]
.RS 4
Plain text metadata stream\&.
.sp
The exact contents of the metadata stream file for a CTF\ \&2 metadata stream\&.
.RE
.SH "BUGS"
.sp
If you encounter any issue or usability problem, please report it on the Babeltrace bug tracker (see <https://bugs.lttng.org/projects/babeltrace>)\&.
.SH "RESOURCES"
.sp
The Babeltrace project shares some communication channels with the LTTng project (see <https://lttng.org/>)\&.
.sp
.RS 4
.ie n \{\
\h'-04'\(bu\h'+03'\c
.\}
.el \{\
.sp -1
.IP \(bu 2.3
.\}
Babeltrace website (see <https://babeltrace.org/>)
.RE
.sp
.RS 4
.ie n \{\
\h'-04'\(bu\h'+03'\c
.\}
.el \{\
.sp -1
.IP \(bu 2.3
.\}
Mailing list (see <https://lists.lttng.org>)
for support and development:
\fBlttng-dev@lists.lttng.org\fR
.RE
.sp
.RS 4
.ie n \{\
\h'-04'\(bu\h'+03'\c
.\}
.el \{\
.sp -1
.IP \(bu 2.3
.\}
IRC channel (see <irc://irc.oftc.net/lttng>):
\fB#lttng\fR
on
\fBirc.oftc.net\fR
.RE
.sp
.RS 4
.ie n \{\
\h'-04'\(bu\h'+03'\c
.\}
.el \{\
.sp -1
.IP \(bu 2.3
.\}
Bug tracker (see <https://bugs.lttng.org/projects/babeltrace>)
.RE
.sp
.RS 4
.ie n \{\
\h'-04'\(bu\h'+03'\c
.\}
.el \{\
.sp -1
.IP \(bu 2.3
.\}
Git repository (see <https://git.efficios.com/?p=babeltrace.git>)
.RE
.sp
.RS 4
.ie n \{\
\h'-04'\(bu\h'+03'\c
.\}
.el \{\
.sp -1
.IP \(bu 2.3
.\}
GitHub project (see <https://github.com/efficios/babeltrace>)
.RE
.sp
.RS 4
.ie n \{\
\h'-04'\(bu\h'+03'\c
.\}
.el \{\
.sp -1
.IP \(bu 2.3
.\}
Continuous integration (see <https://ci.lttng.org/view/Babeltrace/>)
.RE
.sp
.RS 4
.ie n \{\
\h'-04'\(bu\h'+03'\c
.\}
.el \{\
.sp -1
.IP \(bu 2.3
.\}
Code review (see <https://review.lttng.org/q/project:babeltrace>)
.RE
.SH "AUTHORS"
.sp
The Babeltrace\ \&2 project is the result of hard work by many regular developers and occasional contributors\&.
.sp
The current project maintainer is J\('er\('emie Galarneau <mailto:jeremie.galarneau@efficios.com>\&.
.SH "COPYRIGHT"
.sp
This component class is part of the Babeltrace\ \&2 project\&.
.sp
Babeltrace is distributed under the MIT license (see <https://opensource.org/licenses/MIT>)\&.
.SH "SEE ALSO"
.sp
\fBbabeltrace2-intro\fR(7), \fBbabeltrace2-plugin-ctf\fR(7), \fBlttng-crash\fR(1)
